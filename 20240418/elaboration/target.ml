open Annot

module type target =
  sig

    type ttype
    type expr
    type context

    (* context functions *)
    val empty_context: context
    val add_variable: context -> string -> ttype -> unit
    val read_variable: context -> string -> expr option
    val log: context -> string -> unit
    val string_of_context: context -> string

    (* ttype functions *)
    val string_of_ttype: context -> ttype -> string
    val ttype_of_atype: context -> atype -> ttype
    val unify: context -> ttype -> ttype -> ttype option
    val ttype_funct: context -> ttype -> ttype -> ttype
    val ttype_from_funct: context -> ttype -> ttype
    val ttype_to_funct: context -> ttype -> ttype
    val ttype_funct_src_dst: context -> ttype -> (ttype * ttype) option

    val string_of_expr: context -> expr -> string
    val expr_of_string: context -> string -> expr list
    val ttype_of_expr: context -> expr -> ttype
    val app: context -> expr -> expr -> expr
    val comp: context -> expr -> expr -> expr
    val var: context -> string -> ttype -> expr
    val letin: context -> string -> expr -> expr -> expr
    val lambda: context -> string -> ttype -> expr -> expr
  end
