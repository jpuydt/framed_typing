open Annot

(* UNTYPED EXPRESSIONS *)

type uexpr = | UString of string * atype option
             | UApp of uexpr * uexpr * atype option
             | ULet of string * uexpr * uexpr
             | ULambda of string * atype option * uexpr * atype option

let string_of_uexpr ue_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec s_of_ue_helper = function
    | UString (name, oannot) ->
       begin match oannot with
       | None ->
          add_string name
       | Some annot ->
          add_string "(";
          add_string name;
          add_string ": ";
          add_string (string_of_atype annot);
          add_string ")"
       end
    | UApp (fn, UString (name, _), oannot) ->
       begin match oannot with
       | None ->
          s_of_ue_helper fn;
          add_string " ";
          add_string name
       | Some annot ->
          add_string "(";
          s_of_ue_helper fn;
          add_string " ";
          add_string name;
          add_string ": ";
          add_string (string_of_atype annot);
          add_string ")"
       end
    | UApp (fn, arg, oannot) ->
       begin match oannot with
       | None ->
          s_of_ue_helper fn;
          add_string " (";
          s_of_ue_helper arg;
          add_string ")"
       | Some annot ->
          s_of_ue_helper fn;
          add_string " (";
          s_of_ue_helper arg;
          add_string "): ";
          add_string (string_of_atype annot);
          add_string ")"
       end
    | ULet (name, def, body) ->
       begin
         add_string "let ";
         add_string name;
         add_string " = ";
         s_of_ue_helper def;
         add_string " in ";
         s_of_ue_helper body
       end
    | ULambda (name, name_oannot, body, body_oannot) ->
       begin
         add_string "[";
         add_string name;
         Option.iter (fun annot ->
             add_string ": ";
             add_string (string_of_atype annot);
           ) name_oannot;
         add_string " |-> ";
         (match body_oannot with
          | Some annot ->
             add_string "(";
             s_of_ue_helper body;
             add_string "): ";
             add_string (string_of_atype annot);
          | None ->
             s_of_ue_helper body);
         add_string "]"
       end
  in
  s_of_ue_helper ue_;
  Buffer.contents buf

let uapp_none fn arg = UApp (fn, arg, None)
let uapp2_none fn arg1 arg2 = uapp_none (uapp_none fn arg1) arg2
let uapp3_none fn arg1 arg2 arg3 = uapp_none (uapp2_none fn arg1 arg2) arg3
