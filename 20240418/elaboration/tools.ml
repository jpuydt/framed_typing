open Stdlib.Option
open Stdlib.List

module NotTrie = Map.Make(String) (* FIXME: I'd prefer a real trie *)

let ( let* ) o f =
  match o with
  | None -> None
  | Some x -> f x

let string_of_list ?(separator= " ") stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string separator;
                      helper tail
  in
  helper list;
  Buffer.contents buf

let rec repeat n l =
  if n = 0 then []
  else append l (repeat (n - 1) l)

let rec chain_of_responsibility chain arg =
  match chain with
  | [] -> None
  | car :: cdr ->
     match car arg with
     | Some res -> Some res
     | None -> chain_of_responsibility cdr arg

let filter_crossmap2 f l1 l2 =
  flatten (map (fun a -> filter_map (f a) l2) l1)

let filter_map2 f l1 l2 =
  filter_map (fun a -> a) (map2 f l1 l2)

let rec list_uniq l =
  match l with
  | [] -> []
  | head::tail ->
     if mem head tail
     then list_uniq tail
     else head :: (list_uniq tail)

let list_split3 ll =
  let a_s = ref []
  and b_s = ref []
  and c_s = ref []
  in
  iter (fun (a, b, c) ->
      a_s := a :: !a_s;
      b_s := b :: !b_s;
      c_s := c :: !c_s) ll;
 rev !a_s, rev !b_s, rev !c_s
