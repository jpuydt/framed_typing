open Stdlib.Option
open Stdlib.Either
open Stdlib.List

open Tools

type atype = | ABasic of string
             | ABruijn of int
             | AFunct of atype * atype (* source destination *)
             | AParam of string * atype list

let rec atype_max_Bruijn ct =
  match ct with
  | ABasic _ -> -1
  | ABruijn n -> n
  | AFunct (src, dst) -> max (atype_max_Bruijn src) (atype_max_Bruijn dst)
  | AParam (_, params) -> fold_left (fun m param -> max m (atype_max_Bruijn param)) (-1) params

let string_of_atype ct =
  let rec dbvars m n =
    if m <= n then
      " db" ^ (string_of_int m) ^ (dbvars (m + 1) n)
    else
      ""
  and params_helper params =
    match params with
    | [] ->
       ""
    | param :: others ->
       (if String.contains param ' ' then " (" ^ param ^ ")" else " " ^ param)
       ^ (params_helper others)
  in
  let rec helper = function
    | ABasic name ->
       name
    | ABruijn n ->
       "db" ^ (string_of_int n)
    | AFunct (AFunct (_, _) as arg, result) ->
       "(" ^ (helper arg) ^ ") -> " ^ (helper result)
    | AFunct (arg, result) ->
       (helper arg) ^ " -> " ^ (helper result)
    | AParam (name, args) ->
       name ^ (params_helper (map helper args))
  in
  let n = atype_max_Bruijn ct in
  (if n >= 0 then "forall" ^ (dbvars 0 n) ^ ", " else "") ^ (helper ct)

let rec atype_Bruijn_shift ?(above=0) n = function
  | ABasic name -> ABasic name
  | ABruijn m -> if m >= above then ABruijn (m + n) else ABruijn m
  | AFunct (src, dst) -> AFunct (atype_Bruijn_shift ~above n src,
                                 atype_Bruijn_shift ~above n dst)
  | AParam (name, params) -> AParam (name, map (atype_Bruijn_shift ~above n) params)

let rec atype_Bruijn_permute m n = function
  | ABasic name -> ABasic name
  | ABruijn s ->
     if s = m then ABruijn n
     else if s = n then ABruijn m
     else ABruijn s
  | AFunct (src, dst) -> AFunct (atype_Bruijn_permute m n src, atype_Bruijn_permute m n dst)
  | AParam (name, params) -> AParam (name, map (atype_Bruijn_permute m n) params)

let atype_normalize atype =
  let tab = ref [] in
  let read value =
    match assoc_opt value !tab with
    | Some res -> res
    | None ->
       let res = length !tab in
       tab := (value, res) :: !tab;
       res
  in
  let rec helper = function
    | ABasic value -> ABasic value
    | ABruijn value -> ABruijn (read value)
    | AFunct (arg_, res_) ->
       (* BEWARE: not written just AFunct (helper arg, helper res)
          because the order is important! *)
       let arg = helper arg_ in
       let res = helper res_ in
       AFunct (arg, res)
    | AParam (name, args) -> AParam (name, map helper args)
  in
  helper atype

let atype_subst n other orig =
  let rec helper =
    function
    | ABasic name -> ABasic name
    | ABruijn m -> if m = n then other else ABruijn m
    | AFunct (src, dst) -> AFunct (helper src, helper dst)
    | AParam (name, params) -> AParam (name, map helper params)
  in
  atype_normalize (helper (atype_Bruijn_shift ~above:(n+1) (1 + atype_max_Bruijn other) orig))

let atype_function_source ct =
  match ct with
  | AFunct (src, _) -> src
  | _ -> ABruijn 0

let atype_function_destination ct =
  match ct with
  | AFunct (_, res) -> atype_normalize res
  | _ -> ABruijn 0

let atype_predicate ct = AFunct (ct, ABasic "Prop")
let atype_relation ct = AFunct (ct, atype_predicate ct)
let atype_involution ct = AFunct (ct, ct)
let atype_operator ct = AFunct (ct, atype_involution ct)
let atype_funct arg res = AFunct (arg, atype_Bruijn_shift ((atype_max_Bruijn arg)+1) res)
let atype_funct2 arg1 arg2 res = atype_funct arg1 (atype_funct arg2 res)
let atype_funct3 arg1 arg2 arg3 res = atype_funct arg1 (atype_funct2 arg2 arg3 res)
let atype_funct_from ct = AFunct (ct, ABruijn ((atype_max_Bruijn ct)+1))
let atype_funct_to ct = AFunct (ABruijn 0, atype_Bruijn_shift 1 ct)

let rec atype_unify ct1 ct2 =
  let module M = struct exception Discovery of ((int * atype), (int * atype)) Either.t end in
  let rec helper link recht =
    match link, recht with
    | ABasic a, ABasic b ->
       if a = b
       then some (ABasic a)
       else none
    | ABruijn nl, ABruijn nr ->
       if nl < nr then raise (M.Discovery (left (nl, ABruijn nr)))
       else if nr < nl then raise (M.Discovery (right (nr, ABruijn nl)))
       else some (ABruijn nl)
    | ABruijn nl, other ->
       raise (M.Discovery (left (nl, other)))
    | other, ABruijn nr ->
       raise (M.Discovery (right (nr, other)))
    | AFunct (argl, resl), AFunct (argr, resr) ->
       let* arg = helper argl argr in
       let* res = helper resl resr in
       some (AFunct (arg, res))
    | AParam (namel, argsl), AParam (namer, argsr) ->
       if namel <> namer
       then
         none
       else
         let args = filter_map2 helper argsl argsr in
         some (AParam (namel, args))
    | _ ->
       none
  in
  try
    let* pre_res = helper ct1 ct2 in
    some (atype_normalize pre_res)
  with M.Discovery exn ->
        match exn with
        | Left (n, other) ->
           let ct1_new = atype_subst n other ct1 in
           atype_unify ct1_new ct2
        | Right (n, other) ->
           let ct2_new = atype_subst n other ct2 in
           atype_unify ct1 ct2_new
