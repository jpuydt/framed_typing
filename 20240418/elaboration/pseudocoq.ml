open Stdlib.Either
open Stdlib.Option
open Stdlib.List

open Tools
open Annot

include Target

type ttype =
  | CBasic of string
  | CBruijn of int
  | CFunct of ttype * ttype (* source destination *)
  | CParam of string * ttype list

type expr = | CConst of string * ttype
            | CApp of expr * expr
            | CLetin of string * expr * expr
            | CLambda of string * ttype * expr

type context =
  {
    mutable coq_file: Buffer.t;
    mutable variables: ttype NotTrie.t;
  }

let rec ttype_max_Bruijn ct =
  match ct with
  | CBasic _ -> -1
  | CBruijn n -> n
  | CFunct (src, dst) -> max (ttype_max_Bruijn src) (ttype_max_Bruijn dst)
  | CParam (_, params) -> fold_left (fun m param -> max m (ttype_max_Bruijn param)) (-1) params

let internal_string_of_ttype tt =
  let rec dbvars m n =
    if m <= n then
      " db" ^ (string_of_int m) ^ (dbvars (m + 1) n)
    else
      ""
  and params_helper params =
    match params with
    | [] ->
       ""
    | param :: others ->
       (if String.contains param ' ' then " (" ^ param ^ ")" else " " ^ param)
       ^ (params_helper others)
  in
  let rec helper = function
    | CBasic name ->
       name
    | CBruijn n ->
       "db" ^ (string_of_int n)
    | CFunct (CFunct (_, _) as arg, result) ->
       "(" ^ (helper arg) ^ ") -> " ^ (helper result)
    | CFunct (arg, result) ->
       (helper arg) ^ " -> " ^ (helper result)
    | CParam (name, args) ->
       name ^ (params_helper (map helper args))
  in
  let n = ttype_max_Bruijn tt in
  (if n >= 0 then "forall" ^ (dbvars 0 n) ^ ", " else "") ^ (helper tt)

let string_of_ttype (_: context) = internal_string_of_ttype

let ttype_normalize ttype =
  let tab = ref [] in
  let read value =
    match assoc_opt value !tab with
    | Some res -> res
    | None ->
       let res = length !tab in
       tab := (value, res) :: !tab;
       res
  in
  let rec helper = function
    | CBasic value -> CBasic value
    | CBruijn value -> CBruijn (read value)
    | CFunct (arg_, res_) ->
       (* BEWARE: not written just CFunct (helper arg, helper res)
          because the order is important! *)
       let arg = helper arg_ in
       let res = helper res_ in
       CFunct (arg, res)
    | CParam (name, args) -> CParam (name, map helper args)
  in
  helper ttype

let rec ttype_Bruijn_shift ?(above=0) n = function
  | CBasic name -> CBasic name
  | CBruijn m -> if m >= above then CBruijn (m + n) else CBruijn m
  | CFunct (src, dst) -> CFunct (ttype_Bruijn_shift ~above n src,
                                 ttype_Bruijn_shift ~above n dst)
  | CParam (name, params) -> CParam (name, map (ttype_Bruijn_shift ~above n) params)

let ttype_function_destination tt =
  match tt with
  | CFunct (_, res) -> ttype_normalize res
  | CBruijn _ -> CBruijn 0
  | _ -> raise (Invalid_argument ("[ttype_function_destination] type `" ^ (internal_string_of_ttype tt) ^ "` isn't a functional type"))

let ttype_subst n other orig =
  let rec helper =
    function
    | CBasic name -> CBasic name
    | CBruijn m -> if m = n then other else CBruijn m
    | CFunct (src, dst) -> CFunct (helper src, helper dst)
    | CParam (name, params) -> CParam (name, map helper params)
  in
  ttype_normalize (helper (ttype_Bruijn_shift ~above:(n+1) (1 + ttype_max_Bruijn other) orig))

let rec ttype_of_atype context = function
  | ABasic name -> CBasic name
  | ABruijn num -> CBruijn num
  | AFunct (src, dst) -> CFunct (ttype_of_atype context src, ttype_of_atype context dst)
  | AParam (name, params) -> CParam (name, map (ttype_of_atype context) params)

let rec unify (context: context) tt1 tt2 =
  let module M = struct exception Discovery of ((int * ttype), (int * ttype)) Either.t end in
  let rec helper link recht =
    match link, recht with
    | CBasic a, CBasic b ->
       if a = b
       then some (CBasic a)
        else none
    | CBruijn nl, CBruijn nr ->
       if nl < nr then raise (M.Discovery (left (nl, CBruijn nr)))
       else if nr < nl then raise (M.Discovery (right (nr, CBruijn nl)))
       else some (CBruijn nl)
    | CBruijn nl, other ->
       raise (M.Discovery (left (nl, other)))
    | other, CBruijn nr ->
       raise (M.Discovery (right (nr, other)))
    | CFunct (argl, resl), CFunct (argr, resr) ->
       let* arg = helper argl argr in
       let* res = helper resl resr in
       some (CFunct (arg, res))
    | CParam (namel, argsl), CParam (namer, argsr) ->
       if namel <> namer || length argsl <> length argsr
       then none
       else
         let args = filter_map2 helper argsl argsr in
         if length args = length argsl
         then some (CParam (namel, args))
         else none
    | _ ->
       none
  in
  try
    let* pre_res = helper tt1 tt2 in
    let res = ttype_normalize pre_res in
    some res
  with M.Discovery exn ->
        match exn with
        | Left (n, other) ->
           let tt1_new = ttype_subst n other tt1 in
           unify context tt1_new tt2
        | Right (n, other) ->
           let tt2_new = ttype_subst n other tt2 in
           unify context tt1 tt2_new

let ttype_funct (_: context) tt1 tt2 = CFunct (tt1, tt2)

let ttype_from_funct (_: context) tt = CFunct (tt, CBruijn ((ttype_max_Bruijn tt)+1))

let ttype_to_funct (_: context) tt = CFunct (CBruijn 0, ttype_Bruijn_shift 1 tt)

let ttype_funct_src_dst (_: context) tt =
  match tt with
  | CFunct (src, dst) -> some (src, dst)
  | CBruijn _ -> some (CBruijn 0, CBruijn 0)
  | _ -> none

let string_of_expr context expr =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper expr =
    match expr with
    | CConst (name, _) ->
       add_string name
    | CApp (fn, CConst (name, _)) ->
       helper fn;
       add_string " ";
       add_string name
    | CApp (fn, arg) ->
       helper fn;
       add_string " (";
       helper arg;
       add_string ")"
    | CLetin (name, def, body) ->
       add_string "let ";
       add_string name;
       add_string " := ";
       helper def;
       add_string " in ";
       helper body
    | CLambda (name, ttype, body) ->
       add_string "fun ";
       add_string name;
       add_string ": ";
       add_string (string_of_ttype context ttype);
       add_string " => ";
       helper body
  in
  helper expr;
  Buffer.contents buf

let expr_of_string (_: context) (_: string) = []

let rec ttype_of_expr context = function
  | CConst (_, ttype) ->
     ttype
  | CApp (fnct, arg) ->
     begin
       let fnct_pre_tt = ttype_of_expr context fnct
       and arg_tt = ttype_of_expr context arg
       in
       match unify context fnct_pre_tt (ttype_from_funct context arg_tt) with
       | Some fnct_post_tt ->
          ttype_function_destination fnct_post_tt
       | None ->
          raise (Invalid_argument ("ttype_of_expr called on non-unifiable types "
                                   ^ (string_of_ttype context fnct_pre_tt)
                                   ^ " and "
                                   ^ (string_of_ttype context (ttype_from_funct context arg_tt))))
     end
  | CLetin (_, _, body) ->
     ttype_of_expr context body
  | CLambda (_, ttype, body) ->
     ttype_funct context ttype (ttype_of_expr context body)

let app (_: context) fnct arg = CApp (fnct, arg)

let comp (_: context) fnct1 fnct2 =
  let compose = CConst ("compose", CFunct (CFunct (CBruijn 1, CBruijn 2),
                                           CFunct (CFunct (CBruijn 0, CBruijn 1),
                                                   CFunct (CBruijn 0, CBruijn 2)))) in
  CApp (CApp (compose, fnct1), fnct2)

let var (_: context) name ttype = CConst (name, ttype)

let letin (_: context) name def body = CLetin (name, def, body)

let lambda (_: context) name ttype body = CLambda (name, ttype, body)

let log context line =
  Buffer.add_string context.coq_file line

let string_of_context context =
  Buffer.contents context.coq_file

let add_variable_internal context name ttype =
  context.variables <- NotTrie.update name (fun _ -> some ttype) context.variables

let add_variable context name ttype =
  add_variable_internal context name ttype

let ttype_predicate tt = CFunct (tt, CBasic "Prop")
let ttype_relation tt = CFunct (tt, ttype_predicate tt)
let ttype_involution tt = CFunct (tt, tt)
let ttype_operator tt = CFunct (tt, ttype_involution tt)

let empty_context =
  let res = {
      coq_file = Buffer.create 0;
      variables = NotTrie.empty; }
  in
  let add = add_variable_internal res
  in
  let bool = CBasic "bool"
  and prop = CBasic "Prop"
  and nn = CBasic "N"
  and zz = CBasic "Z"
  and a_list = CParam ("list", [CBruijn 0])
  in
  log res "Set Implicit Arguments.\n";
  log res "\n";
  add "is_true" (ttype_predicate bool);
  add "True" prop;
  add "False" prop;
  add "and" (ttype_relation prop);
  add "or" (ttype_relation prop);
  add "not" (ttype_predicate prop);
  add "N.add" (ttype_operator nn);
  add "N.mul" (ttype_operator nn);
  add "Z.add" (ttype_operator zz);
  add "Z.mul" (ttype_operator zz);
  add "Z_of_N" (ttype_funct res nn zz);
  add "nil" a_list;
  add "cons" (CFunct (CBruijn 0, ttype_involution a_list));
  add "compose" (CFunct ((CFunct (CBruijn 1, CBruijn 2)),
                         CFunct ((CFunct (CBruijn 0, CBruijn 1)),
                                 (CFunct (CBruijn 0, CBruijn 2)))));
  res

let read_variable context name =
  let* ttype = NotTrie.find_opt name context.variables in
  some (CConst (name, ttype))
