\documentclass[a4paper]{easychair}

\usepackage[l2tabu,orthodox]{nag}

\usepackage{afterpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{etoolbox}
\usepackage{forest}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{stmaryrd}
\usepackage{version}
\usepackage[section]{placeins}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{fancyvrb}
\VerbatimFootnotes

\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage[autosize]{dot2texi}

\newbool{soluces}

\usepackage[justification=centering]{caption}

\usepackage{listings}
\lstset{language=python,texcl=true,breaklines=true,literate=%
{à}{{\`a}}1
{ç}{{\,c}}1
{è}{{\`e}}1
{é}{{\'e}}1
{ê}{{\^e}}1
{É}{{\'E}}1
{î}{{\^i}}1
{ô}{{\^o}}1
{û}{{\^u}}1
}

\DeclareUnicodeCharacter{221E}{$\infty$}

\usepackage{tikz}
\usetikzlibrary{calc,trees}

\usepackage{pgfplots}
\pgfplotsset{compat=newest,trig format plots=rad}

\usepackage{diagbox}
\usepackage{hhline}

\usepackage{amsthm}
\newtheorem{corollary}{Corollaire}
\newtheorem{definition}{Définition}
\newtheorem{example}{Exemple}
\newtheorem{counterexample}{Contre-exemple}
\newtheorem{note}{Note}
\newtheorem*{prof}{Prof}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remarque}
\newtheorem{notation}{Notation}
\newtheorem{theorem}{Théorème}
\newtheorem{lemma}{Lemme}
\newtheorem{warning}{Avertissement}
\newtheorem{exercice}{Exercice}
\newtheorem{probleme}{Problème}

\newenvironment{enumeratealpha}{\begin{enumerate}[label=\alph*)]}{\end{enumerate}}

\usepackage{bbm}
\def \AA {\mathbbm A}
\def \CC {\mathbbm C}
\def \EE {\mathbbm E}
\def \FF {\mathbbm F}
\def \HH {\mathbbm H}
\def \KK {\mathbbm K}
\def \NN {\mathbbm N}
\def \PP {\mathbbm P}
\def \QQ {\mathbbm Q}
\def \RR {\mathbbm R}
\def \UU {\mathbbm U}
\def \ZZ {\mathbbm Z}

\title{Framed typing \\(extended abstract)}
\author{J.~Puydt\thanks{julien.puydt@gmail.com}}
\institute{Debian}
\titlerunning{Framed typing}
\authorrunning{J.Puydt}
\date{}

\begin{document}
\maketitle

\section{Introduction}

A typical mathematical text is decomposed in two stages: there are
first declarations, where objects are introduced by stating what they
are; and then the development where these objects are used. At that
point the types of the objects is hardly mentioned: it is up to the
reader to figure out what is meant, using the declarations as a guide.

In most proof assistants, the organisation is similar on the surface:
users declare variables and hypotheses, then work on examples or
results. But in fact they internally use very precise information on
the nature of the objects (strong typing), so the textual content
entered by the users is generally considered under-specified and needs
to be refined, a process called elaboration
(\cite{AvigadKongMouraRoux2015a} and \cite{CoqRef} describe how Lean
and Coq handle this). This elaboration is done in the same way at both
the declaration and development stages, with two consequences. First,
the user will need to give typing information in both stages, which
means they get intertwined. Second, if there is a problem (typo or
mistake), then the error message might be quite cryptic.

Objects of mathematics can be interpreted as elements of sets taken in
a gigantic graph where the edges are natural inclusions or more
general morphisms. That means a computer system with a rich
mathematical library will have thousands of possibilities to interpret
anything, and explains why a general elaboration algorithm is complex
and needs hints everywhere. But in mathematical texts the author only
uses very few vertices from the monster graph of all of
mathematics. We wish to exploit the small size of this relevant
subgraph to instantiate/synthetize/preload interpretations at
declaration time and avoid doing so as much as possible at development
time. This can be done using the notion of "frame". We populate the
frame at the declaration stage and use it at the development
stage. This ensures we need much less on-the-spot typing guidance and
are able to give less cryptic error message in case of issue.

The author has a PhD in mathematics and is a teacher in mathematics in
the first two years of university (more precisely: in Classes
Préparatoires aux Grandes Écoles), hence is well aware of both sides
of the problem: a first-year student working on an exercise on real
sequences should not be asked hard questions on normed modules or
topological spaces and their fifty shades of separation. And an expert
should not be asked trivial questions on the usual objects of her or
his domain. Frames are a tool to adapt the assistant to its user.

The choice of the word "frame" is all but innocent, as it is already
an old concept in linguistics, cognitive science and artificial
intelligence which has in later years been used to model how
explicitly given information is combined with expectations deriving
from background knowledge to understand a mathematical text --
\cite{CarlCramerFisseniSarikaryaSchroeder2021a} contains a detailed
description in this setting. What we describe here is how to use those
ideas in code to help with the elaboration problem.

Even if any proof assistant could use the general ideas of this paper,
sample code in OCaml implementing them is available and targets Coq
\footnote{\url{https://framagit.org/jpuydt/framed_typing/-/tree/master/20240418}}.

\section{Frames}

\subsection{Principle}\label{principle}

As mentioned in the introduction the core information of a frame is a
very small acyclic graph adapted to the mathematical situation
considered by the user: the vertices are the sets in which the
introduced objects live and its edges are usual maps (inclusions or
morphisms). In addition to this subgraph, it also contains relevant
background information like the available operations with these
objects (operators, comparisons, etc).

The frame is populated at the declaration stage, depending on the
objects: the usual line
$\NN\rightarrow\ZZ\rightarrow\QQ\rightarrow\RR$ is relevant for most
computations by most users, hence it should probably be available by
default. In analysis, it is often extended with $\bar\RR$ or $\CC$. In
algebra a user might introduce an element of $\FF_2(X)$, and this
should trigger the addition of the line
$\ZZ\rightarrow\FF_2\rightarrow\FF_2[X]\rightarrow\FF_2(X)$ to the
subgraph part of the frame (where $\FF_q$ is the finite field with $q$
elements), and of course the usual operations would be added as
relevant background.

At the development stage, when elaborating an expression, the first
use of the frame is as a set of possible interpretations for the
leaves. If the user works in the $\NN\subset\ZZ\subset\QQ\subset\RR$
frame, then $0$ has four possible interpretations, and the question
is: which is meant? It is easier to answer and more pertinent to the
user than if we started to look for (or worse: to build) an
out-of-touch $\ZZ$-module instance or an abstract additive set at each
usage point.

Bidirectional typing (\cite{DunfieldKrishnaswami2021a} is a good
introduction to this technique) then propagates this information to
the whole expression tree and helps combining constraints to cut down
the initial list of interpretations of the previous paragraph. Once we
have a shorter list of possibilities, either we know the only
interpretation and we are done, or we still are undecisive and need to
choose the simplest one.

And this is the second usage of frames. We now use the fact that a
frame contains a subgraph and not just a set: we can use the edges to
decide what "more simple" or "more basic" means using it. We explain
how this is done in details in section~\ref{symbols}, but let us see a
trivial situation. Assume the frame has maps
$\NN\rightarrow\ZZ\rightarrow R$ where $R$ is some commutative ring
with unit (of course explictly introduced by the user at declaration
stage!), and an expression mentions a $0$ that could be any of
$0\in\NN$, $0\in\ZZ$ or $0\in R$. The natural integer interpretation
is the one we will prefer according to the arrows in the frame.

Let us finish with a simple example about the relevant background
information: in all of mathematics $\leqslant$ can mean many
things. But if the user only declared real and complex numbers, we
will have prepared the frame with $\leqslant$ instances related to
reals (for $\NN$, $\ZZ$, $\QQ$ and $\RR$), and only those. Now if the
user tries a comparison between a real number and a complex number (a
very frequent novice mistake), then we will immediately see it has no
sense in the current frame and report we do not know how to compare a
real and a complex number. In the same situation a typical proof
assistant would first embark on a wild-goose chase for a comparison
operator, fail and report it did not manage to instantiate the
required operator ; and the more it will have achieved in trying to
look for a meaning, the more cryptic the error message will be. The
frame makes us fail early, report tightly.

\subsection{Definition methods}\label{definition}

To set up a frame, several methods should be available:
\begin{description}
\item[Default.] The default frame $\NN\subset\ZZ\subset\QQ\subset\RR$
  with the usual operations and relations covers a very wide range of
  usages.
\item[Automatic inclusion.] The frame should automatically be extended
  when a new object is introduced, without an extra sentence: "Let
  $\KK$ be a field" is already an explicit request to add $\KK$, an
  arrow $\ZZ\rightarrow\KK$ and its operations to the current
  frame. The concrete list of those operations should be modelled on
  what current systems do with type class resolution (mathcomp with
  Coq~\cite{SozeauOury2008a} and mathlib with
  Lean~\cite{AvigadKongMouraRoux2015a}).
\item[Catalog.] The user could explicitly choose among ready-to-use
  frames. Experts in specialized subfields generally share common
  objects and notations. A teacher could provide increasing frames to
  guide the students' progression on a weekly basis.
\item[Fine-tuning.] More advanced users could prepare their own frame,
  choosing the relevant arrows or combining existing frames.
\end{description}

The sample code uses both simple primitives for hand specification and
some smart functions to declare new objects and populate the frame
simultaneously.

\subsection{Symbols}\label{symbols}

A large source of ambiguity in mathematics is the use of symbols,
where a same notation is used to denote very different actual
objects. This is called overloading and what we describe here is
sometimes called adhoc overloading.

As first approximation a symbol is just a set of expressions: the
various instances of the symbol. But such a weak structure is not
enough: for a sum of natural integers, probably all interpretations of
addition make sense from the addition of natural integers itself to
the addition of functions on some domain. Storing those
interpretations as a simple set does not make it clear that the
addition $\NN\times\NN\rightarrow\NN$ is the right one.

A naive idea would be to use the number of required coercions as a way
to determine the best choice. But this means each time we face such a
choice, we should consider all possibilities, elaborate the whole
expression with each such choice and then backtrack to keep the best
one -- it would be extremely expensive, even with the limited number
of objects of a typical frame.

A better idea is to use the frame to order the symbol instances: as it
contains an acyclic graph we can interpret it as a poset, giving an
order relation on the objects.

Unfortunately, this falls short on simple examples already: even if we
know a coercion $\NN\rightarrow\ZZ$, we cannot deduce a clear ordering
for two simple additions like $+_\NN:\NN\rightarrow\NN\rightarrow\NN$
and $+_\ZZ:\ZZ\rightarrow\ZZ\rightarrow\ZZ$ as the implied order on
function types is contravariant on the source and covariant on the
target.

So we need to improve on this initial ordering idea to take into
account more complex types. For this we remark that a complex object
is generally related to a simpler object, so if we interpret this
simpler object as a level of abstraction/complexity, then we will be
able to use the frame to compare these levels and hence the complex
types.

We can now properly define a symbol as a list of pairs (level,
instance), and we order these pairs using the frame on the level
part. For the example of $+_\NN$ and $+_\ZZ$, the associated pairs are
$(\NN, +_\NN)$ and $(\ZZ, +_\ZZ)$ and it is now obvious the first one
is the simplest.

\section{Examples}\label{examples}

The examples are written in a toy language, each line giving the
expression to type and as a string the resulting Coq expression.  The
actual output file contains informative intermediary representations
as comments.

\subsection{Basic examples}\label{basic}

The frame for this first set of examples contains:
\begin{itemize}
\item the $\NN\rightarrow\ZZ$ map ;
\item the \verb+add+ and \verb+mul+ symbols each have two instances,
  one for natural integers and one for relative integers.
\end{itemize}

We then consider three variables: \verb+a+, \verb+b+ and \verb+c+
where the first two are natural integers and the last is a relative
integer.

The sample code is able to type \verb+add a b+ as the sum of two
natural integers, but since it would also have made sense for relative
integers, the choice is actually not straightforward and the algorithm
needs the simplicity criterium discussed in~\ref{symbols} to reach the
conclusion.

The cases of \verb+add a c+ and \verb+add c 1+ look much easier, and
indeed the presence of $c$ means it can only be a relative integer
sum, so the decision on the operator is trivial. But since $a$ and $1$
are seen as possibly elements of $\NN$ or $\ZZ$, they still mean a
choice has to be made.

For \verb+add (add a b) c+, the code detects $a+b$ is a sum of natural
integers, then casts upward to sum with $c$ in relative integers:
\begin{verbatim}
check (a + b + c)
      "(Zadd (Z_of_N (Nadd a b)) c): Z".
\end{verbatim}

For more abstract examples, \verb+add c+ is typed as
$\ZZ\rightarrow\ZZ$ and \verb+add a+ as $\NN\rightarrow\NN$. We want
to eliminate most annotations, but they are sometimes really necessary
so we allow them:
\begin{verbatim}
check (add a : (Z -> Z))
      "(Zadd (Z_of_N a)): Z -> Z".
\end{verbatim}

\subsection{Filter examples}\label{filters}

This set of examples is there to show how framed typing works in
analysis. We first declare new Coq objects to modelize a simple
situation:
\begin{itemize}
\item a type \verb+R+ for reals and a type \verb+Rbar+ for extended
  reals together with conversion functions \verb+Z -> R+ and
  \verb+R -> Rbar+, and \verb+minfty+ and \verb+pinfty+ for the
  $\pm\infty$ elements;
\item a type for filters on a set, \verb+filter: Type -> Type+;
\item a predicate \verb+tends_to+ accepting a function, a filter on
  the source and a filter on the target;
\item the usual filter on the natural integers \verb+inftyN+ and a
  family of filters on reals indexed by extended reals
  \verb+filter_of_Rbar+, giving a map \verb+Rbar -> filter R+.
\end{itemize}

The frame gets defined with:
\begin{itemize}
\item the
  $\NN\rightarrow\ZZ\rightarrow\RR\rightarrow\bar\RR\rightarrow\mathrm{Filter}(\RR)$
  series of coercions;
\item the $-\infty$ symbol has a single instance: the element of
  $\bar\RR$;
\item the $+\infty$ symbol has two instances: as an element of
  $\bar\RR$ and as the usual filter on natural integers.
\end{itemize}

We then declare a real variable \verb+x+, a real sequence \verb+v+ and
a real-to-real function \verb+f+, and we try to elaborate the types of
various expressions describing limits of these functions at various
filters to various filters ; here is a sample of the Coq code obtained
by the sample code:
\begin{verbatim}
check (tends_to v +oo x)
      "(tends_to v inftyN (filter_of_Rbar (Rbar_of_R x))): Prop".

check (tends_to v +oo +oo)
      "(tends_to v inftyN (filter_of_Rbar pinftyR)): Prop".

(* considering untyped expression: tends_to f x x *)
Check (tends_to f (filter_of_Rbar (Rbar_of_R x))
                  (filter_of_Rbar (Rbar_of_R x))): Prop.
\end{verbatim}

\subsection{Function examples}\label{functions}

We now consider functions on an interval $I$ and scalar values: a
pretty usual setting even for first-year students.

The mathematical objects are functions $f$, $g$ and $h$ defined on
$I$, the first two with values in some field $\KK$, and the last one
with integer values. We also use a scalar $k$. This mathematical
situation is quite rich and can lead to ambiguous expressions quite
easily as we can consider any scalar as a constant function.

The frame is hence:
\begin{itemize}
\item a line of coercions
  $\NN\rightarrow\ZZ\rightarrow\KK\rightarrow\mathcal F(I,\KK)$ ;
\item there are four instances of addition (inner law on each level:
  \verb+N.add+, \verb+Z.add+, \verb+Kadd+ and \verb+FIKadd+) ;
\item there are five instances of multiplication (inner law on each
  level and the outer product on the function space: \verb+N.mul+,
  \verb+Z.mul+, \verb+Kmul+, \verb+FIKmul+ and \verb+FIKext_mul+).
\end{itemize}

Here is a sample of untyped expressions which get successfully
interpreted -- the last one displays coercions obtained by composition
using the functoriality of $\mathrm{Hom}$:
\begin{verbatim}
check (f + g)
      "(FIKadd f g): I -> K".

check (f * g)
      "(FIKmul f g): I -> K".

check (k * f)
      "(FIKext_mul k f): I -> K".

check (f + 0)
      "(FIKadd f (FIK_of_K (K_of_Z (Z_of_N N0)))): I -> K".

check (f + h)
      "(FIKadd f (compose K_of_Z (compose Z_of_N h))): I -> K".
\end{verbatim}

\subsection{Inequality examples}\label{inequalities}

The frame is populated with:
\begin{itemize}
\item the line $\NN\subset\ZZ\subset\RR\subset\CC$;
\item inequalities at all levels, except of course for complex numbers.
\end{itemize}

And we declare variables $(a,b,x,z)\in\NN\times\ZZ\times\RR\times\CC$.

We then try to compare those variables, and get the expected results:
\begin{itemize}
\item when possible, the comparison is done at the highest level by
  inserting coercions;
\item trying to compare with a complex number will give an error message
  explaining a complex number is not fit for comparison.
\end{itemize}

\subsection{Polymorphism examples}\label{polymorphism}

The filters examples in subsection~\ref{filters} made it already clear
that polymorphic objects worked quite well. This series of examples
adds a few more samples using list constructions:
\begin{verbatim}
check (cons a (nil : list Z))
      "(cons (Z_of_N a) nil): list Z".

check (cons a (cons b nil))
      "(cons (Z_of_N a) (cons b nil)): list Z".
\end{verbatim}

There is also the case of a ternary equality predicate \verb+eq3+,
showing the algorithm handles long-range typing constraints without
need for a special case:
\footnote{\url{https://github.com/math-comp/math-comp/pull/1051#issuecomment-1877789760}}
\begin{verbatim}
check (eq3 a b c)
      "(eq3 (Z_of_N a) b (Z_of_N c)): bool".
\end{verbatim}

\subsection{Ring examples}\label{rings}

We now turn to more algebraic computations with various rings. We
consider a ring $A$, a quotient $A/I$, and elements
$(a,b,c,d)\in\NN\times\ZZ\times A\times A/I$, and we want to interpret various
expressions combining them using the available operators.

Accordingly the frame consists in:
\begin{itemize}
\item a line of coercions
  $\NN\rightarrow\ZZ\rightarrow A\rightarrow A/I$;
\item the sum and product are symbols with four interpretations (one
  at each level).
\end{itemize}

Here are sample untyped expressions and the resulting elaborated
expression:
\begin{verbatim}
check (a + c)
      "(Aadd (A_of_Z (Z_of_N a)) c): A".

check (add a : (A -> A))
      "(Aadd (A_of_Z (Z_of_N a))): A -> A".

check (c + 0)
      "(Aadd c (A_of_Z (Z_of_N N0))): A".

check (a + (c + d))
      "(AquotIadd (AquotI_of_Z (Z_of_N a)) (AquotIadd (AmodI c) d)): AquotI".

check ((x : A) -> y -> x + y)
      "(fun x: A => fun y: A => Aadd x y): A -> A -> A".
\end{verbatim}

Notice in particular that in the last expression, \verb+add a b+ is
computed in $R$ and the result is then pushed in $R/I$ to sum with
$c$: the elaboration gives as tight an interpretation as possible.

\subsection{Vector examples}\label{vectors}

Another classical algebraic setting is computations in vector
spaces. The mathematical situation we consider is: $\KK$ is a field,
$E$ is a vector space on $\KK$ and $F$ and $G$ are two subspaces of
$E$.

The previous declaration should have been enough to populate the frame
with various data. First several coercions lines:
$\NN\rightarrow\ZZ\rightarrow\KK$ for scalars and $F\rightarrow E$ and
$G\rightarrow E$ for vectors. Then six interpretations of addition and
$0$ should be known (one for each level). Finally multiplication
should be three inner products and three outer products, also six
interpretations.

With this setup, we take objects $(f,g,k)\in F\times G\times\KK$ and
try to compute with them:
\begin{verbatim}
check (f + f)
      "(Fadd f f): F".

check (f + 0)
      "(Fadd f F0): F".

check (f + g)
      "(Eadd (E_of_F f) (E_of_G g)): E".

check (k * 0)
      "(Kmul k (K_of_Z (Z_of_N N0))): K".

check (k * f)
      "(Fext_mul k f): F".
\end{verbatim}

In particular the second sample shows that vectors in different
subspaces get promoted to the ambient vector space to compute their
sum, as expected, and we see \verb+O+ can get interpreted as the zero
of a subspace \verb+FO+ when it makes sense.

\subsection{Zero example}\label{zero}

This final example file combines several previous ones and is a
reference situation: let $\KK$ be a field, $E$ a $\KK$-vector space
and $v:\NN\rightarrow E$ a sequence. The expression $0+0v_0$ gets a
valid interpretation even though there are three different uses of
zero:
\begin{verbatim}
(* considering untyped expression: add 0 (mul 0 (v 0)) *)
Check (Eadd E0 (Eext_mul (K_of_Z (Z_of_N N0)) (v N0))): E.
\end{verbatim}

\section{Conclusion}

All examples given above work automatically with the sample code and
the next objective is to test the ideas in real conditions with a real
mathematical library, possibly with a modified Coq.

\section*{Acknowledments}

I am grateful to Y.~Bertot for his encouraging guidance and his
comments that greatly improved this manuscript, and to A.~Charguéraud
for his explanations on bidirectional typing.

\bibliography{/home/jpuydt/Recherche/Abords/bibliography.bib}
\bibliographystyle{plain}

\end{document}
