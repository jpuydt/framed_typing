\documentclass{beamer}
\usetheme{PaloAlto}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fancyvrb}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{tikz}
\usetikzlibrary{arrows,calc,fit,shapes,trees}

\usepackage{bbm}
\def \AA {\mathbbm A}
\def \CC {\mathbbm C}
\def \EE {\mathbbm E}
\def \FF {\mathbbm F}
\def \HH {\mathbbm H}
\def \KK {\mathbbm K}
\def \NN {\mathbbm N}
\def \PP {\mathbbm P}
\def \QQ {\mathbbm Q}
\def \RR {\mathbbm R}
\def \UU {\mathbbm U}
\def \ZZ {\mathbbm Z}

\title{Framed typing}
\author{J.~Puydt}
\institute{STAMP team}
\date{ThEDU'24}


\begin{document}

\tikzset{%
  block/.style={draw, rectangle,rounded corners}
}

\frame{\titlepage}

\begin{frame}
  \frametitle{Who am I?}

  \begin{block}{Mathematician}
    PhD in number theory, 2003
  \end{block}

  \begin{block}{Teacher}
    Higher education second year students (spé PC)
  \end{block}

  \begin{block}{Software enthusiast}
    Debian Developer (Math, Science, Python, JavaScript and OCaml teams)
  \end{block}

  \begin{block}{Computer scientist}
    Preparing a PhD in computer science with Y.~Bertot in the STAMP team
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{What is my goal?}

  \begin{alertblock}{Core idea}
    Mathematics should be taught, learnt, discussed and invented in a
    proof assistant, from the first year on
  \end{alertblock}

  \begin{block}{Consequences}
    \begin{itemize}
    \item Early definitions and results should not be too abstract
    \item All proofs should be human-readable
    \item A student should be able to implement an exercise without
      guidance (both questions and solutions)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{First target}

  \begin{block}{Problems while experimenting}
    \begin{itemize}
    \item Too many details (scopes, coercions, specific
      operators) in input written expressions
    \item Error messages completely unrelated to the situation
    \end{itemize}
  \end{block}
  \begin{block}{Diagnosis}
    Overloading does not work well enough!
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Informal description of frames}

  In cognitive science, linguistics and artificial intelligence, a
  \textbf{frame} is a conceptual tool to explain how explicit
  information and background knowledge get combined to make sense of a
  situation
\end{frame}

\begin{frame}
  \frametitle{Informal description of framed typing}

  \begin{block}{Mathematical text}
    \begin{itemize}
    \item\textbf{Declaration} We introduce/declare objects and their
      properties and build a frame - setting the stage where the
      reader will be comfortable
    \item\textbf{Usage} We consider expressions and prove properties
      of objects - everything is understood within the frame
    \end{itemize}
  \end{block}

  \begin{block}{Gains for proof assistants}
    \begin{itemize}
    \item Automatic sandboxing
    \item Fail early - report precisely
    \end{itemize}
  \end{block}

\end{frame}
\begin{frame}
  \frametitle{What is a frame for elaboration?}

  \begin{block}{Poset of sets}
    The elements are the relevant sets in the discussion, with
    comparisons given by the known coercion maps

    The default should probably be:
    \[
      \NN\rightarrow\ZZ\rightarrow\QQ\rightarrow\RR
    \]
  \end{block}

  \begin{block}{Symbol store}
    Each symbol is stored associated to the \textbf{ordered} list of
    its instances, e.g. $+$ can be associated to
    $+_\NN;\dots;+_\RR$
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{How to sort symbol instances?}

  \begin{alertblock}{Problem}
    A coercion $\NN\rightarrow\ZZ$ doesn't give a comparison for
    $+_\NN$ and $+_\ZZ$ because arrows are naturally covariant on the
    target and contravariant on the source
  \end{alertblock}

  \begin{exampleblock}{Solution}
    Operators and predicates come attached to a main object, and the
    frame can compare these: $+_\NN$ is simpler than $+_\ZZ$
  \end{exampleblock}

  \begin{block}{Symbol definition}
    \begin{itemize}
    \item a name (e.g.: $+$)
    \item an ordered list of pairs (instance, level)

      (e.g. $(+_\NN,\NN),\dots,(+_\RR,\RR)$)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Ambiguity: unavoidable}

  In the frame $\NN\rightarrow\ZZ\rightarrow\RR$, the expression
  $(m+n)+a=b$ with $m,n\in\NN$ and $a,b\in\ZZ$:

  \begin{figure}
    \begin{tikzpicture}[scale=0.6]
      \node (equal) at (3, 3) {$=_\RR$};
      \node (b) at (4,2) {$b$};
      \node (plush) at (2, 2) {$+_\ZZ$};
      \node (plusb) at (1, 1) {$+_\NN$};
      \node (m) at (0, 0) {$m$};
      \node (n) at (2, 0) {$n$};
      \node (a) at (3, 1) {$a$};
      \draw (equal) -- node[text=blue, scale=0.5, left] {$\ZZ\uparrow\RR$} (plush);
      \draw (equal) -- node[text=blue, scale=0.5, right] {$\ZZ\uparrow\RR$} (b);
      \draw (plush) -- node[text=blue, scale=0.5, left] {$\NN\uparrow\ZZ$} (plusb);
      \draw (plush) -- (a);
      \draw (plusb) -- (m);
      \draw (plusb) -- (n);
      \node[block, fit=(equal)(m)(n)(a)(b)] {};
    \end{tikzpicture}
    \begin{tikzpicture}[scale=0.6]
      \node (equal) at (3, 3) {$=_\RR$};
      \node (b) at (4,2) {$b$};
      \node (plush) at (2, 2) {$+_\RR$};
      \node (plusb) at (1, 1) {$+_\RR$};
      \node (m) at (0, 0) {$m$};
      \node (n) at (2, 0) {$n$};
      \node (a) at (3, 1) {$a$};
      \draw (equal) -- (plush);
      \draw (equal) -- node[text=blue, scale=0.5, right] {$\ZZ\uparrow\RR$} (b);
      \draw (plush) -- (plusb);
      \draw (plush) -- node[text=blue, scale=0.5, right] {$\ZZ\uparrow\RR$} (a);
      \draw (plusb) -- node[text=blue, scale=0.5, left] {$\NN\uparrow\RR$} (m);
      \draw (plusb) -- node[text=blue, scale=0.5, right] {$\NN\uparrow\RR$} (n);
      \node[block, fit=(equal)(m)(n)(a)(b)] {};
    \end{tikzpicture}
    \begin{tikzpicture}[scale=0.6]
      \node (equal) at (3, 3) {$=_\RR$};
      \node (b) at (4,2) {$b$};
      \node (plush) at (2, 2) {$+_\RR$};
      \node (plusb) at (1, 1) {$+_\ZZ$};
      \node (m) at (0, 0) {$m$};
      \node (n) at (2, 0) {$n$};
      \node (a) at (3, 1) {$a$};
      \draw (equal) -- (plush);
      \draw (equal) -- node[text=blue, scale=0.5, right] {$\ZZ\uparrow\RR$} (b);
      \draw (plush) -- node[text=blue, scale=0.5, left] {$\ZZ\uparrow\RR$} (plusb);
      \draw (plush) -- node[text=blue, scale=0.5, right] {$\ZZ\uparrow\RR$} (a);
      \draw (plusb) -- node[text=blue, scale=0.5, left] {$\NN\uparrow\ZZ$} (m);
      \draw (plusb) -- node[text=blue, scale=0.5, right] {$\NN\uparrow\ZZ$} (n);
      \node[block, fit=(equal)(m)(n)(a)(b)] {};
    \end{tikzpicture}
    \begin{tikzpicture}[scale=0.6]
      \node (equal) at (3, 3) {$=_\ZZ$};
      \node (b) at (4,2) {$b$};
      \node (plush) at (2, 2) {$+_\ZZ$};
      \node (plusb) at (1, 1) {$+_\ZZ$};
      \node (m) at (0, 0) {$m$};
      \node (n) at (2, 0) {$n$};
      \node (a) at (3, 1) {$a$};
      \draw (equal) -- (plush);
      \draw (equal) -- (b);
      \draw (plush) -- (plusb);
      \draw (plush) -- (a);
      \draw (plusb) -- node[text=blue, scale=0.5, left] {$\NN\uparrow\ZZ$} (m);
      \draw (plusb) -- node[text=blue, scale=0.5, right] {$\NN\uparrow\ZZ$} (n);
      \node[block, fit=(equal)(m)(n)(a)(b)] {};
    \end{tikzpicture}
    \begin{tikzpicture}[scale=0.6]
      \node (equal) at (3, 3) {$=_\ZZ$};
      \node (b) at (4,2) {$b$};
      \node (plush) at (2, 2) {$+_\ZZ$};
      \node (plusb) at (1, 1) {$+_\NN$};
      \node (m) at (0, 0) {$m$};
      \node (n) at (2, 0) {$n$};
      \node (a) at (3, 1) {$a$};
      \draw (equal) -- (plush);
      \draw (equal) -- (b);
      \draw (plush) -- node[text=blue, scale=0.5, left] {$\NN\uparrow\ZZ$} (plusb);
      \draw (plush) -- (a);
      \draw (plusb) -- (n);
      \draw (plusb) -- (m);
      \node[block, fit=(equal)(m)(n)(a)(b)] {};
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Example}
  \framesubtitle{$(m+n)+a$ with $(m,n,a)\in\NN\times\NN\times\ZZ$ and $\NN\rightarrow\ZZ$}

  \begin{overlayarea}{12cm}{7cm}
    \only<1>{
      \begin{figure}
        \begin{tikzpicture}
          \node (plush) at (2, 2) {$+$};
          \node (plusb) at (1, 1) {$+$};
          \node (m) at (0, 0) {$m$};
          \node (n) at (2, 0) {$n$};
          \node (a) at (4, 1) {$a$};
          \draw (plush) -- (plusb);
          \draw (plush) -- (a);
          \draw (plusb) -- (m);
          \draw (plusb) -- (n);
        \end{tikzpicture}
      \end{figure}
      User-provided untyped tree
    }
    \only<2>{
      \begin{figure}
        \begin{tikzpicture}
          \node (plush) at (2, 2) {$\{+_\NN;+_\ZZ\}$};
          \node (plusb) at (1, 1) {$\{+_\NN;+_\ZZ\}$};
          \node[draw, block] (m) at (0, 0) {$m: \NN$};
          \node[draw, block] (n) at (2, 0) {$n: \NN$};
          \node[draw, block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- (plusb);
          \draw (plush) -- (a);
          \draw (plusb) -- (m);
          \draw (plusb) -- (n);
        \end{tikzpicture}
      \end{figure}
      Fuzzy tree, \textbf{using the frame for symbol expansion}
    }
    \only<3>{
      \begin{figure}
        \begin{tikzpicture}
          \node (plush) at (2, 2) {$\{+_\NN;+_\ZZ\}$};
          \node (plusb) at (1, 1) {$\{+_\NN;+_\ZZ\}$};
          \node[draw, block] (m) at (0, 0) {$m: \NN$};
          \node[draw, block] (n) at (2, 0) {$n: \NN$};
          \node[draw, block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- (plusb);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}$} (a);
          \draw (plusb) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}$} (m);
          \draw (plusb) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\NN;\ZZ\}$} (n);
        \end{tikzpicture}
      \end{figure}
      Start of the first bottom-top subpass of a bidirectional typing pass

      \textbf{Using the frame to saturate types}
    }
    \only<4>{
      \begin{figure}
        \begin{tikzpicture}
          \node[draw, block] (plush) at (2, 2) {$+_\ZZ$};
          \node (plusb) at (1, 1) {$\{+_\NN;+_\ZZ\}$};
          \node[draw, block] (m) at (0, 0) {$m: \NN$};
          \node[draw, block] (n) at (2, 0) {$n: \NN$};
          \node[draw, block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\ZZ\}$} (plusb);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (a);
          \draw (plusb) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (m);
          \draw (plusb) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (n);
        \end{tikzpicture}
      \end{figure}
      End of the first bottom-top subpass of a bidirectional typing pass
    }
    \only<5>{
      \begin{figure}
        \begin{tikzpicture}
          \node[draw, block] (plush) at (2, 2) {$+_\ZZ$};
          \node (plusb) at (1, 1) {$\{+_\NN;+_\ZZ\}$};
          \node[draw, block] (m) at (0, 0) {$m: \NN$};
          \node[draw, block] (n) at (2, 0) {$n: \NN$};
          \node[draw, block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\ZZ\}$} (plusb);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (a);
          \draw (plusb) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (m);
          \draw (plusb) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (n);
        \end{tikzpicture}
      \end{figure}
      First top-bottom subpass of a bidirectional typing pass

      (Typing is stuck, next bidirectional typing passes give nothing)
    }
    \only<6>{
      \begin{figure}
        \begin{tikzpicture}
          \node[draw, block] (plush) at (2, 2) {$+_\ZZ$};
          \node[draw, block] (plusb) at (1, 1) {$+_\NN$};
          \node[draw, block] (m) at (0, 0) {$m: \NN$};
          \node[draw, block] (n) at (2, 0) {$n: \NN$};
          \node[draw, block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\ZZ\}$} (plusb);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (a);
          \draw (plusb) -- node[text=blue, scale=0.5, above left] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (m);
          \draw (plusb) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\NN;\ZZ\}\downarrow\{\NN;\ZZ\}$} (n);
        \end{tikzpicture}
      \end{figure}

      \textbf{Using the frame we make a priority choice}
    }
    \only<7>{
      \begin{figure}
        \begin{tikzpicture}
          \node[block] (plush) at (2, 2) {$+_\ZZ$};
          \node (plusb) at (1, 1) {$+_\NN$};
          \node (m) at (0, 0) {$m: \NN$};
          \node (n) at (2, 0) {$n: \NN$};
          \node[block, fit=(plusb)(m)(n)] (plusbab) {};
          \node[block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- node[text=blue, scale=0.5, left] {$\uparrow\{\NN\}\downarrow\{\ZZ\}$} (plusbab);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (a);
          \draw (plusb) -- (m);
          \draw (plusb) -- (n);
        \end{tikzpicture}
      \end{figure}

      New bidirectional typing pass, start of the bottom-top subpass

      \textbf{Using the frame to coerce}
    }
    \only<8>{
      \begin{figure}
        \begin{tikzpicture}
          \node[block] (plush) at (2, 2) {$+_\ZZ$};
          \node (ZofN) at (1, 1) {$\langle\NN\rightarrow\ZZ\rangle$};
          \node (plusb) at (1, 0) {$+_\NN$};
          \node (m) at (0, -1) {$m: \NN$};
          \node (n) at (2, -1) {$n: \NN$};
          \node[block, fit=(ZofN)(plusb)(m)(n)] (ZofNplusbmn) {};
          \node[block] (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- node[text=blue, scale=0.5, left] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (ZofNplusbmn);
          \draw (ZofN) -- (plusb);
          \draw (plush) -- node[text=blue, scale=0.5, above right] {$\uparrow\{\ZZ\}\downarrow\{\ZZ\}$} (a);
          \draw (plusb) -- (m);
          \draw (plusb) -- (n);
        \end{tikzpicture}
      \end{figure}

      New bidirectional typing pass, middle of the bottom-top subpass
    }
    \only<9>{
      \begin{figure}
        \begin{tikzpicture}
          \node (plush) at (2, 2) {$+_\ZZ$};
          \node (ZofN) at (1, 1) {$\langle\NN\rightarrow\ZZ\rangle$};
          \node (plusb) at (1, 0) {$+_\NN$};
          \node (m) at (0, -1) {$m: \NN$};
          \node (n) at (2, -1) {$n: \NN$};
          \node (a) at (4, 1) {$a: \ZZ$};
          \draw (plush) -- (ZofN);
          \draw (ZofN) -- (plusb);
          \draw (plush) -- (a);
          \draw (plusb) -- (m);
          \draw (plusb) -- (n);
          \node[block, fit=(plush)(ZofN)(plusb)(m)(n)(a)] {};
        \end{tikzpicture}
      \end{figure}

      New bidirectional typing pass, end of the bottom-top subpass

      Final fully typed tree
    }
  \end{overlayarea}
\end{frame}

\begin{frame}[fragile] % needed for verbatim
  \frametitle{Example}
  \framesubtitle{$0+0\times v_0$}
\begin{Verbatim}[fontsize=\tiny]
#input syllabus.eve

declare_vector_space E K.
declare_variable v (N -> E).

check (0 + 0 * v 0)
      "(Eadd E0 (Eext_mul (K_of_Z (Z_of_N N0)) (v N0))): E".
\end{Verbatim}

\end{frame}

\begin{frame}[fragile] % needed for verbatim
  \frametitle{Example}
  \framesubtitle{Vectors}

\begin{Verbatim}[fontsize=\tiny]
#input syllabus.eve

declare_subvector_space F E K.
declare_subvector_space G E K.

declare_variable f F.
declare_variable g G.
declare_variable k K.

check (f + 0)
      "(Fadd f F0): F".

-- mixed sums make sense in the ambient space
check (f + g)
      "(Eadd (E_of_F f) (E_of_G g)): E".

check (k * 0)
      "(Kmul k (K_of_Z (Z_of_N N0))): K".

check (k * f)
      "(Fext_mul k f): F".

check (k * 0 + f)
      "(Fadd (Fext_mul k F0) f): F".
\end{Verbatim}
\end{frame}

\begin{frame}[fragile] % needed for verbatim
  \frametitle{Example}
  \framesubtitle{Functions}

\begin{Verbatim}[fontsize=\tiny]
#input syllabus.eve

declare_variable I Type.
declare_numeric_functions I K.
declare_variable f (I -> K).
declare_variable g (I -> K).
declare_variable h (I -> N).
declare_variable k K.

check (add f)
      "(FIKadd f): (I -> K) -> I -> K".

check (f + g)
      "(FIKadd f g): I -> K".

check (f * g)
      "(FIKmul f g): I -> K".

check (f + 0)
      "(FIKadd f (FIK_of_K (K_of_Z (Z_of_N N0)))): I -> K".

check (k * f)
      "(FIKext_mul k f): I -> K".

check (f + h)
      "(FIKadd f (compose K_of_Z (compose Z_of_N h))): I -> K".
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Future work}

  \begin{block}{}
    Inclusion in a proof assistant
  \end{block}

  \begin{block}{}
    Better error messages
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Conclusion}

  \begin{block}{Thanks}
    \begin{itemize}
    \item Yves Bertot for advising
    \item STAMP team for funding my participation to this event
    \item The audience for its attention
    \end{itemize}
  \end{block}

  \begin{block}{Questions?}
  \end{block}

  \begin{block}{Code}
    \url{https://framagit.org/jpuydt/framed_typing/-/tree/master/20240418}
  \end{block}
\end{frame}
\end{document}