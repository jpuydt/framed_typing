% $ biblatex auxiliary file $
% $ biblatex bbl format version 3.3 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated by
% biber as required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup


\refsection{0}
  \datalist[entry]{nty/global//global/global/global}
    \entry{AvigadKongMouraRoux2015a}{article}{}{}
      \name{author}{4}{}{%
        {{hash=cfd67bda8a38e3422bc1696aae392c66}{%
           family={Avigad},
           familyi={A\bibinitperiod},
           given={J.},
           giveni={J\bibinitperiod}}}%
        {{hash=22c0a7d471410232616ada5b12888cc0}{%
           family={Kong},
           familyi={K\bibinitperiod},
           given={S.},
           giveni={S\bibinitperiod}}}%
        {{hash=8ce49a43cef86f4f7bc0b4f345c8236f}{%
           family={Moura},
           familyi={M\bibinitperiod},
           given={L.\bibnamedelimi de},
           giveni={L\bibinitperiod\bibinitdelim d\bibinitperiod}}}%
        {{hash=c07ab98ea13770bc05daaefc3b7ce007}{%
           family={Roux},
           familyi={R\bibinitperiod},
           given={C.},
           giveni={C\bibinitperiod}}}%
      }
      \strng{namehash}{1f04982b009626bfbdb341a4b4b47444}
      \strng{fullhash}{72a5597557755a2c7456006494b66c1f}
      \strng{fullhashraw}{72a5597557755a2c7456006494b66c1f}
      \strng{bibnamehash}{1f04982b009626bfbdb341a4b4b47444}
      \strng{authorbibnamehash}{1f04982b009626bfbdb341a4b4b47444}
      \strng{authornamehash}{1f04982b009626bfbdb341a4b4b47444}
      \strng{authorfullhash}{72a5597557755a2c7456006494b66c1f}
      \strng{authorfullhashraw}{72a5597557755a2c7456006494b66c1f}
      \field{sortinit}{A}
      \field{sortinithash}{2f401846e2029bad6b3ecc16d50031e2}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{C}omputer research repository}
      \field{title}{{E}laboration in dependent type theory}
      \field{year}{2015}
      \verb{urlraw}
      \verb https://arxiv.org/abs/1505.04324
      \endverb
      \verb{url}
      \verb https://arxiv.org/abs/1505.04324
      \endverb
    \endentry
    \entry{CarlCramerFisseniSarikaryaSchroeder2021a}{article}{}{}
      \name{author}{5}{}{%
        {{hash=30d70e2a9325bc07f71bea5109124bee}{%
           family={Carl},
           familyi={C\bibinitperiod},
           given={M.},
           giveni={M\bibinitperiod}}}%
        {{hash=bbfccc3bc81330bb40d9b2f1685e0249}{%
           family={Cramer},
           familyi={C\bibinitperiod},
           given={M.},
           giveni={M\bibinitperiod}}}%
        {{hash=fcd99a35afe3cf3f993dbc0fa7fb0507}{%
           family={Fisseni},
           familyi={F\bibinitperiod},
           given={B.},
           giveni={B\bibinitperiod}}}%
        {{hash=9a2f94d2eb16d2d5a2976e0c5e4a37b8}{%
           family={Sarikaya},
           familyi={S\bibinitperiod},
           given={D.},
           giveni={D\bibinitperiod}}}%
        {{hash=a169fa1850522e19b856191e96ce22dd}{%
           family={Schröder},
           familyi={S\bibinitperiod},
           given={B.},
           giveni={B\bibinitperiod}}}%
      }
      \strng{namehash}{454fa850ec2b06478f02ae5e5e120f61}
      \strng{fullhash}{a634462654c5d4f6af60258b61139f5f}
      \strng{fullhashraw}{a634462654c5d4f6af60258b61139f5f}
      \strng{bibnamehash}{454fa850ec2b06478f02ae5e5e120f61}
      \strng{authorbibnamehash}{454fa850ec2b06478f02ae5e5e120f61}
      \strng{authornamehash}{454fa850ec2b06478f02ae5e5e120f61}
      \strng{authorfullhash}{a634462654c5d4f6af60258b61139f5f}
      \strng{authorfullhashraw}{a634462654c5d4f6af60258b61139f5f}
      \field{sortinit}{C}
      \field{sortinithash}{4d103a86280481745c9c897c925753c0}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{A}xiomathes}
      \field{title}{{H}ow to frame understanding in mathematics: a case study using extremal proofs}
      \field{volume}{31}
      \field{year}{2021}
      \field{pages}{649\bibrangedash 676}
      \range{pages}{28}
      \verb{urlraw}
      \verb https://doi.org/10.1007/s10516-021-09552-9
      \endverb
      \verb{url}
      \verb https://doi.org/10.1007/s10516-021-09552-9
      \endverb
    \endentry
    \entry{Chargueraud2023a}{preprint}{}{}
      \name{author}{1}{}{%
        {{hash=1c85e2ae338c7a8d125c468acf7a6078}{%
           family={Charguéraud},
           familyi={C\bibinitperiod},
           given={A.},
           giveni={A\bibinitperiod}}}%
      }
      \strng{namehash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{fullhash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{fullhashraw}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{bibnamehash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{authorbibnamehash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{authornamehash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{authorfullhash}{1c85e2ae338c7a8d125c468acf7a6078}
      \strng{authorfullhashraw}{1c85e2ae338c7a8d125c468acf7a6078}
      \field{sortinit}{C}
      \field{sortinithash}{4d103a86280481745c9c897c925753c0}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{note}{{P}reprint}
      \field{title}{{T}yping of overloading in programming languages and mechanized mathematics}
      \field{year}{2023}
    \endentry
    \entry{CohenSakaguchiTassi2020a}{inproceedings}{}{}
      \name{author}{3}{}{%
        {{hash=3f61874ac06748b014aeaf13d5df47e2}{%
           family={Cohen},
           familyi={C\bibinitperiod},
           given={C.},
           giveni={C\bibinitperiod}}}%
        {{hash=cef71dc1598c552462608c4638403884}{%
           family={Sakaguchi},
           familyi={S\bibinitperiod},
           given={K.},
           giveni={K\bibinitperiod}}}%
        {{hash=20ca95214114c54df3855f45f33f0d0c}{%
           family={Tassi},
           familyi={T\bibinitperiod},
           given={E.},
           giveni={E\bibinitperiod}}}%
      }
      \strng{namehash}{b3743339df421037714b65ca70ff4279}
      \strng{fullhash}{b3743339df421037714b65ca70ff4279}
      \strng{fullhashraw}{b3743339df421037714b65ca70ff4279}
      \strng{bibnamehash}{b3743339df421037714b65ca70ff4279}
      \strng{authorbibnamehash}{b3743339df421037714b65ca70ff4279}
      \strng{authornamehash}{b3743339df421037714b65ca70ff4279}
      \strng{authorfullhash}{b3743339df421037714b65ca70ff4279}
      \strng{authorfullhashraw}{b3743339df421037714b65ca70ff4279}
      \field{sortinit}{C}
      \field{sortinithash}{4d103a86280481745c9c897c925753c0}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{booktitle}{{F}ifth international conference on formal structures for computation and deduction ({FSCD}2020)}
      \field{series}{{L}eibniz international proceedings in informatics}
      \field{title}{{H}ierarchy builder: algebraic hierarchies made easy in {C}oq with {E}lpi}
      \field{volume}{167}
      \field{year}{2020}
      \field{pages}{651\bibrangedash 671}
      \range{pages}{21}
      \verb{urlraw}
      \verb https://doi.org/10.4230/LIPIcs.FSCD.2020.34
      \endverb
      \verb{url}
      \verb https://doi.org/10.4230/LIPIcs.FSCD.2020.34
      \endverb
    \endentry
    \entry{Crance2023a}{thesis}{}{}
      \name{author}{1}{}{%
        {{hash=b5b81d70bb8b0dc13d827803e02c4e5b}{%
           family={Crance},
           familyi={C\bibinitperiod},
           given={E.},
           giveni={E\bibinitperiod}}}%
      }
      \list{institution}{1}{%
        {{N}antes {U}niversité}%
      }
      \strng{namehash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{fullhash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{fullhashraw}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{bibnamehash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{authorbibnamehash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{authornamehash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{authorfullhash}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \strng{authorfullhashraw}{b5b81d70bb8b0dc13d827803e02c4e5b}
      \field{sortinit}{C}
      \field{sortinithash}{4d103a86280481745c9c897c925753c0}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{{M}éta-programmation pour le transfert de preuve en théorie des types dépendants}
      \field{type}{phdthesis}
      \field{year}{2023}
      \verb{urlraw}
      \verb https://theses.hal.science/tel-04719004
      \endverb
      \verb{url}
      \verb https://theses.hal.science/tel-04719004
      \endverb
    \endentry
    \entry{DunfieldKrishnaswami2021a}{article}{}{}
      \name{author}{2}{}{%
        {{hash=4b8dc3c702f3598d599778558c6c8031}{%
           family={Dunfield},
           familyi={D\bibinitperiod},
           given={J.},
           giveni={J\bibinitperiod}}}%
        {{hash=12aa5401488ef8279a90962890dd4157}{%
           family={Krishnaswami},
           familyi={K\bibinitperiod},
           given={N.},
           giveni={N\bibinitperiod}}}%
      }
      \strng{namehash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{fullhash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{fullhashraw}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{bibnamehash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{authorbibnamehash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{authornamehash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{authorfullhash}{814f2cff8cbb6832a7183ddcab39e302}
      \strng{authorfullhashraw}{814f2cff8cbb6832a7183ddcab39e302}
      \field{sortinit}{D}
      \field{sortinithash}{6f385f66841fb5e82009dc833c761848}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{ACM} {C}omputing surveys}
      \field{number}{5}
      \field{title}{{B}idirectional typing}
      \field{volume}{54}
      \field{year}{2021}
      \field{pages}{1\bibrangedash 38}
      \range{pages}{38}
      \verb{urlraw}
      \verb https://arxiv.org/abs/1908.05839
      \endverb
      \verb{url}
      \verb https://arxiv.org/abs/1908.05839
      \endverb
    \endentry
    \entry{LewisMadelaine2020a}{article}{}{}
      \name{author}{2}{}{%
        {{hash=d7dbafc924e355e2e28cf5233132cfab}{%
           family={Lewis},
           familyi={L\bibinitperiod},
           given={R.\bibnamedelimi Y.},
           giveni={R\bibinitperiod\bibinitdelim Y\bibinitperiod}}}%
        {{hash=a76ed6a1ccb12ae7996effcad3b993c5}{%
           family={Madelaine},
           familyi={M\bibinitperiod},
           given={P.-N.},
           giveni={P\bibinithyphendelim N\bibinitperiod}}}%
      }
      \strng{namehash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{fullhash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{fullhashraw}{b3fab2286b127da6d6036f812810bf2f}
      \strng{bibnamehash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{authorbibnamehash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{authornamehash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{authorfullhash}{b3fab2286b127da6d6036f812810bf2f}
      \strng{authorfullhashraw}{b3fab2286b127da6d6036f812810bf2f}
      \field{sortinit}{L}
      \field{sortinithash}{7c47d417cecb1f4bd38d1825c427a61a}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{P}ractical aspects of automated reasoning}
      \field{title}{{S}implifying casts and coercions}
      \field{year}{2020}
      \field{pages}{53\bibrangedash 62}
      \range{pages}{10}
      \verb{urlraw}
      \verb https://arxiv.org/abs/2001.10594
      \endverb
      \verb{url}
      \verb https://arxiv.org/abs/2001.10594
      \endverb
    \endentry
    \entry{OwreRushbyShankar1998a}{article}{}{}
      \name{author}{3}{}{%
        {{hash=1df87ba07d3a2825663f160d63eb8728}{%
           family={Owre},
           familyi={O\bibinitperiod},
           given={S.},
           giveni={S\bibinitperiod}}}%
        {{hash=c30bf30faef38e121405750ec145182f}{%
           family={Rushby},
           familyi={R\bibinitperiod},
           given={J.},
           giveni={J\bibinitperiod}}}%
        {{hash=e0e60a40e21d1dac99144b7db4928fa3}{%
           family={Shankar},
           familyi={S\bibinitperiod},
           given={N.},
           giveni={N\bibinitperiod}}}%
      }
      \strng{namehash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{fullhash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{fullhashraw}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{bibnamehash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{authorbibnamehash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{authornamehash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{authorfullhash}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \strng{authorfullhashraw}{227bc1b8973db1e5e16aa5c1b60a31ff}
      \field{sortinit}{O}
      \field{sortinithash}{2cd7140a07aea5341f9e2771efe90aae}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{IEEE} transactions on software engineering}
      \field{number}{9}
      \field{title}{{S}ubtypes for specifications: predicate subtyping in {PVS}}
      \field{volume}{24}
      \field{year}{1998}
      \field{pages}{709\bibrangedash 720}
      \range{pages}{12}
      \verb{urlraw}
      \verb https://ieeexplore.ieee.org/document/713327
      \endverb
      \verb{url}
      \verb https://ieeexplore.ieee.org/document/713327
      \endverb
    \endentry
    \entry{SozeauOury2008a}{article}{}{}
      \name{author}{2}{}{%
        {{hash=ad95772bd765d00efa614333f78efa4d}{%
           family={Sozeau},
           familyi={S\bibinitperiod},
           given={M.},
           giveni={M\bibinitperiod}}}%
        {{hash=b46f84dc221efcf1914b108e35684b12}{%
           family={Oury},
           familyi={O\bibinitperiod},
           given={N.},
           giveni={N\bibinitperiod}}}%
      }
      \strng{namehash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{fullhash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{fullhashraw}{9c736fcfad16110d581d1c182fda4b13}
      \strng{bibnamehash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{authorbibnamehash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{authornamehash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{authorfullhash}{9c736fcfad16110d581d1c182fda4b13}
      \strng{authorfullhashraw}{9c736fcfad16110d581d1c182fda4b13}
      \field{sortinit}{S}
      \field{sortinithash}{b164b07b29984b41daf1e85279fbc5ab}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{T}heorem proving in higher order logics}
      \field{title}{{F}irst-class type classes}
      \field{year}{2008}
      \field{pages}{278\bibrangedash 293}
      \range{pages}{16}
      \verb{urlraw}
      \verb https://hal.science/inria-00628864/
      \endverb
      \verb{url}
      \verb https://hal.science/inria-00628864/
      \endverb
    \endentry
    \entry{SozeauZiliani2015a}{article}{}{}
      \name{author}{2}{}{%
        {{hash=ad95772bd765d00efa614333f78efa4d}{%
           family={Sozeau},
           familyi={S\bibinitperiod},
           given={M.},
           giveni={M\bibinitperiod}}}%
        {{hash=ef135fab0fb1f37a9a4283f391c0c61f}{%
           family={Ziliani},
           familyi={Z\bibinitperiod},
           given={B.},
           giveni={B\bibinitperiod}}}%
      }
      \strng{namehash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{fullhash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{fullhashraw}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{bibnamehash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{authorbibnamehash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{authornamehash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{authorfullhash}{ccff50e159e77711c4c0cc2deb0631c3}
      \strng{authorfullhashraw}{ccff50e159e77711c4c0cc2deb0631c3}
      \field{sortinit}{S}
      \field{sortinithash}{b164b07b29984b41daf1e85279fbc5ab}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{{SIGPLAN} {N}otices}
      \field{number}{9}
      \field{title}{{A} unification algorithm for {C}oq featuring universe polymorphism and overloading}
      \field{volume}{50}
      \field{year}{2015}
      \field{pages}{179\bibrangedash 191}
      \range{pages}{13}
      \verb{urlraw}
      \verb http://dx.doi.org/10.1145/2858949.2784751
      \endverb
      \verb{url}
      \verb http://dx.doi.org/10.1145/2858949.2784751
      \endverb
    \endentry
    \entry{CoqRef}{book}{}{}
      \name{author}{1}{}{%
        {{hash=d47c52d4add3e3728b111d5bc2db76ec}{%
           family={Team},
           familyi={T\bibinitperiod},
           given={The\bibnamedelimb Coq\bibnamedelima Development},
           giveni={T\bibinitperiod\bibinitdelim C\bibinitperiod\bibinitdelim D\bibinitperiod}}}%
      }
      \strng{namehash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{fullhash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{fullhashraw}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{bibnamehash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{authorbibnamehash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{authornamehash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{authorfullhash}{d47c52d4add3e3728b111d5bc2db76ec}
      \strng{authorfullhashraw}{d47c52d4add3e3728b111d5bc2db76ec}
      \field{sortinit}{T}
      \field{sortinithash}{9af77f0292593c26bde9a56e688eaee9}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{{C}oq reference manual}
      \field{year}{2024}
      \verb{urlraw}
      \verb https://zenodo.org/records/11551307
      \endverb
      \verb{url}
      \verb https://zenodo.org/records/11551307
      \endverb
    \endentry
  \enddatalist
\endrefsection
\endinput

