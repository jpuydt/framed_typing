(rule
  (target basic.log)
  (deps basic.v)
  (action (with-stdout-to basic.log (run coqc basic.v))))

(rule
  (target basic.v)
  (deps syllabus.eve basic.eve)
  (action (run ../bin/eve.exe basic.eve)))

(rule
  (target filters.log)
  (deps filters.v)
  (action (with-stdout-to filters.log (run coqc filters.v))))

(rule
  (target filters.v)
  (deps syllabus.eve filters.eve)
  (action (run ../bin/eve.exe filters.eve)))

(rule
  (target functions.log)
  (deps functions.v)
  (action (with-stdout-to functions.log (run coqc functions.v))))

(rule
  (target functions.v)
  (deps syllabus.eve functions.eve)
  (action (run ../bin/eve.exe functions.eve)))

(rule
  (target inequalities.log)
  (deps inequalities.v)
  (action (with-stdout-to inequalities.log (run coqc inequalities.v))))

(rule
  (target inequalities.v)
  (deps syllabus.eve inequalities.eve)
  (action (run ../bin/eve.exe inequalities.eve)))

(rule
  (target letin.log)
  (deps letin.v)
  (action (with-stdout-to letin.log (run coqc letin.v))))

(rule
  (target letin.v)
  (deps syllabus.eve letin.eve)
  (action (run ../bin/eve.exe letin.eve)))

(rule
  (target polymorphic.log)
  (deps polymorphic.v)
  (action (with-stdout-to polymorphic.log (run coqc polymorphic.v))))

(rule
  (target polymorphic.v)
  (deps syllabus.eve polymorphic.eve)
  (action (run ../bin/eve.exe polymorphic.eve)))

(rule
  (target rings.log)
  (deps rings.v)
  (action (with-stdout-to rings.log (run coqc rings.v))))

(rule
  (target rings.v)
  (deps syllabus.eve rings.eve)
  (action (run ../bin/eve.exe rings.eve)))

(rule
  (target vectors.log)
  (deps vectors.v)
  (action (with-stdout-to vectors.log (run coqc vectors.v))))

(rule
  (target vectors.v)
  (deps syllabus.eve vectors.eve)
  (action (run ../bin/eve.exe vectors.eve)))

(rule
  (target zero.log)
  (deps zero.v)
  (action (with-stdout-to zero.log (run coqc zero.v))))

(rule
  (target zero.v)
  (deps syllabus.eve zero.eve)
  (action (run ../bin/eve.exe zero.eve)))

