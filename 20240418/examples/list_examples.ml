let generate_rules name =
  Printf.printf
    {|(rule
  (target %s.log)
  (deps %s.v)
  (action (with-stdout-to %s.log (run coqc %s.v))))

(rule
  (target %s.v)
  (deps syllabus.eve %s.eve)
  (action (run ../bin/eve.exe %s.eve)))

|} name name name name name name name


let () =
  Sys.readdir "."
  |> Array.to_list
  |> List.sort String.compare
  |> List.filter_map (Filename.chop_suffix_opt ~suffix: ".eve")
  |> List.filter_map (fun name -> if name = "syllabus" then None else Some name)
  |> List.iter generate_rules
