-- macros definitions

new_command declare_addmul name {
  declare_variable name Type.
  declare_variable [name 0] name.
  declare_symbol_instance 0 [name 0] name.
  declare_variable [name add] (name -> name -> name).
  declare_symbol_instance add [name add] name.
  declare_variable [name mul] (name -> name -> name).
  declare_symbol_instance mul [name mul] name.
}.

new_command declare_le name {
  declare_variable [name le] (name -> name -> Prop).
  declare_symbol_instance le [name le] name.
}.

new_command declare_N {
  declare_addmul N.
  declare_le N.
}.

new_command declare_Z {
  declare_N.
  declare_addmul Z.
  declare_le Z.
  declare_variable Z_of_N (N -> Z).
  declare_coercion Z_of_N N Z.
}.

new_command declare_R {
  declare_Z.
  declare_addmul R.
  declare_le R.
  declare_variable R_of_Z (Z -> R).
  declare_coercion R_of_Z Z R.
}.

new_command declare_Rbar {
  declare_R.
  declare_addmul Rbar.
  declare_le Rbar.
  declare_variable Rbar_of_R (R -> Rbar).
  declare_coercion Rbar_of_R R Rbar.
  declare_variable pinftyR Rbar.
  declare_symbol_instance +oo pinftyR Rbar.
  declare_variable minftyR Rbar.
  declare_symbol_instance -oo minftyR Rbar.
}.

new_command declare_C {
  declare_addmul C.
  declare_variable C_of_R (R -> C).
  declare_coercion C_of_R R C.
}.

new_command declare_ring A {
  declare_addmul A.
  declare_variable [A _of_Z] (Z -> A).
  declare_coercion [A _of_Z] Z A.
}.

new_command declare_field K {
  declare_ring K.
}.

new_command declare_quotient_ring A I {
  declare_ring A.
  declare_ring [A quot I].
  declare_variable [A mod I] (A -> AquotI). -- FIXME: [A quot I] should work
  declare_coercion [A mod I] A [A quot I].
}.

new_command declare_vector_space name K {
  declare_field K.
  declare_variable name Type.
  declare_variable [name add] (name -> name -> name).
  declare_symbol_instance add [name add] name.
  declare_variable [name 0] name.
  declare_symbol_instance 0 [name 0] name.
  declare_variable [name ext_mul] (K -> name -> name).
  declare_symbol_instance mul [name ext_mul] name.
}.

new_command declare_subvector_space name E K {
  declare_vector_space E K.
  declare_vector_space name K.
  declare_variable [E _of_ name] (name -> E).
  declare_coercion [E _of_ name] name E.
}.

new_command declare_filters {
  declare_Rbar.
  declare_variable filter (Type -> Type).
  declare_variable tends_to ((?0 -> ?1) -> (filter ?0) -> (filter ?1) -> Prop).
  declare_variable inftyN (filter N).
  declare_symbol_instance +oo inftyN N.
  declare_variable filter_of_Rbar (Rbar -> filter R).
  declare_coercion filter_of_Rbar Rbar (filter R).
}.

new_command declare_numeric_functions I K {
  declare_field K.
  declare_variable [F I K _of_ K] (K -> I -> K).
  declare_coercion [F I K _of_ K] K (I -> K).
  declare_variable [F I K add] ((I -> K) -> (I -> K) -> (I -> K)).
  declare_symbol_instance add [F I K add] (I -> K).
  declare_variable [F I K ext_mul] (K -> (I -> K) -> (I -> K)).
  declare_symbol_instance mul [F I K ext_mul] (I -> K).
  declare_variable [F I K mul] ((I -> K) -> (I -> K) -> (I -> K)).
  declare_symbol_instance mul [F I K mul] (I -> K).
}.

-- active code

declare_variable compose ((?1 -> ?2) -> (?0 -> ?1) -> (?0 -> ?2)).
declare_variable_silent list (?0 -> Type).
declare_variable_silent cons (?0 -> list ?0 -> list ?0).
declare_variable_silent nil (list ?0).
declare_Z.