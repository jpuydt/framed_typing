open Tools

type ident =
  | SimpleIdent of string
  | ConcatIdent of string list

type term =
  | TermApp of term * term list
  | TermProd of term * term
  | TermSum of term * term
  | TermFun of term * term
  | TermLetin of ident * term * term
  | TermAnnot of term * term
  | TermAtom of ident

type command_arg =
  | TermArg of term
  | BlockArg of command list
  | StringArg of string
and command =
  | Command of ident * command_arg list
  | Comment of string

let string_of_ident = function
  | SimpleIdent name -> name
  | ConcatIdent chunks -> "[" ^ (string_of_list (fun s -> s) chunks) ^ "]"

let string_of_term =
  let priority = function
    | TermAtom _ -> 6
    | TermAnnot _ -> 5
    | TermApp _ -> 4
    | TermProd _ -> 3
    | TermSum _ -> 2
    | TermFun _ -> 1
    | TermLetin _ -> 0
  in
  let rec bracket_under term level =
    if priority term <= level
    then "(" ^ (helper term) ^ ")"
    else helper term
  and helper = function
    | TermLetin (ident, def, body) ->
       "let " ^ (string_of_ident ident)
       ^ " := " ^ (bracket_under def 0)
       ^ " in " ^ (bracket_under body 0)
    | TermFun (src, dst) ->
       (bracket_under src 1) ^ " -> " ^ (helper dst)
    | TermSum (left, right) ->
       (bracket_under left 1) ^ " + " ^ (bracket_under right 2)
    | TermProd (left, right) ->
       (bracket_under left 2) ^ " * " ^ (bracket_under right 3)
    | TermApp (fnct, args) ->
       (bracket_under fnct 4) ^ " " ^ (string_of_list (fun arg -> bracket_under arg 4) args)
    | TermAnnot (term, annot) ->
       (bracket_under term 5) ^ " : " ^ (bracket_under annot 5)
    | TermAtom id ->
       (string_of_ident id)
  in
  helper

let rec string_of_command_arg = function
  | TermArg (TermAtom atom) ->
     string_of_ident atom
  | TermArg term ->
     "(" ^ string_of_term term ^ ")"
  | BlockArg body ->
     "{\n"
     ^ (string_of_list ~separator:"" string_of_command body)
     ^ "}\n"
  | StringArg str ->
     "\"" ^ str ^ "\""
and string_of_command = function
  | Command (name, args) ->
     (string_of_ident name) ^ " "
     ^ (string_of_list string_of_command_arg args)
     ^ ".\n"
  | Comment comment ->
     "-- " ^ comment ^ "\n"

open Elaboration.Annot

let rec atype_of_term = function
  | TermApp (TermAtom (SimpleIdent str), args) ->
     AParam (str, List.map atype_of_term args)
  | TermApp _ ->
     raise (Invalid_argument "TermApp's first argument should be a simple ident")
  | TermProd _ ->
     raise (Invalid_argument "FIXME TermProd unsupported in type annotations")
  | TermSum _ ->
     raise (Invalid_argument "FIXME TermSum unsupported in type annotations")
  | TermAnnot _ ->
     raise (Invalid_argument "TermAnnot doesn't make sense in type annotations")
  | TermLetin _ ->
     raise (Invalid_argument "TermLetin doesn't make sense in type annotations")
  | TermFun (src, dst) ->
     AFunct (atype_of_term src, atype_of_term dst)
  | TermAtom (SimpleIdent str) ->
     if String.starts_with ~prefix:"?" str
     then ABruijn (int_of_string (String.sub str 1 (String.length str - 1)))
     else ABasic str
  | TermAtom (ConcatIdent strs) ->
     ABasic (string_of_list ~separator:"" (fun s -> s) strs)

open Elaboration.Untyped

let rec impose uterm atype =
  match uterm with
  | UString (str, _) -> UString (str, Some atype)
  | UApp (fnct, arg, _) -> UApp (fnct, arg, Some atype)
  | ULet (name, def, body) -> ULet (name, def, impose body atype)
  | ULambda (name, annot, body, _) -> ULambda (name, annot, body, Some atype)

let rec uexpr_of_term = function
  | TermApp (fnct, args) ->
     List.fold_left (fun acc arg -> UApp (acc, uexpr_of_term arg, None)) (uexpr_of_term fnct) args
  | TermProd (left, right) ->
     UApp (UApp (UString ("mul", None), uexpr_of_term left, None), uexpr_of_term right, None)
  | TermSum (left, right) ->
     UApp (UApp (UString ("add", None), uexpr_of_term left, None), uexpr_of_term right, None)
  | TermFun (TermAnnot (TermAtom (SimpleIdent name), annot), dst) ->
     ULambda (name, Some (atype_of_term annot), uexpr_of_term dst, None)
  | TermFun (TermAtom (SimpleIdent name), dst) ->
     ULambda (name, None, uexpr_of_term dst, None)
  | TermFun (src, _) ->
     raise (Invalid_argument ("uexpr_of_term unsupported TermFun source: `"
                              ^ (string_of_term src) ^ "`"))
  | TermLetin (ident, def, body) ->
     ULet (string_of_ident ident, uexpr_of_term def, uexpr_of_term body)
  | TermAnnot (term, annot) ->
     impose (uexpr_of_term term) (atype_of_term annot)
  | TermAtom (SimpleIdent str) ->
     UString (str, None)
  | TermAtom (ConcatIdent strs) ->
     UString (string_of_list ~separator:"" (fun s -> s) strs, None)
