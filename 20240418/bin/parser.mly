%token EOF
%token DOT COLON
%token LPAR RPAR LBRACK RBRACK LSQUARE RSQUARE
%token PLUS TIMES ARROW
%token DEFEQ LET IN
%token<string> STRING QUOTED COMMENT

%{
    open Skeleton
%}

%type <command list>commands
%start commands

%%

commands: command* EOF { $1 }

command:
  | COMMENT { Comment $1 }
  | ident command_arg* DOT { Command ($1, $2) }

ident:
  | STRING { SimpleIdent $1 }
  | LSQUARE STRING* RSQUARE { ConcatIdent $2 }

command_arg:
  | ident { TermArg (TermAtom $1) }
  | LPAR term RPAR { TermArg $2 }
  | QUOTED { StringArg $1 }
  | LBRACK command+ RBRACK { BlockArg $2 }

term:
  | term_letin { $1 }

term_letin:
  | LET ident DEFEQ term_fun IN term_fun { TermLetin ($2, $4, $6) }
  | term_fun { $1 }

term_fun:
  | term_annot ARROW term_fun { TermFun ($1, $3) }
  | term_annot { $1 }

term_annot:
  | term_sum COLON term_sum { TermAnnot ($1, $3) }
  | term_sum { $1 }

term_sum:
  | term_sum PLUS term_prod { TermSum ($1, $3) }
  | term_prod { $1 }

term_prod:
  | term_app TIMES term_prod { TermProd ($1, $3) }
  | term_app { $1 }

term_app:
  | atom atom+ { TermApp ($1, $2) }
  | atom { $1 }

atom:
  | LPAR term RPAR { $2 }
  | STRING { TermAtom (SimpleIdent $1) }
