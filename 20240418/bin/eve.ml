open Skeleton
open Tools

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F
module E = Elaboration.Elaborate.Make(T)

module NotTrie = Map.Make(String)

type context =
  {
    frame: F.t;
    mutable already_run: (string * command_arg list) list;
    mutable interpreters: (string * (context -> command_arg list -> unit)) list
  }

let check context term expected =
  let uexpr = uexpr_of_term term in
  let res = E.elaborate context.frame uexpr in
  let s_res = string_of_expr context.frame.context res
  and t_res = string_of_ttype context.frame.context (ttype_of_expr context.frame.context res)
  in
  let result = "(" ^ s_res ^ "): " ^ t_res in
  if not (String.equal result expected) then
    raise (Invalid_argument ("interpret_check for `"
                                       ^ (string_of_term term) ^ "`"
                                       ^ " gave `"
                                       ^ result ^ "`"
                                       ^ " where `"
                                       ^ expected ^ "` was expected"));
  log context.frame.context ("Check " ^ result ^ ".\n")

let fail_check context term expected =
  let error = ref true in
  let uexpr = uexpr_of_term term in
  try
    let res = E.elaborate context.frame uexpr in
    ignore res;
    error := false;
    raise (Invalid_argument "it worked...")
  with Invalid_argument msg ->
    if not !error then
      log context.frame.context ("(* bad: " ^ msg ^ " *)\n")
    else
      if String.equal msg expected
      then log context.frame.context ("(* as expected it failed with: " ^ msg ^ " *)\n")
      else raise (Invalid_argument ("fail_check with message `" ^ msg ^ "`"
                                    ^ " but we expected: `" ^ expected ^ "`"))

let interpret_command context = function
  | Command (ident, args) ->
     let name = string_of_ident ident in
     if not (List.mem (name, args) context.already_run) then
       begin
         context.already_run <- (name, args) :: context.already_run;
         try
           let interpreter = List.assoc name context.interpreters in
           interpreter context args
         with Not_found -> raise (Invalid_argument ("interpret_command failed for '" ^ name ^ "'"))
       end
  | Comment comment ->
     log context.frame.context ("\n(* " ^ comment ^ " *)\n")

let rec preparse_in_buffer buffer name =
  let input_pragma = "#input " in
  let treat line =
    if String.starts_with ~prefix:input_pragma line then
      begin
        preparse_in_buffer buffer (String.sub line (String.length input_pragma)
                                     (String.length line - String.length input_pragma))
      end
    else
      Buffer.add_string buffer (line ^ "\n")
  in
  let input = open_in name in
  try
    while true; do
      treat (input_line input)
    done
  with
    End_of_file ->
    close_in input

let commands_of_file name =
  let buffer = Buffer.create 0 in
  let _ = preparse_in_buffer buffer name in
  let lexbuf = Sedlexing.Utf8.from_string (Buffer.contents buffer) in
  let lexer = Sedlexing.with_tokenizer Lexer.token lexbuf in
  let parser = MenhirLib.Convert.Simplified.traditional2revised Parser.commands in
  parser lexer

let interpret_declare_coercion (context: context) = function
  | [name; src; dst] ->
     begin
       match name, src, dst with
       | TermArg (TermAtom (SimpleIdent name)), TermArg _(*src*), TermArg _(*dst*)
         ->
          add_coercion context.frame name
       | _ ->
          raise (Invalid_argument "interpret_declare_coercion expects a simple name")
     end
  | args ->
     raise (Invalid_argument ("interpret_declare_coercion wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_declare_symbol_instance (context: context) = function
  | [name; instance; level] ->
     begin
       match name, instance, level with
       | TermArg (TermAtom (SimpleIdent name)),
         TermArg (TermAtom (SimpleIdent var)),
         TermArg level ->
          begin
            match T.read_variable context.frame.context var with
            | Some expr ->
               add_symbol context.frame name
                 (T.ttype_of_atype context.frame (atype_of_term level)) expr
            | None ->
               raise (Invalid_argument ("interpret_declare_symbol_instance: '" ^ var ^ "' is unknown"))
          end
       | _ ->
          raise (Invalid_argument ("interpret_declare_symbol_instance wrong arguments, got:"
                                   ^ " " ^ (string_of_command_arg name)
                                   ^ " " ^ (string_of_command_arg instance)
                                   ^ " " ^ (string_of_command_arg level)))
     end
  | args ->
     raise (Invalid_argument ("interpret_declare_symbol_instance wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_declare_variable (context: context) = function
  | [name; typ] ->
     begin
       match name, typ with
       | TermArg (TermAtom (SimpleIdent name)), TermArg typ ->
          let ttype = ttype_of_atype context.frame (atype_of_term typ) in
          log context.frame.context ("Variable " ^ name ^ ": "
                                     ^ (string_of_ttype context.frame.context ttype) ^ ".\n");
          add_variable context.frame name ttype
       | _ ->
          raise (Invalid_argument ("interpret_declare_variable wrong arguments, got:"
                                   ^ " " ^ (string_of_command_arg name)
                                   ^ " " ^ (string_of_command_arg typ)))
     end
  | args ->
     raise (Invalid_argument ("interpret_declare_variable wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_declare_variable_silent (context: context) = function
  | [name; typ] ->
     begin
       match name, typ with
       | TermArg (TermAtom (SimpleIdent name)), TermArg typ ->
          let ttype = ttype_of_atype context.frame (atype_of_term typ) in
          add_variable context.frame name ttype
       | _ ->
          raise (Invalid_argument ("interpret_declare_variable wrong arguments, got:"
                                   ^ " " ^ (string_of_command_arg name)
                                   ^ " " ^ (string_of_command_arg typ)))
     end
  | args ->
     raise (Invalid_argument ("interpret_declare_variable wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_check (context: context) = function
  | [arg1; arg2] ->
     begin
       match arg1, arg2 with
       | TermArg (term), StringArg(expected) ->
          check context term expected
       | _ -> raise (Invalid_argument ("interpret_check wrong argument, got `"
                                       ^ (string_of_command_arg arg1) ^ "`"
                                       ^ " and `"
                                       ^ (string_of_command_arg arg2) ^ "`"))
     end
  | args ->
     raise (Invalid_argument ("interpret_check wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_fail_check (context: context) = function
  | [arg1; arg2] ->
     begin
       match arg1, arg2 with
       | TermArg (term), StringArg(expected) ->
          fail_check context term expected
       | _ -> raise (Invalid_argument ("interpret_fail_check wrong argument, got `"
                                       ^ (string_of_command_arg arg1) ^ "`"
                                       ^ " and `"
                                       ^ (string_of_command_arg arg2) ^ "`"))
     end
  | args ->
     raise (Invalid_argument ("interpret_fail_check wrong arguments, got: "
                              ^ (string_of_list string_of_command_arg args)))

let interpret_new_command (context: context) args =
  let extract_ident = function
    | TermArg (TermAtom (SimpleIdent name)) -> name
    | arg -> raise (Invalid_argument ("extract_ident error on `"
                                    ^ (string_of_command_arg arg) ^ "`"))
  in
  let fusion_string correspondance str =
    Option.value (List.assoc_opt str correspondance) ~default:str
  in
  let fusion_ident correspondance = function
    | SimpleIdent ident ->
       SimpleIdent (fusion_string correspondance ident)
    | ConcatIdent idents ->
       SimpleIdent (string_of_list ~separator:"" (fusion_string correspondance) idents)
  in
  let rec fusion_term correspondance = function
    | TermApp (fnct, args) ->
       TermApp (fusion_term correspondance fnct, List.map (fusion_term correspondance) args)
    | TermProd (left, right) ->
       TermProd (fusion_term correspondance left, fusion_term correspondance right)
    | TermSum (left, right) ->
       TermSum (fusion_term correspondance left, fusion_term correspondance right)
    | TermFun (src, dst) ->
       TermFun (fusion_term correspondance src, fusion_term correspondance dst)
    | TermLetin (ident, def, body) ->
       TermLetin (fusion_ident correspondance ident, fusion_term correspondance def,
                  fusion_term correspondance body)
    | TermAnnot (term, annot) ->
       TermAnnot (fusion_term correspondance term, fusion_term correspondance annot)
    | TermAtom ident ->
       TermAtom (fusion_ident correspondance ident)
  in
  let fusion_command_arg correspondance = function
    | TermArg term ->
       TermArg (fusion_term correspondance term)
    | BlockArg _ ->
       raise (Invalid_argument "fusion_command_arg found a block argument within another")
    | StringArg str ->
       StringArg str
  in
  let fusion_command correspondance = function
    | Command (ident, command_args) ->
       Command (fusion_ident correspondance ident,
                List.map (fusion_command_arg correspondance) command_args)
    | Comment comment ->
       Comment comment
  in
  let interpreter args body (_: context) values =
    let correspondance =
      List.combine args (List.map extract_ident values)
    in
    List.iter (interpret_command context) (List.map (fusion_command correspondance) body)
  in
  match args with
  | (TermArg (TermAtom (SimpleIdent name))) :: rest ->
     begin
       let rest = List.rev rest in
       match rest with
       | BlockArg body :: rest ->
          begin
            let rest = List.rev (List.map extract_ident rest) in
            context.interpreters <- (name, interpreter rest body) :: context.interpreters
          end
       | body :: _ ->
          raise (Invalid_argument ("interpret_new_command the last argument should be a block, got: `"
                                   ^ (string_of_command_arg body) ^ "`"))
       | [] -> raise (Invalid_argument "interpret_new_command should have a block argument after the name")
     end
  | name :: _ ->
     raise (Invalid_argument ("interpret_new_command the first argument should be a simple name, got `"
                              ^ (string_of_command_arg name) ^ "`"))
  | [] ->
     raise (Invalid_argument "interpret_new_command has arguments")

let treat_file filename =
  let name = Filename.remove_extension filename in
  let context =
    {
      frame = empty ();
      already_run = [];
      interpreters = ["declare_variable", interpret_declare_variable;
                      "declare_variable_silent", interpret_declare_variable_silent;
                      "declare_coercion", interpret_declare_coercion;
                      "declare_symbol_instance", interpret_declare_symbol_instance;
                      "check", interpret_check;
                      "fail_check", interpret_fail_check;
                      "new_command", interpret_new_command];
    }
  in
  let log context line = Buffer.add_string context.frame.context.coq_file line in
  let section_name = String.capitalize_ascii (Filename.basename name) in
  log context ("Section " ^ section_name ^ ".\n");
  ignore (List.map (interpret_command context) (commands_of_file filename));
  log context ("End " ^ section_name ^ ".\n");
  let output = open_out (name ^ ".v") in
  output_string output (string_of_frame context.frame)

let _ =
  try
    let filenames = ref [] in
    Arg.parse [] (fun filename -> filenames := filename :: !filenames) "";
    List.iter treat_file !filenames
  with Invalid_argument msg ->
    print_string ("Failure: " ^ msg)
