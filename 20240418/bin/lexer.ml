open Parser

let rec token buf =
  match%sedlex buf with
  | eof -> EOF
  | Plus (Chars " \t\r\n") -> token buf
  | '(' -> LPAR
  | ')' -> RPAR
  | '{' -> LBRACK
  | '}' -> RBRACK
  | '[' -> LSQUARE
  | ']' -> RSQUARE
  | '.' -> DOT
  | " : " -> COLON
  | " + " -> PLUS
  | " * " -> TIMES
  | " -> " -> ARROW
  | ":=" -> DEFEQ
  | "let " -> LET
  | " in " -> IN
  | "-- " -> read_comment (Buffer.create 0) buf
  | '"' -> read_string (Buffer.create 0) buf
  | Plus ('a'..'z' | 'A'..'Z' | '0'..'9' | Chars "<>+*-/_^?:") -> STRING (Sedlexing.Utf8.lexeme buf)
  | _ -> failwith ("Unexpected token '" ^ (Sedlexing.Utf8.lexeme buf) ^ "'")
and read_string store buf =
  match%sedlex buf with
  | Compl '"' -> (Buffer.add_string store (Sedlexing.Utf8.lexeme buf); read_string store buf)
  | '"' -> QUOTED (Buffer.contents store)
  | _ -> assert false
and read_comment store buf =
  match%sedlex buf with
  | Compl '\n' -> (Buffer.add_string store (Sedlexing.Utf8.lexeme buf); read_comment store buf)
  | '\n' -> COMMENT (Buffer.contents store)
  | _ -> assert false
