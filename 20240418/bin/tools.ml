
let string_of_list ?(separator= " ") stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string separator;
                      helper tail
  in
  helper list;
  Buffer.contents buf
