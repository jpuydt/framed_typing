# Framed typing test code

## Generalities

This code is showcasing that the algorithm described in the article
(sources in `../20240418/article/article.tex`) is actually working.

The examples are given as eve files in the `examples/` directory, each
describing a mathematical situation (declare variables and define a
frame) and listing the expressions to check. They are written in a toy
language specifically designed to this end.

The sample code provides a transpiler from this toy language to Coq:
each source eve file is turned into a Coq file, which is then compiled
to confirm the success.

You should read those example files to understand how framed typing
works, and you may want to compare each `foo.eve` file with the
resulting `foo.v` file (which `dune` places in
`_build/default/examples/`).

Compiling the project should be as simple as running `dune build` ;
there should be neither error nor warning.

## How to read eve files?

The toy language used in the example files is of course formally
described in `bin/lexer.ml` and `bin/parser.mly`, but a few highlights
are useful.

A user of a mathematical library needs to understand:

- Comments start with `--` and extend to the end of the line.

- The `#input` command includes another eve file at the given point.

- The `declare_variable` command has as arguments a variable name and
  a type, and will as expected add a variable to the context for the
  elaborator, and declare it in the proof assistant too.

- The `declare_variable_silent` command is a variant of
  `declare_variable`, but makes the variable available only to the
  elaborator. It is useful when it is already known to the proof
  assistant.

- The `check` command has as arguments an untyped expression and a
  string, and checks that elaborating the expression will indeed give
  a typed expression represented by this string.

- The `fail_check` has arguments an untyped expression and a string,
  and checks that elaborating the expression will fail with the
  corresponding error message.

Additionally, the mathematical library can define new commands, making
it easier to declare objects relevant to the mathematical task at
hand.

The developers of a mathematical library will use all the above, but
in addition they can populate the current frame using:

- The `declare_symbol_instance` command takes three arguments: the
symbol name, the instance and the level.

- The `declare_coercion` command takes a function name, a source type
  and a destination type.

Finally the 'new_command` command bundles several commands into
one. It takes as arguments the name of the new command, a list of
names for the arguments (possibly empty) and in curly brackets the
body of the new command, in which square brackets mean string
concatenation. When using a command, the actual arguments are strings
and used as such. In `examples/common.eve` for example, the `add_mul`
macro declares the zero of the object using the name `[name 0]` so
calling `add_mul N` will declare the zero object under the name `N0`.

The expressions and types should be pretty readable for anyone
accustomed to type theory ; let us just mention that for generic
function de Bruijn indices are used with a question mark (`nil: list
?0` for example).

## Examples

In the `examples/` directory, the file `syllabus.eve` shows what
should be in the mathematical library: ready-to-use macros/functions
for the user. All other eve files are actual examples of what a user
could type: they set the stage, then consider expressions.

(Those examples are also used to test the code, since they contain the
sample expressions with their expected answers.)

### `basic.eve`

In this file we consider the simple frame $\mathbb N\rightarrow\mathbb
Z$ and three variables, and consider various arithmetical expressions
involving them.

### `filters.eve`

Those examples are an example of considering analytical expressions
involving filters ; it is mostly about deciding what `+oo` means - the
filter is the usual line from $\mathbb N$ to the extended real line,
completed to the right with filters on reals.

## `functions.eve`

There we want to consider algebraic expressions involving objects of
$\mathcal F(I,\mathbb K)$, including an element of $\mathcal
F(I,\mathbb N)$ which will involve a functional composition.

## `inequalities.eve`

In this file the frame is the usual line $\mathbb N\rightarrow\mathbb
Z\rightarrow\mathbb R\rightarrow\mathbb C$ and we try to compare
variables in the various levels using the usual $\leqslant$.

## `letin.eve`

The `let ... in ...` construct is not used in mathematics, but we
still implemented it.

## `polymorphic.eve`

Here we consider examples using the `cons` and `nil` polymorphic
functions and construct various expressions.

## `rings.eve`

The frame is $\mathbb N\rightarrow\mathbb Z\rightarrow A\rightarrow
A/I$, where $A$ is (thought to be) a commutative ring with unit and
$A/I$ a quotient by an ideal. We consider various algebraic
expressions -- the situation is similar to `basic.v`, but in a more
abstract setting.

## `vectors.eve`

There we consider a linear algebra situation with an ambient vector
space $E$ on a field $\mathbb K$ and two subspaces $F$ and $G$ ; the
frame is hence a non-connected graph with the line $\mathbb
N\rightarrow\mathbb Z\rightarrow\mathbb K$ and $F\rightarrow
E\leftarrow G$. We can then combine vectors and scalars in various
algebraic expressions like first year students do all the time.

## `zero.eve`

In this single-example file we consider the expression $0+0v_0$, where
the frame is about a sequence $v$ of elements of a vector space $E$ on
a field $\mathbb KK$. There are three different uses of $0$ in a
single expression, and it is a very typical example where current
proof assistants need annotations.

## Code structure

### `bin/` directory

It contains the code for the transpiler, hence is responsible of
lexing&parsing for the toy language, which we describe below.

### `elaboration/` directory

This is where most of the work is done ; since the code lives outside
of a complete proof assistant, it needs to implement much more than
the strict minimum for framed typing: it contains a rudimentary
bidirectional typing system with a pinch of gradual typing.

The `target.ml` files declares an abstract interface for a target
proof assistant, and `pseudocoq.ml` is an implementation that will
produce strings representing valid Coq expressions. The pseudo- prefix
is because it doesn't use Coq to produce direct Coq objects.

Here is a short description of the various file, in logical order:

- `tools.ml` defines a few useful functions used in the rest of the
  code

- `target.ml` and `pseudocoq.ml` were already described

- `annot.ml` describes the annotation type `atype` used in the untyped
expressions

- `untyped.ml` describes the untyped expressions `uexpr`, with optional
  annotation with `atype`

- `frame.ml` contains much material, because it defines many notions:

   - a notion of fuzzy type, since during elaboration the types are
  not really known, we only have constraints),

  - with corresponding fuzzy expressions

  - a notion of symbol as discussed in the article

  - the main frame type `t`, which is a store for:
  
      - the poset defined by the relevant objects
      
      - the known/defined symbols
      
      - the constant parsers (not used in the examples, but left from
        an earlier version)

      - the local variables when a let-in or a lambda is treated

- `elaborate.ml` has the code to run the algorithm with the data
  defined elsewhere, hence the bidirectional typing code, the
  imposition function and the choice function - all described in
  details in the article.

### `examples/` directory

Already discussed above.
