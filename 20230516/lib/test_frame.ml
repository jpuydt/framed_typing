open Option

open Coq
open Fuzzy
open Context



let add_NZ_to context =
  let n_parser value =
    if Str.string_match (Str.regexp "[0-9]+$") value 0
    then
      some (CConst (value ^ "%N", CBasic "N"))
    else none
  and z_parser value =
    if Str.string_match (Str.regexp "-?[0-9]+$") value 0 then
      begin
        if String.starts_with ~prefix:"-" value
        then some (CConst ("(" ^ value ^ ")%Z", CBasic "Z"))
        else some (CConst (value ^ "%Z", CBasic "Z"))
      end
    else none
  in
  context_add_constant_parser context n_parser;
  context_add_constant_parser context z_parser;
  context_add_coercion context "Z_of_N" (CBasic "N") (CBasic "Z")

let add_field context name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (noose_of_ftype (ftype_of_ctype ctype))
  and kk = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator kk);
  add (name ^ "mul") (ctype_operator kk);
  add (name ^ "pow") (CFunct (kk, CFunct (CBasic "N", kk)));
  context_add_symbol context "add" kk (CConst (name ^ "add", ctype_operator kk));
  context_add_symbol context "mul" kk (CConst (name ^ "mul", ctype_operator kk));
  add (name ^ "0") kk;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst (name ^ "0", kk))
                                                    else none);
  add (name ^ "1") kk;
  context_add_constant_parser context (fun value -> if value = "1"
                                                    then some (CConst (name ^ "1", kk))
                                                    else none);
  add (name ^ "_of_Z") (CFunct (CBasic "Z", kk));
  context_add_coercion context (name ^ "_of_Z") (CBasic "Z") kk

let add_vector_space context name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (noose_of_ftype (ftype_of_ctype ctype))
  and kk = CBasic kk_name
  and ee = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator ee);
  add (name ^ "sub") (ctype_operator ee);
  add (name ^ "ext_mul") (CFunct (kk, CFunct (ee, ee)));
  context_add_symbol context "add" ee (CConst (name ^ "add", ctype_operator ee));
  context_add_symbol context "mul" ee (CConst (name ^ "ext_mul", CFunct (kk, CFunct (ee, ee))));
  add (name ^ "0") ee;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst (name ^ "0", ee))
                                                    else none)

let add_subvector_space context name ee_name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (noose_of_ftype (ftype_of_ctype ctype))
  and kk = CBasic kk_name
  and ee = CBasic ee_name
  and ff = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator ff);
  add (name ^ "sub") (ctype_operator ff);
  add (name ^ "ext_mul") (CFunct (kk, CFunct (ff, ff)));
  add (name ^ "inj" ^ ee_name) (CFunct (ff, ee));
  context_add_symbol context "add" ff (CConst (name ^ "add", ctype_operator ff));
  context_add_symbol context "mul" ff (CConst (name ^ "ext_mul", CFunct (kk, CFunct (ff, ff))));
  context_add_coercion context (name ^ "inj" ^ ee_name) ff ee;
  add (name ^ "0") ff;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst (name ^ "0", ff))
                                                    else none)

let add_numeric_functions_on context name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (noose_of_ftype (ftype_of_ctype ctype))
  and kk = CBasic kk_name
  and fik = CFunct (CBasic name, CBasic kk_name)
  in
  add name (CBasic "Type");
  add ("F" ^ name ^ kk_name ^ "add") (ctype_operator fik);
  add ("F" ^ name ^ kk_name ^ "ext_mul") (CFunct (kk, CFunct (fik, fik)));
  add (kk_name ^ "injF" ^ name ^ kk_name) (CFunct (kk, fik));
  context_add_symbol context "add" fik (CConst ("F" ^ name ^ kk_name ^ "add", ctype_operator fik));
  context_add_symbol context "mul" fik (CConst ("F" ^ name ^ kk_name ^ "ext_mul", CFunct (kk, CFunct (fik, fik))));
  (*FIXME: makes FIK0 useless context_add_coercion context (kk_name ^ "injF" ^ name ^ kk_name) kk fik;*)
  add ("F" ^ name ^ kk_name ^ "0") fik;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst ("F" ^ name ^ kk_name ^ "0", fik))
                                                    else none)

let add_filters context =
  let add name ctype =
    context_add_variable ~for_coq:true context name (noose_of_ftype (ftype_of_ctype ctype))
  and r = CBasic "R"
  and rbar = CBasic "Rbar"
  and filter_n = CParam ("filter", [CBasic "N", Neither])
  and filter_r = CParam ("filter", [CBasic "R", Neither])
  in
  add "filter" (CFunct (CBasic "Type", CBasic "Type"));
  add "R" (CBasic "Type");
  add "Rbar" (CBasic "Type");
  add "injRRbar" (CFunct (r, rbar));
  context_add_coercion context "injRRbar" r rbar;
  add "filter_of_Rbar" (CFunct (rbar, filter_r));
  context_add_coercion context "filter_of_Rbar" rbar filter_r;
  add "filter_Ninfty" filter_n;
  add "filter_Rinfty" filter_r;
  context_add_constant_parser context (fun value -> if value = "oo"
                                                    then some (CConst ("filter_Ninfty", filter_n))
                                                    else none);
  context_add_constant_parser context (fun value -> if value = "oo"
                                                    then some (CConst ("filter_Rinfty", filter_r))
                                                    else none);
  add "tends_to_NR" (ctype_funct3 (CFunct (CBasic "N", CBasic "R"))
                       filter_n filter_r (CBasic "Prop"));
  add "tends_to_RR" (ctype_funct3 (CFunct (CBasic "R", CBasic "R"))
                       filter_r filter_r (CBasic "Prop"));
  context_add_symbol context "tends_to" (CBasic "N") (CConst ("tends_to_NR",
                                                              ctype_funct3 (CFunct (CBasic "N",
                                                                                    CBasic "R"))
                                                                filter_n filter_r (CBasic "Prop")));
  context_add_symbol context "tends_to" (CBasic "R") (CConst ("tends_to_RR",
                                                              ctype_funct3 (CFunct (CBasic "R",
                                                                                    CBasic "R"))
                                                                filter_r filter_r (CBasic "Prop")));
  ()
