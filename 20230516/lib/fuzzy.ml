open Option
open List

open Tools
open Coq

(* FUZZY TYPES *)

type ftype = | FBottom
             | FTop
             | FBasic of string
             | FFunct of ftype * ftype (* argument result *)
             | FParam of string * (ftype * mode) list

let ftype_predicate ftype = FFunct (ftype, FBasic "Prop")
let ftype_relation ftype = FFunct (ftype, FFunct (ftype, FBasic "Prop"))
let ftype_operator ftype = FFunct (ftype, FFunct (ftype, ftype))

let rec ftype_of_ctype = function
  | CBasic name -> FBasic name
  | CFunct (fn, arg) -> FFunct (ftype_of_ctype fn, ftype_of_ctype arg)
  | CParam (name, args) -> FParam (name,
                                   map (fun (arg, mode) -> ftype_of_ctype arg, mode) args)

let ftype_of_cexpr cexpr = ftype_of_ctype (ctype_of_cexpr cexpr)

let rec string_of_ftype ft =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  (match ft with
   | FBottom ->
      add_string "bottom"
   | FTop ->
      add_string "top"
   | FBasic name ->
      add_string name
   | FFunct (arg, result) ->
      add_string "(";
      add_string (string_of_ftype arg);
      add_string " -> ";
      add_string (string_of_ftype result);
      add_string ")"
   | FParam (name, args) ->
      add_string name;
      add_string " ";
      add_string (string_of_list (fun (arg, _) -> string_of_ftype arg) args)
  );
  Buffer.contents buf

let rec ctype_of_ftype ftype =
  match ftype with
  | FBottom | FTop ->
     raise (Invalid_argument ("Fuzzy type "
                              ^ (string_of_ftype ftype)
                              ^ " isn't a Coq type"))
  | FBasic name ->
     CBasic name
  | FFunct (src, dst) ->
     CFunct (ctype_of_ftype src, ctype_of_ftype dst)
  | FParam (name, args) ->
     CParam (name, map (fun (arg, mode) -> ctype_of_ftype arg, mode) args)



(* FRAME *)

type frame =
  {
    (* source destination and the names of the coercions proving the inequality *)
    mutable le: (ftype * ftype * string list) list;
  }

let frame_new () =
  {
    le = [];
  }

let frame_add_coercion frame name source_ target_ =
  let source = ftype_of_ctype source_
  and target = ftype_of_ctype target_
  in
  let helper (src, dst, path) =
    if src = target then
      [(src, dst, path); (source, dst, name :: path)]
    else if dst = source then
      [(src, dst, path); (src, target, list_add_last path name)]
    else [(src, dst, path)]
  in
  frame.le <- (source, target, [name]) :: (concat_map helper frame.le)

let frame_coercion_path frame src dst =
  try
    let (_, _, path) = find (fun (src_, dst_, _) -> src = src_ && dst = dst_) frame.le in
    path
  with Not_found -> []

let rec frame_le frame ft1 ft2 =
  if ft1 = ft2 || (exists (fun (src, dst, _) -> src = ft1 && dst = ft2) frame.le) then
    true
  else
    match ft1, ft2 with
    | FBottom, _ | _, FTop ->
       true
    | _, FBottom | FTop, _ ->
       false
    | FBasic _, FFunct _ | FFunct _, FBasic _ | FBasic _, FParam _ | FParam _, FBasic _ | FFunct _, FParam _ | FParam _, FFunct _ ->
       false
    | FFunct (arg1, res1), FFunct (arg2, res2) ->
       (frame_le frame arg2 arg1) && (frame_le frame res1 res2)
    | FParam _, FParam _ | FBasic _, FBasic _ ->
       false (* we would have seen ft1 = ft2 above! *)

let rec frame_inf frame ft1 ft2 =
  if frame_le frame ft1 ft2
  then
    ft1
  else if frame_le frame ft2 ft1
  then
    ft2
  else
    let cut1 = filter_map (fun (src, dst, _) -> if dst = ft1 then some src else none) frame.le in
    let commons = filter_map (fun (src, dst, _) -> if dst = ft2 && mem src cut1 then some src else none) frame.le in
    match commons with
    | head :: tail ->
       fold_left (fun res candidate -> if frame_le frame res candidate then candidate else res) head tail
    | [] ->
       begin
         match ft1, ft2 with
         | FBottom, _ | _, FBottom ->
            FBottom
         | FTop, other | other, FTop ->
            other
         | FBasic _, FFunct _ | FFunct _, FBasic _ | FBasic _, FBasic _ | FBasic _, FParam _ | FParam _, FBasic _ | FFunct _, FParam _ | FParam _, FFunct _ | FParam _, FParam _ ->
            FBottom
         | FFunct (arg1, res1), FFunct (arg2, res2) ->
            FFunct (frame_sup frame arg1 arg2, frame_inf frame res1 res2)
       end
and frame_sup frame ft1 ft2 =
  if frame_le frame ft1 ft2
  then
    ft2
  else if frame_le frame ft2 ft1
  then
    ft1
  else
    let cut1 = filter_map (fun (src, dst, _) -> if src = ft1 then some dst else none) frame.le in
    let commons = filter_map (fun (src, dst, _) -> if src = ft2 && mem dst cut1 then some dst else none) frame.le in
    match commons with
    | head :: tail ->
       fold_left (fun res candidate -> if frame_le frame res candidate then candidate else res) head tail
    | [] ->
       begin
         match ft1, ft2 with
         | FBottom, other | other, FBottom ->
            other
         | FTop, _ | _, FTop ->
            FTop
         | FBasic _, FFunct _ | FFunct _, FBasic _ | FBasic _, FBasic _ | FBasic _, FParam _ | FParam _, FBasic _ | FFunct _, FParam _ | FParam _, FFunct _ | FParam _, FParam _->
            FTop
         | FFunct (arg1, res1), FFunct (arg2, res2) ->
            FFunct (frame_inf frame arg1 arg2, frame_sup frame res1 res2)
       end



(* NOOSE *)

type noose = ftype * ftype

let noose_default = (FBottom, FTop)

let noose_new below above = (below, above)

let noose_of_ftype ftype = noose_new ftype ftype

let noose_of_ctype ctype = noose_of_ftype (ftype_of_ctype ctype)

let noose_tighten frame (below1, above1) (below2, above2) =
  (frame_sup frame below1 below2, frame_inf frame above1 above2)

let noose_loosen frame (below, above) ftype =
  (frame_inf frame below ftype, frame_sup frame above ftype)

let noose_loosest frame = function
  | [] -> raise (Invalid_argument "Empty list!")
  | first :: others ->
     let ftype_of_cexpr cexpr = ftype_of_ctype (ctype_of_cexpr cexpr) in
     fold_left (fun noose cexpr -> noose_loosen frame noose (ftype_of_cexpr cexpr)) (noose_of_ftype (ftype_of_cexpr first)) others

let noose_compatible frame (below, above) ftype =
  (frame_le frame below ftype) && (frame_le frame ftype above)

let string_of_noose (below, above) =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  if below = above then
      add_string (string_of_ftype below)
  else begin
      add_string "between ";
      add_string (string_of_ftype below);
      add_string " and ";
      add_string (string_of_ftype above)
    end;
  Buffer.contents buf



(* SYMBOLS *)

type symbol = string * (ctype * cexpr) list (* list of (level, instance) *)

let symbol_new name = name, []

let symbol_name symbol = let name, _ = symbol in name

let symbol_add_instance symbol level cexpr =
  let name, instances = symbol in
  name, list_add_last instances (level, cexpr)

let symbol_normalize symbol frame =
  let name, instances = symbol in
  let helper (ct1, _) (ct2, _) =
    let ft1 = ftype_of_ctype ct1
    and ft2 = ftype_of_ctype ct2
    in
    if frame_le frame ft1 ft2 then -1
    else if frame_le frame ft2 ft1 then 1
    else 0
  in
  name, stable_sort helper instances

let symbol_noose frame symbol =
  let _, instances = symbol in
  noose_loosest frame (map snd instances)

let symbol_tighten frame symbol noose =
  let name, instances = symbol in
  name, filter (fun (_, cexpr) ->
            noose_compatible frame noose (ftype_of_cexpr cexpr)) instances

let symbol_is_empty symbol = length (snd symbol) = 0

let symbol_count_ambiguities symbol = length (snd symbol)

let symbol_first_instance symbol =
  let name, instances = symbol in
  match instances with
  | [] ->
     raise (Invalid_argument ("Symbol `"
                              ^ name
                              ^ "` doesn't have any instance!"))
  | (_, res) :: _ ->
     res

let string_of_symbol symbol =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf
  and name, instances = symbol in
  add_string name;
  add_string " { ";
  add_string (string_of_list (fun (_, ce) -> string_of_cexpr ce) instances);
  add_string " }";
  Buffer.contents buf


(* FUZZY EXPRESSIONS *)

type fexpr = | FVar of string * noose
             | FSymb of symbol * noose
             | FApp of fexpr * fexpr * noose
             | FLambda of string * noose * fexpr * noose
             | FLet of string * fexpr * fexpr * noose

let noose_of_fexpr = function
  | FVar (_, noose) | FSymb (_, noose) | FApp (_, _, noose) | FLet (_, _, _, noose) ->
     noose
  | FLambda (_, (below_arg, above_arg), _, (below_body, above_body)) ->
     (FFunct (above_arg, below_body), FFunct (below_arg, above_body))

let rec fexpr_of_cexpr = function
  | CConst (name, ctype) ->
     FVar (name, noose_of_ctype ctype)
  | CApp (fn, body) ->
     FApp (fexpr_of_cexpr fn,
           fexpr_of_cexpr body,
           noose_of_ctype (ctype_of_cexpr body))
  | CLambda (var, var_ctype, body) ->
     FLambda (var,
              noose_of_ctype var_ctype,
              fexpr_of_cexpr body,
              noose_of_ctype (ctype_of_cexpr body))
  | CLet (var, expr, body) ->
     FLet (var, fexpr_of_cexpr expr, fexpr_of_cexpr body, noose_of_ctype (ctype_of_cexpr body))

let string_of_fexpr ?(with_annot=true) fe_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec s_of_fe fe =
    match fe with
    | FVar (name, noose) ->
       if with_annot
       then begin
           add_string "(";
           add_string name;
           add_string ": ";
           add_string (string_of_noose noose);
           add_string ")"
         end else
         add_string name
    | FSymb (symbol, noose) ->
       let (name, _) = symbol in
       if with_annot
       then begin
           add_string "(";
           add_string name;
           add_string ": ";
           add_string (string_of_noose noose);
           add_string ")"
         end else
         add_string name
    | FApp (fn, body, noose) ->
       if with_annot
       then begin
           add_string "(";
           s_of_fe fn;
           add_string " ";
           s_of_fe body;
           add_string "): ";
           add_string (string_of_noose noose)
         end else begin
           add_string "(";
           s_of_fe fn;
           add_string " ";
           s_of_fe body;
           add_string ")"
         end
    | FLambda (var, var_noose, body, body_noose) ->
       if with_annot
       then begin
           add_string "(fun ";
           add_string var;
           add_string ": ";
           add_string (string_of_noose var_noose);
           add_string " => ";
           s_of_fe body;
           add_string "): ";
           let (var_noose_below, var_noose_above) = var_noose
           and (body_noose_below, body_noose_above) = body_noose
           in
           add_string (string_of_noose (FFunct (var_noose_above, body_noose_below),
                                        FFunct (var_noose_below, body_noose_above)))
         end else begin
           add_string "(fun ";
           add_string var;
           add_string " => ";
           s_of_fe body;
           add_string ")"
         end
    | FLet (var, expr, body, noose) ->
       add_string "(let ";
       add_string var;
       add_string " := ";
       s_of_fe expr;
       add_string " in ";
       s_of_fe body;
       if with_annot
       then begin
           add_string "): ";
           add_string (string_of_noose noose)
         end else
         add_string ")"
  in
  s_of_fe fe_;
  Buffer.contents buf

let rec fexpr_count_ambiguities = function
  | FVar _ -> 0
  | FSymb (symbol, _) -> symbol_count_ambiguities symbol
  | FApp (fn, arg, _) -> (fexpr_count_ambiguities fn) + (fexpr_count_ambiguities arg)
  | FLambda (_, _, body, _) -> fexpr_count_ambiguities body
  | FLet (_, expr, body, _) -> (fexpr_count_ambiguities expr) + (fexpr_count_ambiguities body)
