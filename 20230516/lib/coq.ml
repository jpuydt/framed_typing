open Tools

(* COQ TYPES *)
(* simplified... *)

type mode = | Cov | Contrav | Neither

type ctype = | CBasic of string
             | CFunct of ctype * ctype (* argument result *)
             | CParam of string * (ctype * mode) list

let ctype_predicate ct = CFunct (ct, CBasic "Prop")
let ctype_relation ct = CFunct (ct, CFunct (ct, CBasic "Prop"))
let ctype_operator ct = CFunct (ct, CFunct (ct, ct))
let ctype_list_of ct = CParam ("list", [ct, Cov])
let ctype_funct2 arg1 arg2 res = CFunct (arg1, CFunct (arg2, res))
let ctype_funct3 arg1 arg2 arg3 res = CFunct (arg1, ctype_funct2 arg2 arg3 res)

let ctype_comp ct1 ct2 =
  match ct1 with
  | CFunct (arg, result) ->
     if arg = ct2
     then result
     else raise (Invalid_argument "composing non-composable types")
  | _ -> raise (Invalid_argument "composing non-composable types")

let rec string_of_ctype ct =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  (match ct with
   | CBasic name ->
      add_string name;
   | CFunct (arg, result) ->
      add_string "(";
      add_string (string_of_ctype arg);
      add_string " -> ";
      add_string (string_of_ctype result);
      add_string ")"
   | CParam (name, args) ->
      add_string name;
      add_string " ";
      add_string (string_of_list (fun (arg, _) -> string_of_ctype arg) args)
  );
  Buffer.contents buf



(* COQ EXPRESSIONS *)
(* simplified... *)

type cexpr = | CConst of string * ctype
             | CApp of cexpr * cexpr
             | CLambda of string * ctype * cexpr
             | CLet of string * cexpr * cexpr

let rec ctype_of_cexpr = function
  | CConst (_, ctype) -> ctype
  | CApp (fn, arg) -> ctype_comp (ctype_of_cexpr fn) (ctype_of_cexpr arg)
  | CLambda (_, ctype, body) -> CFunct (ctype, ctype_of_cexpr body)
  | CLet (_, _, body) -> ctype_of_cexpr body

let string_of_cexpr cexpr_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper cexpr =
    match cexpr with
    | CConst (name, _) ->
       add_string name
    | CApp (fn, arg) ->
       add_string "(";
       helper fn;
       add_string " ";
       helper arg;
       add_string ")"
    | CLambda (var, ctype, body) ->
       add_string "(fun ";
       add_string var;
       add_string ": ";
       add_string (string_of_ctype ctype);
       add_string " -> ";
       helper body;
       add_string ")"
    | CLet (name, expr, body) ->
       add_string "(let ";
       add_string name;
       add_string " := ";
       helper expr;
       add_string " in ";
       helper body;
       add_string ")"
  in
  helper cexpr_;
  Buffer.contents buf
