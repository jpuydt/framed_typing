(* HELPING TOOLS *)

open List

module NotTrie = Map.Make(String) (* FIXME: I'd prefer a real trie *)

let string_of_list ?(separator= " ") stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string separator;
                      helper tail
  in
  helper list;
  Buffer.contents buf

let rec list_add_last list elt =
  match list with
  | [] -> [elt]
  | head :: tail -> head :: (list_add_last tail elt)

let list_map_option_product (worker: 'a -> 'b -> 'c option) (first: 'a list) (second: 'b list) =
  concat_map (fun s -> (filter_map (fun w -> w s) (map worker first))) second
