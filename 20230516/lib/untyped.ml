open Fuzzy

(* UNTYPED EXPRESSIONS *)
(* untyped, but with possible annotations *)

type uexpr = | UConst of string * noose option
             | UApp of uexpr * uexpr * noose option
             | ULambda of string * noose option * uexpr * noose option
             | ULet of string * uexpr * noose option * uexpr * noose option

let uapp_none fn arg = UApp (fn, arg, None)
let uapp2_none fn arg1 arg2 = uapp_none (uapp_none fn arg1) arg2
let uapp3_none fn arg1 arg2 arg3 = uapp_none (uapp2_none fn arg1 arg2) arg3

let string_of_uexpr ue_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec s_of_ue_helper = function
    | UConst (name, oannot) ->
       begin match oannot with
       | None ->
          add_string name
       | Some annot ->
          add_string "(";
          add_string name;
          add_string ": ";
          add_string (string_of_noose annot);
          add_string ")"
       end
    | UApp (fn, arg, oannot) ->
       begin match oannot with
       | None ->
          s_of_ue_helper fn;
          add_string " ";
          s_of_ue_helper arg
       | Some annot ->
          add_string "(";
          s_of_ue_helper fn;
          add_string " ";
          s_of_ue_helper arg;
          add_string ": ";
          add_string (string_of_noose annot);
          add_string ")"
       end
    | ULambda (name, oannotname, body, oannotbody) ->
       add_string "[";
       add_string name;
       begin match oannotname with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_noose annot)
       end;
       add_string " |-> ";
       begin match oannotbody with
       | None ->
          s_of_ue_helper body
       | Some annot ->
          add_string "(";
          s_of_ue_helper body;
          add_string "): ";
          add_string (string_of_noose annot)
       end;
       add_string "]"
    | ULet (name, expr, oannotexpr, body, oannotbody) ->
       add_string "{ let ";
       add_string name;
       begin match oannotexpr with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_noose annot)
       end;
       add_string " := ";
       s_of_ue_helper expr;
       add_string " in ";
       s_of_ue_helper body;
       begin match oannotbody with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_noose annot)
       end;
       add_string " }"
  in
  s_of_ue_helper ue_;
  Buffer.contents buf
