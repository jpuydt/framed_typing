open Option
open List

open Elaboration.Fuzzy
open Elaboration.Untyped
open Elaboration.Context
open Elaboration.Elaborate

open Elaboration.Test_coq
open Elaboration.Test_frame

let uexpr_for_basic_tests =
  let stupid = UConst ("a", some (noose_of_ctype (CBasic "bool")))
  and plus_a_b = uapp2_none (UConst ("add", none)) (UConst ("a", none)) (UConst ("b", none))
  and plus_a_c = uapp2_none (UConst ("add", none)) (UConst ("a", none)) (UConst ("c", none))
  and plus_c_1 = uapp2_none (UConst ("add", none)) (UConst ("c", none)) (UConst ("1", none))
  and not_foo = ULambda ("x", none,
                         uapp_none (UConst ("not", none)) (UConst ("x", none)), none)
  and lambda_x_plus_1 = ULambda ("x", some (noose_of_ctype (CBasic "N")),
                                 uapp2_none (UConst ("add", none)) (UConst ("x", none))
                                   (UConst ("-1", none)),
                                 none)
  and plus_a_annot = UApp (UConst ("add", none), UConst ("a", none),
                           some (noose_of_ctype (CFunct (CBasic "Z", CBasic "Z"))))
  and plus_c = uapp_none (UConst ("add", none)) (UConst ("c", none))
  and plus_a = uapp_none (UConst ("add", none)) (UConst ("a", none))
  and plus_a_b_c = uapp2_none (UConst ("add", none))
                     (uapp2_none (UConst ("add", none))
                        (UConst ("a", none))
                        (UConst ("b", none)))
                     (UConst ("c", none))
  and plus_two_vars = ULambda ("x", none, ULambda ("y", none,
                                                   uapp2_none (UConst ("add", none))
                                                     (UConst ("x", none))
                                                     (UConst ("y", none)),
                                                     none),
                               none)
  and let_add_a = ULet ("x",
                        UConst ("a", none),
                        none,
                        uapp_none (UConst ("add", none)) (UConst ("x", none)),
                        none)
  and let_add_1 = ULet ("x",
                        UConst ("1", none),
                        none,
                        uapp_none (UConst ("add", none)) (UConst ("x", none)),
                        none)
  and let_add_1_bis = ULet ("x",
                            UConst ("1", none),
                            none,
                            uapp_none (UConst ("add", none)) (UConst ("x", none)),
                            some (FFunct (FBasic "Z", FBottom), FFunct (FBasic "Z", FTop)))
  and let_AC_ex10 = ULet ("plus",
                          UConst ("add", none),
                          none,
                          uapp2_none (UConst ("plus", none))
                            (uapp2_none (UConst ("plus", none))
                               (UConst ("a", none))
                               (UConst ("b", none)))
                            (UConst ("c", none)),
                          none)
  and list_N = uapp2_none
                 (UConst ("cons", none))
                 (UConst ("1", none))
                 (uapp2_none
                    (UConst ("cons", none))
                    (UConst ("2", none))
                    (UConst ("nil", none)))
  and list_Z1 = uapp2_none
                 (UConst ("cons", none))
                 (UConst ("-1", none))
                 (uapp2_none
                    (UConst ("cons", none))
                    (UConst ("2", none))
                    (UConst ("nil", none)))
  and list_Z2 = uapp2_none
                 (UConst ("cons", none))
                 (UConst ("1", none))
                 (uapp2_none
                    (UConst ("cons", none))
                    (UConst ("-2", none))
                    (UConst ("nil", none)))
  in
  [stupid; plus_a_b; plus_a_c; plus_c_1; not_foo;
   lambda_x_plus_1; plus_a_annot; plus_c; plus_a;
   plus_a_b_c; plus_two_vars; let_add_a; let_add_1;
   let_add_1_bis; let_AC_ex10; list_N; list_Z1;
   list_Z2]

let uexpr_for_vector_tests =
  let add_f = uapp_none (UConst ("add", none)) (UConst ("f", none))
  and mul_k = uapp_none (UConst ("mul", none)) (UConst ("k", none))
  in
  let add_ff = uapp_none add_f (UConst ("f", none))
  and add_fg = uapp_none add_f (UConst ("g", none))
  and add_f0 = uapp_none add_f (UConst ("0", none))
  and mul_k0 = uapp_none mul_k (UConst ("0", none))
  and mul_kf = uapp_none mul_k (UConst ("f", none))
  in
  [add_f; add_ff; add_fg; add_f0; mul_k; mul_k0; mul_kf]

let uexpr_for_functions_tests =
  let add_f = uapp_none (UConst ("add", none)) (UConst ("f", none))
  and mul_k = uapp_none (UConst ("mul", none)) (UConst ("k", none))
  in
  let add_fg = uapp_none add_f (UConst ("g", none))
  and add_f0 = uapp_none add_f (UConst ("0", none))
  (* FIXME: coercions on function types don't work and add_fh = uapp_none add_f (UConst ("h", none)) *)
  and mul_kf = uapp_none mul_k (UConst ("f", none))
  in
  [add_f; add_fg; add_f0;(* add_fh; *) mul_kf]

let uexpr_for_filter_tests =
  let v_to_x = uapp3_none
                 (UConst ("tends_to", none))
                 (UConst ("v", none))
                 (UConst ("oo", none))
                 (UConst ("x", none))
  and v_to_infty = uapp3_none
                     (UConst ("tends_to", none))
                     (UConst ("v", none))
                     (UConst ("oo", none))
                     (UConst ("oo", none))
  and f_x_to_x = uapp3_none
                   (UConst ("tends_to", none))
                   (UConst ("f", none))
                   (UConst ("x", none))
                   (UConst ("x", none))
  and f_x_to_infty = uapp3_none
                       (UConst ("tends_to", none))
                       (UConst ("f", none))
                       (UConst ("x", none))
                       (UConst ("oo", none))
  and f_infty_to_x = uapp3_none
                       (UConst ("tends_to", none))
                       (UConst ("f", none))
                       (UConst ("oo", none))
                       (UConst ("x", none))
  and f_infty_to_infty = uapp3_none
                           (UConst ("tends_to", none))
                           (UConst ("f", none))
                           (UConst ("oo", none))
                           (UConst ("oo", none))
  in
  [
    v_to_x; v_to_infty;
    f_x_to_x; f_x_to_infty; f_infty_to_x; f_infty_to_infty
  ]
  
let _ =
  let context = context_new ()
  and buf = Buffer.create 0
  in
  let treatments =
    let tb = elaborate_top_bottom_pass context noose_default
    and bt = elaborate_bottom_top_pass context
    in
    [("tb", tb);
     ("bt", bt);
     ("tb", tb);
     ("bt", bt);
     ("tb", tb);
     ("bt", bt);
     ("choice", elaborate_force_choice context);
    ]
  and add_string = Buffer.add_string buf
  in
  let run name action value =
    add_string "\t(* ";
    add_string name;
    try
      let result = action value in
      let ambiguities = fexpr_count_ambiguities result in
      add_string " (ambiguities left: ";
      add_string (string_of_int ambiguities);
      add_string "): ";
      add_string (string_of_fexpr result);
      add_string " *)\n";
      result, ambiguities
    with
      Invalid_argument msg ->
      add_string " raises an exception: ";
      add_string msg;
      add_string " *)\n";
      value, -1
  in
  let rec chainer ?(run_anyway = false) value workers =
    match workers with
    | [] -> value, fexpr_count_ambiguities value
    | (name, action) :: others ->
       let result, ambiguities = run name action value in
       if ambiguities > 0 || run_anyway then chainer result others
       else if ambiguities = 0 then result, 0
       else value, -1
  in
  let insist fexpr =
    let result = ref fexpr in
    let ambiguities = ref (fexpr_count_ambiguities !result) in
    while !ambiguities > 0 do
      let next, amb = chainer !result treatments in
      result := next;
      ambiguities := amb;
    done;
    if !ambiguities >= 0
    then chainer ~run_anyway:true !result
           [("bt_fin", elaborate_bottom_top_pass context);
            ("tb_fin", elaborate_top_bottom_pass context noose_default)]
    else !result, !ambiguities
  in
  let handle uexpr =
    add_string "\n(* considering untyped expression: ";
    add_string (string_of_uexpr uexpr);
    add_string " *)\n";
    try
      let fexpr = fexpr_of_uexpr context uexpr noose_default in
      add_string "\t(* fexpr";
      add_string " (ambiguities left: ";
      add_string (string_of_int (fexpr_count_ambiguities fexpr));
      add_string "): ";
      add_string (string_of_fexpr fexpr);
      add_string " *)\n";
      let partial, ambiguities = insist fexpr in
      if ambiguities = 0 then
        let complete, ambiguities = run "coercions" (elaborate_coercions context) partial in
        if ambiguities = 0 then
          begin
            add_string "Check ";
            add_string (string_of_fexpr ~with_annot:false complete);
            add_string ".\n"
          end
    with Invalid_argument msg ->
      add_string "\t(* turning to fexpr raises an exception: ";
      add_string msg;
      add_string " *)\n"
  in
  add_coq_to context;
  add_NZ_to context;
  context_local_work context (fun () ->
      add_string "Require Import ZArith.\n";
      add_string "Section Main.\n";
      context_add_variable context "a" ~for_coq:true (noose_of_ftype (FBasic "N"));
      context_add_variable context "b" ~for_coq:true (noose_of_ftype (FBasic "N"));
      context_add_variable context "c" ~for_coq:true (noose_of_ftype (FBasic "Z"));
      add_string (string_of_context ~for_coq:true context);
      iter handle uexpr_for_basic_tests;
      add_string "End Main.\n";
      print_string (Buffer.contents buf));
  Buffer.reset buf;
  context_local_work context (fun () ->
      add_string "Section Vector.\n";
      add_field context "K";
      add_vector_space context "E" "K";
      add_subvector_space context "F" "E" "K";
      add_subvector_space context "G" "E" "K";
      context_add_variable context "f" ~for_coq:true (noose_of_ftype (FBasic "F"));
      context_add_variable context "g" ~for_coq:true (noose_of_ftype (FBasic "G"));
      context_add_variable context "k" ~for_coq:true (noose_of_ftype (FBasic "K"));
      add_string (string_of_context ~for_coq:true context);
      iter handle uexpr_for_vector_tests;
      add_string "End Vector.\n";
      print_string (Buffer.contents buf));
  Buffer.reset buf;
  context_local_work context (fun () ->
      add_string "Section Functions.\n";
      add_field context "K";
      add_numeric_functions_on context "I" "K";
      context_add_variable context "f" ~for_coq:true (noose_of_ftype (FFunct (FBasic "I", FBasic "K")));
      context_add_variable context "g" ~for_coq:true (noose_of_ftype (FFunct (FBasic "I", FBasic "K")));
      context_add_variable context "h" ~for_coq:true (noose_of_ftype (FFunct (FBasic "I", FBasic "N")));
      context_add_variable context "k" ~for_coq:true (noose_of_ftype (FBasic "K"));
      add_string (string_of_context ~for_coq:true context);
      iter handle uexpr_for_functions_tests;
      add_string "End Functions.\n";
      print_string (Buffer.contents buf));
  Buffer.reset buf;
  context_local_work context (fun () ->
      add_string "Section Filters.\n";
      add_filters context;
      context_add_variable context "x" ~for_coq:true (noose_of_ftype (FBasic "R"));
      context_add_variable context "v" ~for_coq:true (noose_of_ftype (FFunct (FBasic "N", FBasic "R")));
      context_add_variable context "f" ~for_coq:true (noose_of_ftype (FFunct (FBasic "R", FBasic "R")));
      add_string (string_of_context ~for_coq:true context);
      iter handle uexpr_for_filter_tests;
      add_string "End Filters.\n";
      print_string (Buffer.contents buf));
  ()
