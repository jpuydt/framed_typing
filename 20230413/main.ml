(* in this file:

- manage to interpret mixed untyped expressions

defects :

- none, it's perfect

 *)

(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     (match option_list_of_list_option tail with
      | Some tail_ -> some (head :: tail_)
      | None -> none)
  | None :: tail -> none
  | _ -> some []

let rec list_product_map (f: 'a -> 'b list) (l: 'a list) =
  match l with
  | [] -> []
  | head :: [] -> map (fun a -> [a]) (f head)
  | head :: tail -> concat_map
                      (fun l -> map (fun a -> a :: l) (f head))
                      (list_product_map f tail)

let rec chain_of_responsibility workers work =
  match workers with
  | worker :: tail ->
     (match worker work with
      | Some result -> some result
      | None -> chain_of_responsibility tail work)
  | _ -> none

let rec chain_of_contribution workers work =
  match workers with
  | worker :: tail ->
     (match worker work with
      | Some result -> result :: (chain_of_contribution tail work)
      | None -> chain_of_contribution tail work)
  | _ -> []

(* UNTYPED EXPRESSIONS *)

type uexpr = | UConstant of string
             | UApp of uexpr * uexpr list

let uoperator name arg1 arg2 = UApp (UConstant name, [arg1; arg2])

let rec string_of_uexpr ue =
  match ue with
  | UConstant name -> name
  | UApp (app, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_uexpr app);
     iter (fun arg -> add_string " "; add_string (string_of_uexpr arg)) args;
     add_string ")";
     Buffer.contents buf

(* COQ TYPES *)
(* simplified... *)

type ctype = | CBasic of string
             | CFunctional of ctype * (ctype list)

let ctype_comp ct1 ct2 =
  match ct1 with
  | CBasic _ -> none
  | CFunctional (result, []) -> none
  | CFunctional (result, [head] ) ->
     if head = ct2 then some result
     else none
  | CFunctional (result, head :: tail ) ->
     if head = ct2 then some (CFunctional (result, tail))
     else none

let rec ctype_comps ct1 cts =
  match cts with
  | [] -> some ct1
  | ct :: other ->
     let* ctnext = ctype_comp ct1 ct in
     ctype_comps ctnext other

let rec string_of_ctype ct =
  match ct with
  | CBasic name -> name
  | CFunctional (result, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     iter (fun arg -> add_string (string_of_ctype arg);
                      add_string " -> ") args;
     add_string (string_of_ctype result);
     add_string ")";
     Buffer.contents buf

let ctype_predicate ct =
  CFunctional (CBasic "Prop", [ct])

let ctype_relation ct =
  CFunctional (CBasic "Prop", [ct; ct])

let ctype_operator ct =
  CFunctional (ct, [ct; ct])

(* COQ EXPRESSIONS *)
(* simplified... *)

type cexpr = | CConstant of string * ctype (* name and type *)
             | CApp of cexpr * cexpr list

let rec cexpr_typecheck ce =
  match ce with
  | CConstant (_, ct) -> some ct
  | CApp (fn, args) ->
     let* fnct = cexpr_typecheck fn in
     let* argsct = option_list_of_list_option (map cexpr_typecheck args) in
     ctype_comps fnct argsct

(* this is how we can find the simpler expression! *)
let rec cexpr_weight ce =
  match ce with
  | CConstant _ -> 1
  | CApp (fn, args) ->
     List.fold_left (fun sum ce_ -> sum + (cexpr_weight ce_)) 0 (fn :: args)

let rec string_of_cexpr ?(with_ctype = true) ce =
  match ce with
  | CConstant (name, _) ->
     if with_ctype then (
       let buf = Buffer.create 0 in
       let add_string = Buffer.add_string buf in
       add_string name;
       add_string ": ";
       (match cexpr_typecheck ce with
        | Some ct -> add_string (string_of_ctype ct)
        | None -> add_string "impossible!");
       Buffer.contents buf
     ) else name
  | CApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_cexpr ~with_ctype:false fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_cexpr ~with_ctype:false arg)) args;
     add_string ")";
     if with_ctype then (
       add_string ": ";
       match cexpr_typecheck ce with
       | Some ct -> add_string (string_of_ctype ct)
       | None -> add_string "impossible!"
     );
     Buffer.contents buf

(* CONTEXT *)

module StringMap = Map.Make(String) (* FIXME: can't I have a trie? *)

type context = {
    (* Coq-visible part *)
    mutable variables: (ctype * bool) StringMap.t; (* boolean for internal *)
    mutable hypotheses: (cexpr * bool) StringMap.t; (* boolean for internal *)

    (* frame *)
    mutable parsers: (string -> cexpr option) list; (* recognize constants *)
    mutable pushers: (cexpr -> cexpr) list; (* typechecking will clean *)
  }

let context_empty () = {
    variables = StringMap.empty;
    hypotheses = StringMap.empty;
    parsers = [];
    pushers = [fun p -> p];
  }

let context_add_variable ?(internal = false) context name ctype =
  context.variables <- StringMap.add name (ctype, internal) context.variables

let context_add_hypothesis ?(internal = false) context name cexpr =
  context.hypotheses <- StringMap.add name (cexpr, internal) context.hypotheses

let context_add_parser context parser =
  context.parsers <- parser :: context.parsers

let context_add_pusher context pusher =
  context.pushers <- pusher :: context.pushers

let context_interpret_constant context name =
  let variable_parser name =
    match StringMap.find_opt name context.variables with
    | Some (ctype, _) -> some (CConstant (name, ctype))
    | None -> none
  in
  chain_of_contribution (variable_parser :: context.parsers) name

let rec context_interpret_uexpr_all context ue =
  let run_pushers ce = map (fun pusher -> pusher ce) context.pushers in
  match ue with
  | UConstant name ->
     concat_map run_pushers (context_interpret_constant context name)
  | UApp (fn, args) ->
     let fns = context_interpret_uexpr_all context fn in
     let argss = list_product_map (context_interpret_uexpr_all context) args in
     let possible = concat_map run_pushers (concat_map (fun fn_ -> map (fun args_ -> CApp (fn_, args_)) argss) fns) in
     filter (fun ce -> is_some (cexpr_typecheck ce)) possible

let context_interpret_uexpr context ue =
  match
    List.sort
      (fun ue1 ue2 -> Int.compare (cexpr_weight ue1) (cexpr_weight ue2))
      (context_interpret_uexpr_all context ue)
  with
  | hd :: _ -> some hd
  | _ -> none

let string_of_context context =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let var_helper name (ctype, internal) =
    if not internal then (
      add_string "Variable ";
      add_string name;
      add_string ": ";
      add_string (string_of_ctype ctype);
      add_string ".\n"
    )
  and hyp_helper name (cexpr, internal) =
    if not internal then (
      add_string "Hypothesis ";
      add_string name;
      add_string ": ";
      add_string (string_of_cexpr ~with_ctype:true cexpr);
      add_string ".\n"
    )
  in
  StringMap.iter var_helper context.variables;
  StringMap.iter hyp_helper context.hypotheses;
  Buffer.contents buf

(* N CONTEXT *)
(* only useful because we're out of Coq *)

let context_add_coq_N context =
  let typename = CBasic "N" in
  context_add_variable ~internal:true context "N.add" (ctype_operator typename);
  context_add_variable ~internal:true context "N.mul" (ctype_operator typename);
  context_add_variable ~internal:true context "N.eq" (ctype_relation typename);
  context_add_variable ~internal:true context "N.le" (ctype_relation typename)

(* Z CONTEXT *)
(* only useful because we're out of Coq *)

let context_add_coq_Z context =
  let typename = CBasic "Z" in
  context_add_variable ~internal:true context "Z.add" (ctype_operator typename);
  context_add_variable ~internal:true context "Z.mul" (ctype_operator typename);
  context_add_variable ~internal:true context "Z.eq" (ctype_relation typename);
  context_add_variable ~internal:true context "Z.le" (ctype_relation typename)

(* N FRAME *)

let context_add_N context =
  let typename = CBasic "N" in
  let cst_parser value =
    if Str.string_match (Str.regexp "[0-9]+") value 0
    then some (CConstant (value, typename))
    else none
  and ops_parser value =
    assoc_opt value
      [
        ("eq", CConstant ("N.eq", ctype_relation typename));
        ("le", CConstant ("N.le", ctype_relation typename));
        ("add", CConstant ("N.add", ctype_operator typename));
        ("mul", CConstant ("N.mul", ctype_operator typename));
      ]
  in
  context_add_parser context cst_parser;
  context_add_parser context ops_parser

(* Z FRAME *)

let context_add_Z context =
  let typename = CBasic "Z" in
  let cst_parser value =
    if Str.string_match (Str.regexp "-?[0-9]+") value 0
    then
      if String.get value 0 = '-' then
        some (CConstant ("(" ^ value ^ ")%Z", typename))
      else
        some (CConstant (value, typename))
    else none
  and ops_parser value =
    assoc_opt value
      [
        ("eq", CConstant ("Z.eq", ctype_relation typename));
        ("le", CConstant ("Z.le", ctype_relation typename));
        ("add", CConstant ("Z.add", ctype_operator typename));
        ("mul", CConstant ("Z.mul", ctype_operator typename));
      ]
  in
  context_add_parser context cst_parser;
  context_add_parser context ops_parser

(* Z OF N FRAME *)

let context_add_Z_of_N context =
  let pusher ce = CApp (CConstant ("Z.of_N",
                                   CFunctional (CBasic "Z", [CBasic "N"])), [ce]) in
  context_add_pusher context pusher
  
(* TEST CODE *)

let print_opening buf =
  let add_string = Buffer.add_string buf in
  let add_line line = add_string line; add_string "\n" in
  add_line "Require Import ZArith.";
  add_line "Section Main."

let print_closing buf = Buffer.add_string buf "End Main.\n"
                      
let main () =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let context = context_empty () in
  let helper action ue =
    match context_interpret_uexpr context ue with
    | Some ce -> action ce
    | None ->
       add_string "(* couldn't interpret untyped expression: ";
       add_string (string_of_uexpr ue);
       add_string " *)\n"
  in
  (* this is because we're out of Coq *)
  context_add_coq_N context;
  context_add_coq_Z context;

  (* this is because we want to type mixed expressions *)
  context_add_N context;
  context_add_Z context;
  context_add_Z_of_N context;

  (* now we get to actually work *)
  context_add_variable context "a" (CBasic "N");
  context_add_variable context "b" (CBasic "Z");
  helper (context_add_hypothesis context "Ha") (UApp (UConstant "le", [UConstant "2"; UConstant "a"]));
  helper (context_add_hypothesis context "Ha2") (UApp (UConstant "le", [UConstant "-2"; UConstant "a"]));
  helper (context_add_hypothesis context "Hb") (UApp (UConstant "le", [UConstant "2"; UConstant "b"]));
  helper (context_add_hypothesis context "Hb2") (UApp (UConstant "le", [UConstant "-2"; UConstant "b"]));
  print_opening buf;
  add_string (string_of_context context);
  print_closing buf;
  print_string (Buffer.contents buf)

;; main ()
