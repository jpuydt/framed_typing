open Option
open List

let my_raii pre post action =
  pre ();
  try
    let res = action () in
    post ();
    res
  with whatever ->
    post ();
    raise whatever

let ( let* ) o f =
  match o with
  | None -> None
  | Some x -> f x

module NotTrie = Map.Make(String) (* FIXME: I'd prefer a real trie *)

let string_of_list ?(separator= " ") stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string separator;
                      helper tail
  in
  helper list;
  Buffer.contents buf

let rec list_uniq l =
  match l with
  | [] -> []
  | head::tail ->
     if mem head tail
     then list_uniq tail
     else head :: (list_uniq tail)

let my_map_product f l1 l2 = (* Seq has that since OCaml 4.14 *)
  let helper o l2 = map (fun x -> f o x) l2 in
  concat_map (fun o -> helper o l2) l1

let rec my_option_map2 f l1 l2 =
  match l1, l2 with
  | [], [] ->
     some []
  | car1 :: cdr1, car2 :: cdr2 ->
     let* car = f car1 car2 in
     let* cdr = my_option_map2 f cdr1 cdr2 in
     some (car :: cdr)
  | _ -> raise (Invalid_argument ("different length lists!"))

let my_option_product_map f l1 l2 =
  let helper o l2 = filter_map (f o) l2 in
  concat_map (fun o -> helper o l2) l1

let my_find_map2 f l1 l2 =
  let helper o = find_map (f o) l2 in
  find_map helper l1
