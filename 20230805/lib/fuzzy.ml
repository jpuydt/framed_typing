open Either
open Option
open List

open Tools
open Coq



(* FUZZY TYPES *)

type ftype = (ctype, ctype list) Either.t

let ftype_normalize = function
  | Left ct ->
     left ct
  | Right [ct] ->
     left ct
  | Right cts_pre ->
     let cts_post = list_uniq cts_pre in
     begin
       match cts_post with
       | [ct] ->
          left ct
       | _ ->
          right cts_post
     end

let string_of_ftype (ftype: ftype) =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  begin
    match ftype with
    | Left ctype ->
       add_string (string_of_ctype ctype)
    | Right ctypes ->
         add_string "{ ";
         add_string (string_of_list ~separator:", " string_of_ctype ctypes);
         add_string " }"
  end;
  Buffer.contents buf

let ftype_funct (src_ftype: ftype) (dst_ftype: ftype): ftype =
  match src_ftype, dst_ftype with
  | Left src_ctype, Left dst_ctype ->
     Left (ctype_funct src_ctype dst_ctype)
  | Left src_ctype, Right dst_ctypes ->
     Right (map (ctype_funct src_ctype) dst_ctypes)
  | Right src_ctypes, Left dst_ctype ->
     Right (map (fun src -> ctype_funct src dst_ctype) src_ctypes)
  | Right src_ctypes, Right dst_ctypes ->
     Right (my_map_product ctype_funct src_ctypes dst_ctypes)

let ftype_lift (fnct: ctype -> ctype): ftype -> ftype = function
  | Left ct ->
     left (fnct ct)
  | Right cts ->
     ftype_normalize (right (map fnct cts))

let ftype_function_source = ftype_lift ctype_function_source

let ftype_function_destination = ftype_lift ctype_function_destination

let ftype_funct_from = ftype_lift ctype_funct_from

let ftype_funct_to = ftype_lift ctype_funct_to

let ftype_union (ftype1: ftype) (ftype2: ftype): ftype =
  let pre =
    match ftype1, ftype2 with
    | Left ctype1, Left ctype2 ->
       right [ctype1; ctype2]
    | Left ctype1, Right ctypes2 ->
       right (ctype1 :: ctypes2)
    | Right ctypes1, Left ctype2 ->
       right (ctype2 :: ctypes1)
    | Right ctypes1, Right ctypes2 ->
       right (ctypes1 @ ctypes2)
  in
  ftype_normalize pre



(* FRAME *)

type frame =
  {
    (* source, destination and the names of the coercions proving the inequality *)
    mutable le: (ctype * ctype * string list) list;
  }

let frame_new () =
  {
    le = [];
  }

let frame_add_coercion frame name source target =
  let helper (src, dst, path) =
    if src = target then
      [(src, dst, path); (source, dst, name :: path)]
    else if dst = source then
      [(src, dst, path); (src, target, path @ [name])]
    else [(src, dst, path)]
  in
  frame.le <- (source, target, [name]) :: (concat_map helper frame.le)

let frame_coercion_path frame src dst =
  if src = dst
  then []
  else
    let (_, _, path) = find (fun (src_, dst_, _) -> src = src_ && dst = dst_) frame.le in
    path

let rec frame_le frame ct1 ct2 =
  if ct1 = ct2 then
    true
  else if exists (fun (src, dst, _) -> src = ct1 && dst = ct2) frame.le then
    true
  else
    match ct1, ct2 with
    | CFunct (arg1, res1), CFunct (arg2, res2) ->
       (frame_le frame arg2 arg1) && (frame_le frame res1 res2)
    | _, _ ->
       false

let frame_ctype_is_compatible frame known used =
  frame_le frame known used || is_some (ctype_unify known used)

let frame_ftype_is_compatible frame (known: ftype) (used: ftype) =
  match known, used with
  | Left known, Left used ->
     frame_ctype_is_compatible frame known used
  | Left known, Right used ->
     exists (frame_ctype_is_compatible frame known) used
  | Right known, Left used ->
     exists (fun ct -> frame_ctype_is_compatible frame ct used) known
  | Right known, Right used ->
     exists2 (frame_ctype_is_compatible frame) known used

let frame_ftype_above_ctype frame ct: ftype =
  let others = filter_map (fun (src, dst, _) ->
                   if ct = src
                   then some dst
                   else none) frame.le
  in
  ftype_normalize (right (ct :: others))

let frame_ftype_above_ftype frame (ftype: ftype) =
  match ftype with
  | Left ctype ->
     frame_ftype_above_ctype frame ctype
  | Right ctypes ->
     let rec helper ctypes =
       match ctypes with
       | [] ->
          Right []
       | ctype :: rest ->
          ftype_union (frame_ftype_above_ctype frame ctype) (helper rest)
     in
     helper ctypes

let frame_ftype_tighten frame known_ftype used_ftype =
  let msg () =
    "[frame_ftype_tighten] an expression known as "
    ^ (string_of_ftype known_ftype)
    ^ " and used as "
    ^ (string_of_ftype used_ftype)
    ^ " is incoherent"
  and compat known used =
    if frame_le frame known used
    then some known
    else ctype_unify known used
  in
  match known_ftype, used_ftype with
  | Left known, Left used ->
     begin
       match compat known used with
       | Some common ->
          left common
       | None ->
          raise (Invalid_argument (msg ()))
     end
  | Left known, Right used ->
     ftype_normalize (right (filter_map (compat known) used))
  | Right known, Left used ->
     ftype_normalize (right (filter_map (fun ct -> compat ct used) known))
  | Right known, Right used ->
     ftype_normalize (right (my_option_product_map compat known used))

let frame_app_ftypes frame (fn_ftype: ftype) (arg_ftype: ftype) (res_ftype: ftype)
  : ftype * ftype * ftype =
  let helper fn_ctype arg_ctype =
    let from_arg_ctype = ctype_funct_from arg_ctype
    and to_res_ftype = ftype_funct_to res_ftype
    in
    match ctype_unify from_arg_ctype fn_ctype with
    | Some common ->
       begin
         if frame_ftype_is_compatible frame (left common) to_res_ftype
         then
           some (common,
                 ctype_function_source common,
                 ctype_function_destination common)
         else
           none
       end
    | None ->
       begin
         let fn_src_ctype = ctype_function_source fn_ctype
         and fn_dst_ctype = ctype_function_destination fn_ctype
         in
         if frame_ctype_is_compatible frame arg_ctype fn_src_ctype
            && frame_ftype_is_compatible frame (left fn_dst_ctype) res_ftype
         then
           some (fn_ctype, arg_ctype, fn_dst_ctype)
         else
           none
      end
  in
  let raw =
    match fn_ftype, arg_ftype with
    | Left fn_ctype, Left arg_ctype ->
       fold ~none:[] ~some:(fun c -> [c]) (helper fn_ctype arg_ctype)
    | Left fn_ctype, Right arg_ctypes ->
       my_option_product_map helper [fn_ctype] arg_ctypes
    | Right fn_ctypes, Left arg_ctype ->
       my_option_product_map helper fn_ctypes [arg_ctype]
    | Right fn_ctypes, Right arg_ctypes ->
       my_option_product_map helper fn_ctypes arg_ctypes
  in
  let fns = ref []
  and args = ref []
  and ress = ref []
  in
  iter (fun (a, b, c) ->
      fns := a :: !fns;
      args := b :: !args;
      ress := c :: !ress) raw;
  ftype_normalize (right !fns), ftype_normalize (right !args), ftype_normalize (right !ress)



(* SYMBOLS *)

type symbol = string * (ctype * cexpr) list (* symbol-name (level, instance) *)

let symbol_new name: symbol = name, []

let symbol_name (symbol: symbol) = fst symbol

let symbol_is_empty (symbol: symbol) = [] = snd symbol

let symbol_is_definite (symbol: symbol) = 1 = length (snd symbol)

let symbol_add_instance (symbol: symbol) level cexpr: symbol =
  let name, instances = symbol in
  name, instances @ [level, cexpr]

let symbol_ftype frame (symbol: symbol): ftype =
  fold_left ftype_union (right [])
    (map (fun (_, instance) -> frame_ftype_above_ctype frame (ctype_of_cexpr instance)) (snd symbol))

let symbol_tighten (frame: frame) (symbol: symbol) (used: ftype): symbol =
  let used = Either.fold ~left:(fun ct -> [ct]) ~right:(fun cts -> cts) used in
  let name, instances = symbol in
  name, filter (fun (_, cexpr) ->
            let ctype = ctype_of_cexpr cexpr in
            exists (fun ct -> frame_ctype_is_compatible frame ctype ct) used) instances

let symbol_first_instance (symbol: symbol): cexpr =
  let name, instances = symbol in
  match instances with
  | [] ->
     raise (Invalid_argument ("Symbol `"
                              ^ name
                              ^ "` doesn't have any instance!"))
  | (_, res) :: _ ->
     res

let symbol_ambiguities (symbol: symbol) = length (snd symbol) - 1

let string_of_symbol (symbol: symbol) =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf
  and name, instances = symbol in
  add_string name;
  add_string " [ ";
  add_string (string_of_list ~separator:", "
                (fun instance -> string_of_cexpr (snd instance)) instances);
  add_string " ]";
  Buffer.contents buf



(* FUZZY EXPRESSIONS *)

type fexpr = | FCoq of cexpr * ftype (* Coq expression and known constraint *)
             | FVar of string * ftype (* variable name and known type *)
             | FSymb of symbol * ftype (* symbol and used type *)
             | FApp of fexpr * fexpr * ftype (* function, argument and result used types *)
             | FLambda of string * ftype * fexpr * ftype (* variable name and type,
                                                            body and its used types *)
             | FLet of string * fexpr * ftype * fexpr * ftype (* variable name, definition
                                                                 and its known type and body
                                                                 and its known type *)

let ftype_of_fexpr = function
  | FCoq (_, res) | FVar (_, res) | FSymb (_, res) | FApp (_, _, res) | FLet (_, _, _, _, res)
    -> res
  | FLambda (_, arg , _, res) ->
     ftype_funct arg res

let rec fexpr_fuzzy_height = function
  | FCoq _ ->
     0
  | FVar _ ->
     1
  | FSymb _ ->
     1
  | FApp (fnct, arg, _) ->
     1 + max (fexpr_fuzzy_height fnct) (fexpr_fuzzy_height arg)
  | FLambda (_, _, body, _) ->
     1 + fexpr_fuzzy_height body
  | FLet (_, def, _, body, _) ->
     max (fexpr_fuzzy_height def) (fexpr_fuzzy_height body)

let rec fexpr_ambiguities = function
  | FCoq (_, known) ->
     if is_left known then 0 else 1
  | FVar (_, known) ->
     if is_left known then 0 else 1
  | FSymb (symbol, _) ->
     symbol_ambiguities symbol
  | FApp (fn, arg, _) ->
     (fexpr_ambiguities fn) + (fexpr_ambiguities arg)
  | FLambda (_, var_ftype, body, _) ->
     (if is_left var_ftype then 0 else 1) + fexpr_ambiguities body
  | FLet (_, def, def_ftype, body, body_ftype) ->
     (if is_left def_ftype then 0 else 1) + fexpr_ambiguities def
     + (if is_left body_ftype then 0 else 1) + fexpr_ambiguities body

let string_of_fexpr ?(with_annot=true) fexpr =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let add_annot ftype =
    if with_annot
    then
      begin
        add_string ": ";
        add_string (string_of_ftype ftype)
      end
  in
  let rec s_of_fe fe =
    match fe with
    | FCoq (cexpr, ftype) ->
       add_string (string_of_cexpr cexpr);
       add_annot ftype
    | FVar (name, ftype) ->
       add_string ("?" ^ name);
       add_annot ftype
    | FSymb (symbol, ftype) ->
       add_string (string_of_symbol symbol);
       add_annot ftype
    | FApp (fnct, arg, noose) ->
       add_string "(";
       s_of_fe fnct;
       add_string " ";
       s_of_fe arg;
       add_string ")";
       add_annot noose;
    | FLambda (var_name, var_ftype, body, body_ftype) ->
       add_string "(fun ";
       add_string var_name;
       add_annot var_ftype;
       add_string " => ";
       s_of_fe body;
       add_string ")";
       add_annot (ftype_funct var_ftype body_ftype)
    | FLet (var_name, def, def_ftype, body, body_ftype) ->
       add_string "(let ";
       add_string var_name;
       add_string " = (";
       s_of_fe def;
       add_string ") ";
       add_annot def_ftype;
       add_string " in ";
       s_of_fe body;
       add_string ")";
       add_annot body_ftype
  in
  s_of_fe fexpr;
  Buffer.contents buf

let rec string_of_fexpr_plain fexpr =
  match fexpr with
  | FCoq (cexpr, ftype) ->
     "FCoq (" ^ (string_of_cexpr cexpr) ^ ", " ^ (string_of_ftype ftype) ^ ")"
  | FVar (name, ftype) ->
     "FVar (" ^ name ^ ", " ^ (string_of_ftype ftype) ^ ")"
  | FSymb (symbol, ftype) ->
     "FSymb (" ^ (string_of_symbol symbol) ^ ", " ^ (string_of_ftype ftype) ^ ")"
  | FApp (fnct, arg, ftype) ->
     "FApp (" ^ (string_of_fexpr_plain fnct) ^ ", " ^ (string_of_fexpr_plain arg)
     ^ (string_of_ftype ftype) ^ ")"
  | FLambda (var_name, var_ftype, body, body_ftype) ->
     "FLambda (" ^ var_name ^ ", " ^ (string_of_ftype var_ftype) ^ ", "
     ^ (string_of_fexpr_plain body) ^ ", " ^ (string_of_ftype body_ftype) ^ ")"
  | FLet (var_name, def, def_ftype, body, body_ftype) ->
     "FLet (" ^ var_name
     ^ ", " ^ (string_of_fexpr_plain def) ^ ", " ^ (string_of_ftype def_ftype)
     ^ ", " ^ (string_of_fexpr_plain body) ^ ", " ^ (string_of_ftype body_ftype)
     ^ ")"
