%token EOF
%token LBRACK RBRACK COLON
%token ARROW MAPSTO LET IN LAMBDA EQUAL
%token<string> STRING

%{
    open Coq
    open Untyped
    
    let uexpr_impose ue_ ctype =
      match ue_ with
      | UString (str, _) -> UString (str, Some ctype)
      | UApp (fnct, arg, _) -> UApp (fnct, arg, Some ctype)
      | ULambda (name, octype, expr, _) -> ULambda (name, octype, expr , Some ctype)
      | ULet (name, uexpr, octype, expr, _) -> ULet (name, uexpr, octype, expr, Some ctype)

    let rec uexpr_of_list uexprs =
      match uexprs with
      | [] -> raise (Invalid_argument "Shouldn't happen!")
      | uexpr :: [] -> uexpr
      | fnct :: arg :: [] -> UApp (fnct, arg, None)
      | fnct :: arg :: rest -> uexpr_of_list ((UApp (fnct, arg, None)) :: rest)

%}

%type <uexpr>main
%start main

%%

main: uexpr EOF { $1 }

uexpr:
  | uexpr_basic { $1 }
  | uexpr_list { uexpr_of_list $1 }

uexpr_list:
  | uexpr_basic uexpr_basic { [$1; $2] }
  | uexpr_basic uexpr_list { $1 :: $2 }

uexpr_basic:
  | LBRACK uexpr RBRACK { $2 }
  | uexpr COLON ctype { uexpr_impose $1 $3 }
  | LAMBDA STRING MAPSTO uexpr { ULambda ($2, None, $4, None) }
  | LAMBDA STRING COLON ctype MAPSTO uexpr { ULambda ($2, Some $4, $6, None) }
  | LET STRING EQUAL uexpr IN uexpr { ULet ($2, $4, None, $6, None) }
  | STRING { UString ($1, None) }

ctype:
  | ctype_simple { $1 }
  | ctype_param { $1 }

ctype_simple:
  | STRING { CBasic $1 }
  | LBRACK ctype RBRACK { $2 }
  | ctype ARROW ctype {  CFunct ($1, $3) }

ctype_param:
  | STRING ctype+ { CParam ($1, $2) }
