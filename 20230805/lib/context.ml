open Option
open List

open Tools
open Coq
open Fuzzy

(* VARIABLE LAYERS *)

type layer =
  {
    mutable variables: ftype NotTrie.t;
  }

let layer_new () =
  {
    variables = NotTrie.empty;
  }

let layer_has_variable layer name = NotTrie.mem name layer.variables

let layer_add_variable layer name ftype =
  layer.variables <- NotTrie.update name (fun _ -> some ftype) layer.variables

let layer_variable_ftype layer name =
  NotTrie.find_opt name layer.variables

let string_of_layer layer =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  add_string "(* layer of variables:\n";
  NotTrie.iter (fun name ftype ->
      add_string name;
      add_string ": ";
      add_string (string_of_ftype ftype);
      add_string "\n"
    ) layer.variables;
  add_string "*)\n";
  Buffer.contents buf



(* CONTEXT *)

type context = {
    mutable coq: Buffer.t;
    mutable symbols: symbol NotTrie.t;
    mutable constant_parsers: (string -> cexpr option) list;
    mutable layers: layer list;
    mutable frame: frame;
  }

let context_new () =
  {
    coq = Buffer.create 0;
    symbols = NotTrie.empty;
    constant_parsers = [];
    layers = [];
    frame = frame_new ();
  }

let context_add_coq_line context string =
  Buffer.add_string context.coq string;
  Buffer.add_string context.coq "\n"

let context_local_work context action =
  let context_push_layer () =
    context.layers <- (layer_new ()) :: context.layers
  and context_pop_layer () =
    match context.layers with
    | [] | [_] -> context.layers <- raise (Invalid_argument "won't happen!")
    | _ :: rest -> context.layers <- rest
  in
  my_raii context_push_layer context_pop_layer action

let context_add_constant_parser context parser =
  context.constant_parsers <- context.constant_parsers @ [parser]

let context_add_variable context ?(for_coq = false) name ftype =
  if for_coq then
    context_add_coq_line context ("Variable "
                                  ^ name
                                  ^ ": "
                                  ^ (string_of_ftype ftype)
                                  ^ ".");
  match context.layers with
  | [] ->
     let layer = layer_new () in
     context.layers <- [layer];
     layer_add_variable layer name ftype
  | layer :: _ ->
     layer_add_variable layer name ftype

let context_update_variable context name ftype =
  let helper layer =
    if layer_has_variable layer name
    then
      begin
        layer_add_variable layer name ftype;
        false
      end
    else
      true
  in
  if for_all helper context.layers then
    raise (Invalid_argument ("[context_update_variable] variable `"
                             ^ name
                             ^ "` doesn't exist"))

let context_variable_ftype context name =
  let rec finder layers =
    match layers with
    | [] -> none
    | layer :: rest ->
       begin
         match layer_variable_ftype layer name with
         | None -> finder rest
         | Some result -> some result
       end
  in
finder context.layers

let symbol_normalize frame (symbol: symbol): symbol =
  let name, instances = symbol in
  let helper (ct1, _) (ct2, _) =
    if frame_le frame ct1 ct2 then -1
    else if frame_le frame ct2 ct1 then 1
    else 0
  in
  name, stable_sort helper instances

let context_add_coercion context name source target =
  frame_add_coercion context.frame name source target;
  context.symbols <- NotTrie.map (fun symbol -> symbol_normalize context.frame symbol) context.symbols

let context_add_symbol context name level cexpr =
  context.symbols <- NotTrie.update name
                       (fun osymb
                        -> let symb = value ~default:(symbol_new name) osymb in
                           some (symbol_add_instance symb level cexpr))
                             context.symbols

let context_parse_symbol context name =
  match NotTrie.find_opt name context.symbols with
  | Some symbol ->
     symbol
  | None ->
     begin
       match filter_map (fun parser -> parser name) context.constant_parsers with
       | [] ->
          raise (Invalid_argument ("Constant symbol `" ^ name ^ "` is unknown in the context!"))
       | cexprs ->
          let raw =
            fold_left
              (fun symb cexpr -> symbol_add_instance symb (ctype_of_cexpr cexpr) cexpr)
              (symbol_new name)
              cexprs
          in
          symbol_normalize context.frame raw
     end

let string_of_context context =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let string_of_layers () =
    add_string "\n(* known fuzzy variables, by layer: *)\n\n";
    iter (fun layer -> add_string (string_of_layer layer)) context.layers;
    add_string "\n\n"
  and string_of_symbols () =
    add_string "\n(* available symbols for elaboration:\n";
    NotTrie.iter (fun _ symbol ->
        add_string "\t";
        add_string (string_of_symbol symbol);
        add_string "\n") context.symbols;
    add_string "*)\n\n"
  in
  string_of_layers ();
  string_of_symbols ();
  add_string "(* active Coq code follows: *)\n\n";
  add_string (Buffer.contents context.coq);
  Buffer.contents buf
