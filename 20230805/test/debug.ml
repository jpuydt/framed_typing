open Either

open Elaboration.Coq
open Elaboration.Context
open Elaboration.Elaborate
open Elaboration.Fuzzy

open Test_tools


let eq3_typer ct = CFunct (ct, ctype_relation ct);;

let _ =
  let context = context_new () in
  add_coq_preamble context;
  add_coq_to context;
  add_NZ_to context;

  context_add_variable context ~for_coq:true "eq3" (left (eq3_typer (CBruijn 0)));
  context_add_variable context ~for_coq:true "a" (left (CBasic "N"));
  context_add_variable context ~for_coq:true "b" (left (CBasic "N"));
  context_add_variable context ~for_coq:true "c" (left (CBasic "Z"));

  let fn_ftype, arg_ftype, res_ftype =
    frame_app_ftypes context.frame
      (right [eq3_typer (CBruijn 0)])
      (right [CBasic "N"; CBasic "Z"])
      (right [CBruijn 0])
  in
  print_endline ("fn_ftype = " ^ (string_of_ftype fn_ftype));
  print_endline ("arg_ftype = " ^ (string_of_ftype arg_ftype));
  print_endline ("res_ftype = " ^ (string_of_ftype res_ftype));
  print_endline "";

  let fexpr =
    FApp (FCoq (CConst ("eq3", eq3_typer (CBruijn 0)), (left (eq3_typer (CBruijn 0)))),
          FCoq (CConst ("a", CBasic "N"), right [CBasic "N"; CBasic "Z"]),
          left (CBruijn 0))
  in
  let second = elaborate_bottom_top_pass context fexpr in
  print_endline ("fexpr = " ^ (string_of_fexpr fexpr));
  print_endline ("second = " ^ (string_of_fexpr second));
  print_endline "";

  let fexpr = FCoq (CConst ("eq3", eq3_typer (CBruijn 0)), left (eq3_typer (CBruijn 0))) in
  let second =
    elaborate_impose context fexpr (right [eq3_typer (CBasic "N"); eq3_typer (CBasic "Z")])
  in
  print_endline ("fexpr = " ^ (string_of_fexpr fexpr));
  print_endline ("second = " ^ (string_of_fexpr second));
  print_endline "";

  let ftype = left (eq3_typer (CBruijn 0)) in
  let used = right [eq3_typer (CBasic "N"); eq3_typer (CBasic "Z")] in
  let tight = frame_ftype_tighten context.frame ftype used in
  print_endline ("tight = " ^ (string_of_ftype tight));
  ()
