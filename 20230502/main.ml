(* in this file:

- untyped expressions get interpreted as typed expression using (what
   I understood of) Charguéraud's algorithm

- for this, there's an intermediary step with fuzzy types and
   expressions before coming to full types and expressions

- natural number constants only get recognized when their surroundings
   impose what they are -- otherwise they could be relative numbers

- the notion of symbol is interesting: they are a collection of
   fully-typed expressions, and the fuzzy type of a symbol is
   automatically computed as upper bound of their types

defects:

- something as simple as "eq 1 1" never gets typed correctly

- no coercion! That means something like "eq a c" means I detect a: N,
   c: Z and eq: N -> Z -> Prop, but none of the two available
   instances N -> N -> Prop and Z -> Z -> Prop fit the bill.

 *)



(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     let* tail_ = option_list_of_list_option tail in
     some (head :: tail_)
  | None :: tail -> none
  | [] -> some []

let string_of_list stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string " ";
                      helper tail
  in
  helper list;
  Buffer.contents buf

let rec chain_of_responsibility workers work =
  match workers with
  | [] -> none
  | worker :: tail ->
     match worker work with
     | Some result -> some result
     | None -> chain_of_responsibility tail work

let chain_of_contribution workers work =
  concat_map (fun worker -> worker work) workers

let rec list_product_map (f: 'a -> 'b list) (l: 'a list) =
  match l with
  | [] -> []
  | head :: [] -> map (fun a -> [a]) (f head)
  | head :: tail -> concat_map
                      (fun l -> map (fun a -> a :: l) (f head))
                      (list_product_map f tail)

let rec list_map2_partial worker l1 l2 =
  match l1, l2 with
  | [], [] -> []
  | hd1 :: tl1, hd2 :: tl2 -> (worker hd1 hd2) :: (list_map2_partial worker tl1 tl2)
  | _ -> []

let rec list_pad l1 l2 obj =
  match l1, l2 with
  | [], [] -> []
  | [], hd :: tl -> obj :: (list_pad [] tl obj)
  | hd1 :: tl1, hd2 :: tl2 -> hd1 :: (list_pad tl1 tl2 obj)
  | _ -> raise (Invalid_argument "list_pad's first argument should be a shorter list to pad")

let try_improve worker obj =
  match worker obj with
  | Some better -> better
  | None -> obj



(* UNTYPED EXPRESSIONS *)

type uexpr = | UConst of string
             | UApp of uexpr * uexpr list

let rec string_of_uexpr ue =
  match ue with
  | UConst name -> name
  | UApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_uexpr fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_uexpr arg)) args;
     Buffer.add_string buf ")";
     Buffer.contents buf



(* COQ TYPES *)
(* simplified... *)

exception BadCType of string

type ctype = | CBasic of string
             | CFunct of ctype * ctype list

let ctype_predicate ct = CFunct (CBasic "Prop", [ct])
let ctype_relation ct = CFunct (CBasic "Prop", [ct; ct])
let ctype_operator ct = CFunct (ct, [ct; ct])

let string_of_ctype ct =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper ct_ =
    match ct_ with
    | CBasic name -> add_string name
    | CFunct (result, args) ->
       add_string "(";
       iter (fun arg -> helper arg;
                        add_string " -> ") args;
       helper result;
       add_string ")"
  in
  helper ct;
  Buffer.contents buf

let ctype_comp ct1 ct2 =
  match ct1 with
  | CFunct (result, [arg] ) ->
     if arg = ct2 then result
     else raise (BadCType ((string_of_ctype ct2)
                           ^ " was given while "
                           ^ (string_of_ctype arg)
                           ^ " was expected"))
  | CFunct (result, arg :: others ) ->
     if arg = ct2 then CFunct (result, others)
     else raise (BadCType ((string_of_ctype ct2)
                           ^ " was given while "
                           ^ (string_of_ctype arg)
                           ^ " was expected"))
  | _ -> raise (BadCType ("trying to apply not-functional type "
                          ^ (string_of_ctype ct1)
                          ^ " at type "
                          ^ (string_of_ctype ct2)))

let rec ctype_comps ct1 cts =
  match cts with
  | ct :: others -> ctype_comps (ctype_comp ct1 ct) others
  | [] -> ct1

let rec ctype_check ct =
  match ct with
  | CBasic _ -> ct
  | CFunct (fn, args) -> ctype_comps (ctype_check fn) (map ctype_check args)



(* FUZZY TYPES *)

exception BadFType of string

type ftype = | FImpossible
             | FWildcard
             | FBasic of string
             | FFunct of ftype * ftype list

let rec ftype_curryfy ft =
  match ft with
  | FImpossible | FWildcard | FBasic _ ->
     ft
  | FFunct (partial, fts) ->
     begin
       match ftype_curryfy partial with
       | FImpossible | FWildcard | FBasic _ ->
          ft
       | FFunct (real, others) ->
          FFunct (real, append fts others)
     end
                       
let rec ftype_compatible ft1 ft2 =
  match ft1, ft2 with
  | _, FImpossible | FImpossible, _ -> false
  | _, FWildcard -> true
  | FWildcard, _ -> true
  | FBasic name1, FBasic name2 ->
     name1 = name2
  | FFunct (fn1, args1), FFunct (fn2, args2) ->
     ftype_compatible fn1 fn2
     && length args1 = length args2
     && for_all2 ftype_compatible args2 args1
  | _ -> false

let rec ftype_sup ft1 ft2 =
  match ft1, ft2 with
  | _, FImpossible -> ft1
  | FImpossible, _ -> ft2
  | FBasic name1, FBasic name2 ->
     if name1 = name2
     then FBasic name1
     else FWildcard
  | FFunct (fn1, args1), FFunct (fn2, args2) ->
     if length args1 = length args2
     then FFunct (ftype_sup fn1 fn2, map2 ftype_sup args1 args2)
     else FWildcard
  | _ -> FWildcard

let rec ftype_inf ft1 ft2 =
  match ft1, ft2 with
  | _, FWildcard -> ft1
  | FWildcard, _ -> ft2
  | FBasic name1, FBasic name2 ->
     if name1 = name2
     then FBasic name1
     else FImpossible
  | FFunct (fn1, args1), FFunct (fn2, args2) ->
     if length args1 = length args2
     then FFunct (ftype_inf fn1 fn2, map2 ftype_inf args1 args2)
     else FImpossible
  | _ -> FImpossible

let rec ftype_of_ctype ct =
  match ct with
  | CBasic name -> FBasic name
  | CFunct (result, args) -> FFunct (ftype_of_ctype result, map ftype_of_ctype args)

let rec ctype_of_ftype ft =
  match ft with
  | FImpossible | FWildcard -> none
  | FBasic name -> some (CBasic name)
  | FFunct (result, args) ->
     let* cresult = ctype_of_ftype result in
     let* cargs = option_list_of_list_option (map ctype_of_ftype args) in
     some (CFunct (cresult, cargs))

let string_of_ftype ft =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper ft_ =
    match ft_ with
    | FImpossible -> add_string "Type (* impossible *)"
    | FWildcard -> add_string "_"
    | FBasic name -> add_string name
    | FFunct (result, args) ->
       add_string "(";
       iter (fun arg -> helper arg;
                        add_string " -> ") args;
       helper result;
       add_string ")"
  in
  helper ft;
  Buffer.contents buf

let ftype_comp ft1 ft2 =
  match ft1, ft2 with
  | FFunct (result, [arg]), _ ->
     if ftype_compatible ft2 arg
     then result
     else raise (BadFType ((string_of_ftype ft2)
                           ^ " was given while "
                           ^ (string_of_ftype arg)
                           ^ " was expected"))
  | FFunct (result, arg :: others), _ ->
     if ftype_compatible ft2 arg
     then FFunct (result, others)
     else raise (BadFType ((string_of_ftype ft2)
                           ^ " was given while "
                           ^ (string_of_ftype arg)
                           ^ " was expected"))
  | _ -> raise (BadFType ("trying to apply not-functional type "
                          ^ (string_of_ftype ft1)
                          ^ " at type "
                          ^ (string_of_ftype ft2)))

let rec ftype_comps ft1 fts =
  match fts with
  | [] -> ft1
  | ft :: others -> ftype_comps (ftype_comp ft1 ft) others



(* COQ EXPRESSIONS *)
(* somewhat simplified of course, but that's the goal *)

exception BadCExpr of string

type cexpr = | CConst of string * ctype
             | CApp of cexpr * cexpr list

let rec cexpr_typecheck ce =
  match ce with
  | CConst (_, ct) -> ct
  | CApp (fn, args) -> ctype_comps (cexpr_typecheck fn) (map cexpr_typecheck args)

let rec string_of_cexpr ?(with_ctype = true) ce =
  match ce with
  | CConst (name, _) ->
     if with_ctype then (
       let buf = Buffer.create 0 in
       let add_string = Buffer.add_string buf in
       add_string name;
       add_string ": ";
       add_string (string_of_ctype (cexpr_typecheck ce));
       Buffer.contents buf
     ) else name
  | CApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_cexpr ~with_ctype:false fn);
     iter (fun arg -> add_string " ";
                           add_string (string_of_cexpr ~with_ctype:false arg)) args;
     add_string ")";
     if with_ctype then
       begin
         add_string ": ";
         add_string (string_of_ctype (cexpr_typecheck ce))
       end;
     Buffer.contents buf



(* FUZZY EXPRESSIONS *)

exception BadFExpr of string

type fexpr = | FConst of string * ftype
             | FApp of fexpr * ftype * fexpr list * ftype list

let rec ftype_of_fexpr fe =
  match fe with
  | FConst (_, ft) -> ft
  | FApp (fn, ft, args, fts) -> ftype_comps ft fts

let rec cexpr_of_fexpr fe =
  match fe with
  | FConst (name, ftype) ->
     let* ctype = ctype_of_ftype ftype in
     some (CConst (name, ctype))
  | FApp (fn_, _, args_, _) ->
     let* fn = cexpr_of_fexpr fn_ in
     let* args = option_list_of_list_option (map cexpr_of_fexpr args_) in
     some (CApp (fn, args))

let rec fexpr_of_cexpr ce =
  match ce with
  | CConst (name, ctype) -> FConst (name, ftype_of_ctype ctype)
  | CApp (fn, args) -> FApp (fexpr_of_cexpr fn,
                             ftype_of_ctype (cexpr_typecheck fn),
                             map fexpr_of_cexpr args,
                             map (fun arg -> ftype_of_ctype (cexpr_typecheck arg)) args)

let rec string_of_fexpr fe =
  match fe with
  | FConst (name, ftype) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string name;
     add_string ": ";
     add_string (string_of_ftype ftype);
     add_string ")";
     Buffer.contents buf
  | FApp (fn, ft, args, fts) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_fexpr fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_fexpr arg)) args;
     add_string "): ";
     add_string (string_of_ftype (ftype_comps ft fts));
     add_string " obtained as ";
     add_string (string_of_ftype ft);
     iter (fun ft_ -> add_string " "; add_string (string_of_ftype ft_)) fts;
     Buffer.contents buf



(* SYMBOL *)

type symbol = ftype * cexpr list

let symbol_new () = (FWildcard, [])

let symbol_with symbol cexpr =
  let (ft, others) = symbol in
  let ct = cexpr_typecheck cexpr in
  if others == []
  then (ftype_of_ctype ct, [cexpr])
  else (ftype_sup ft (ftype_of_ctype ct), cexpr :: others)

let string_of_symbol symbol =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf
  and (ftype, cexprs) = symbol in
  add_string (string_of_ftype ftype);
  add_string " { ";
  add_string (string_of_list string_of_cexpr cexprs);
  add_string " }";
  Buffer.contents buf



(* CONTEXT *)

module NotTrie = Map.Make(String) (* FIXME: I'd prefer a real trie *)

type context = {
    (* excarnated objects *)
    mutable symbols: symbol NotTrie.t;
    (* recognize what a constant can be *)
    mutable constant_parsers: (string -> (ftype * cexpr) option) list;
    (* variables in the Coq context *)
    mutable variables: ctype NotTrie.t;
  }

let context_empty () =
  {
    symbols = NotTrie.empty;
    constant_parsers = [];
    variables = NotTrie.empty;
  }

let context_add_symbol context name cexpr =
  context.symbols <- NotTrie.update name
                       (fun ores -> match ores with
                                    | Some res -> some (symbol_with res cexpr)
                                    | None -> some (symbol_with (symbol_new ()) cexpr))
                       context.symbols

let context_add_variable context name ctype =
  context.variables <- NotTrie.update name (fun _ -> some ctype) context.variables;
  context_add_symbol context name (CConst (name, ctype))

let context_add_constant_parser context parser =
  context.constant_parsers <- parser :: context.constant_parsers

let context_parse_string context target chaine =
  let symbol_parser name =
    match NotTrie.find_opt name context.symbols with
    | Some (ftype, cexprs) -> map (fun ce -> ftype_of_fexpr (fexpr_of_cexpr ce), ce) cexprs
    | None -> []
  in
  let raw = append (filter_map
                      (fun parser -> parser chaine)
                      context.constant_parsers)
              (symbol_parser chaine)
  in
  filter (fun (ft, _) -> ftype_compatible ft target) raw

let string_of_context context =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let string_of_variables () =
    add_string "\n(* Coq variables *)\n\n";
    NotTrie.iter (fun key value ->
        add_string "Variable ";
        add_string key;
        add_string ": ";
        add_string (string_of_ctype value);
        add_string ".\n") context.variables;
    add_string "\n\n"
  and string_of_symbols () =
    add_string "\n(* available symbols for elaboration:\n";
    NotTrie.iter (fun key symbol ->
        add_string "\t";
        add_string key;
        add_string ": ";
        add_string (string_of_symbol symbol);
        add_string "\n") context.symbols;
    add_string "*)\n"
  in
  string_of_symbols ();
  string_of_variables ();
  Buffer.contents buf

let context_elaborate context uexpr =
  let rec constants_pass ue =
    match ue with
    | UConst name ->
       begin
         match context_parse_string context FWildcard name with
         | [(ftype, cexpr)] ->
            some (fexpr_of_cexpr cexpr)
         | (ftype, cexpr) :: others ->
            some (FConst (name, fold_left (fun ft_ (ft, ce) -> ftype_sup ft_ ft) ftype others))
         | [] ->
            some (FConst (name, FWildcard)) (* FIXME: perhaps it should even be an error ? *)
       end
    | UApp (fn, args) ->
       let* ffn = constants_pass fn in
       let* fargs = option_list_of_list_option (map constants_pass args) in
       some (FApp (ffn, ftype_of_fexpr ffn, fargs, map ftype_of_fexpr fargs))
  and top_down_pass target fe =
    match fe with
    | FConst (name, ftype) ->
       begin
         match context_parse_string context (ftype_inf ftype target) name with
         | [(ft, ce)] ->
            some (fexpr_of_cexpr ce)
         | _ ->
            some (FConst (name, ftype))
       end
    | FApp (fn, ft_fn, args, ft_args) ->
       let ft_args_improved =
         begin
           match ft_fn with
           | FImpossible | FBasic _ ->
              raise (BadFType "Found a non-functional type at the left of an application")
           | FFunct (result, ft_args_fn) ->
              list_map2_partial ftype_inf ft_args_fn ft_args
           | FWildcard ->
              ft_args
         end
       in
       let args_improved = map2 (fun a ft_a ->
                               try_improve (top_down_pass ft_a) a) args ft_args_improved in
       let fn_improved = try_improve (top_down_pass ft_fn) fn in
       some (FApp (fn_improved, ft_fn, args_improved, ft_args_improved))
  and down_top_pass fe =
    match fe with
    | FConst _ -> some fe
    | FApp (fn, ft_fn, args, ft_args) -> (* fn and args have been improved *)
       let fn_type = 
         ftype_inf
           (ftype_of_fexpr fn)
           (ftype_curryfy (FFunct (ftype_comps ft_fn ft_args, ft_args)))
       and args_type = map ftype_of_fexpr args
       in
       some (FApp (fn, fn_type, args, args_type))
  in
  let (-->) = bind in
  constants_pass uexpr
  --> down_top_pass
  --> top_down_pass FWildcard
  --> down_top_pass
  --> top_down_pass FWildcard
  --> down_top_pass
  --> top_down_pass FWildcard


(* LEARN ABOUT COQ STUFF *)
(* this is needed because we're out of Coq *)

let add_coq_to_context context =
  let add name ctype =
    context_add_symbol context name (CConst (name, ctype))
  in
  begin
    let typename = CBasic "Prop" in
    add "True" typename;
    add "False" typename;
    add "and" (ctype_relation typename);
    add "or" (ctype_relation typename);
    add "not" (ctype_predicate typename)
  end;
  begin
    let typename = CBasic "N" in
    add "N.eq" (ctype_relation typename);
    add "N.le" (ctype_relation typename);
    add "N.add" (ctype_operator typename);
    add "N.mul" (ctype_operator typename)
  end;
  begin
    let typename = CBasic "Z" in
    add "Z.eq" (ctype_relation typename);
    add "Z.le" (ctype_relation typename);
    add "Z.add" (ctype_operator typename);
    add "Z.mul" (ctype_operator typename)
  end



(* N *)

let add_N_to_context context =
  let ctype = CBasic "N"
  and add = context_add_symbol context
  in
  let cst_parser value =
    if Str.string_match (Str.regexp "[0-9]+$") value 0
    then some (ftype_of_ctype ctype, CConst (value ^ "%N", ctype))
    else none
  in
  context_add_constant_parser context cst_parser;
  add "eq" (CConst ("N.eq", ctype_relation ctype));
  add "add" (CConst ("N.add", ctype_operator ctype));
  add "mul" (CConst ("N.mul", ctype_operator ctype))



(* Z *)

let add_Z_to_context context =
  let ctype = CBasic "Z"
  and add = context_add_symbol context
  in
  let cst_parser value =
    if Str.string_match (Str.regexp "-?[0-9]+$") value 0 then
      begin
        if String.starts_with ~prefix:"-" value
        then some (ftype_of_ctype ctype, CConst ("(" ^ value ^ ")%Z", ctype))
        else some (ftype_of_ctype ctype, CConst (value ^ "%Z", ctype))
      end
    else none
  in
  context_add_constant_parser context cst_parser;
  add "eq" (CConst ("Z.eq", ctype_relation ctype));
  add "add" (CConst ("Z.add", ctype_operator ctype));
  add "sub" (CConst ("Z.sub", ctype_operator ctype));
  add "mul" (CConst ("Z.mul", ctype_operator ctype))



(* TEST CODE *)

let print_opening buf =
  let add_string = Buffer.add_string buf in
  let add_line line = add_string line; add_string "\n" in
  add_line "Require Import ZArith.";
  add_line "Section Main."

let print_closing buf = Buffer.add_string buf "End Main.\n"
  
let main () =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let context = context_empty () in
  let helper ue =
    match context_elaborate context ue with
    | Some fe ->
       add_string "(* expression ";
       add_string (string_of_uexpr ue);
       add_string " elaborates to: ";
       add_string (string_of_fexpr fe);
       add_string " *)\n";
       begin
         match cexpr_of_fexpr fe with
         | Some ce ->
            add_string "Check ";
            add_string (string_of_cexpr ce);
            add_string ".\n\n"
         | None ->
            add_string "(* but it's still fuzzy *)\n\n"
       end
    | None ->
       add_string "(* expression ";
       add_string (string_of_uexpr ue);
       add_string " doesn't typecheck at all *)\n\n"
  in
  add_coq_to_context context;
  add_N_to_context context;
  add_Z_to_context context;
  context_add_variable context "a" (CBasic "N");
  context_add_variable context "b" (CBasic "N");
  context_add_variable context "c" (CBasic "Z");
  print_opening buf;
  add_string (string_of_context context);
  ignore begin
      map helper [
          UConst "a";
          UConst "N.add";
          UApp (UConst "N.add", [UConst "a"]);
          UApp (UConst "N.add", [UConst "a"; UConst "2"]);
          UApp (UConst "Z.add", [UConst "c"; UConst "2"]);
          UApp (UConst "Z.add", [UConst "c"; UConst "-2"]);
          UApp (UConst "not", [UConst "True"]);
          UApp (UConst "sub", [UConst "c"; UConst "c"]);
          UApp (UConst "eq", [UConst "a"; UConst "b"]);
          UApp (UConst "eq", [UConst "1"; UConst "-1"]);
          UApp (UConst "eq", [UConst "1"; UConst "1"]);
          UApp (UConst "eq", [UConst "a"; UConst "c"]);
        ]
    end;
  print_closing buf;
  print_string (Buffer.contents buf)

;;

main ()
