(* in this file:

- from an untyped expression (an Eve expression), get the list of
   possible interpretations as typed expressions (Coq expressions)

- the frame/context type is better: more structure, and the different
   levels can be added as-a-block to the current context

defects:

- when adding a variable, the pushers need to have been put into the
   context in the right order, because we just loop through them once,
   so that isn't as good as what bribe_20230404 did

- no proof!

 *)

(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     fold (option_list_of_list_option tail)
       ~none: none
       ~some:(fun tail_ -> some (head :: tail_))
  | None :: tail -> none
  | _ -> some []

let string_of_list stringer list =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper l =
    match l with
    | [] -> add_string ""
    | [head] -> add_string (stringer head)
    | head :: tail -> add_string (stringer head);
                      add_string " ";
                      helper tail
  in
  helper list;
  Buffer.contents buf

let rec chain_of_responsibility workers work =
  match workers with
  | worker :: tail -> fold (worker work)
                        ~none:(chain_of_responsibility tail work)
                        ~some:(fun result -> some result)
  | _ -> none

let chain_of_contribution workers work =
  concat_map (fun worker -> worker work) workers

let rec list_product_map (f: 'a -> 'b list) (l: 'a list) =
  match l with
  | [] -> []
  | head :: [] -> map (fun a -> [a]) (f head)
  | head :: tail -> concat_map
                      (fun l -> map (fun a -> a :: l) (f head))
                      (list_product_map f tail)

                  
(* COQ TYPES AND EXPRESSIONS *)
(* somewhat simplified of course, but that's the goal *)

type ctype = | CBasic of string
             | CFunctional of ctype * (ctype list)

type cexpr = | CConstant of string * ctype
             | CApp of cexpr * cexpr list

let ctype_comp ct1 ct2 =
  match ct1 with
  | CBasic _ -> none
  | CFunctional (result, []) -> none
  | CFunctional (result, [head] ) ->
     if head = ct2 then some result
     else none
  | CFunctional (result, head :: tail ) ->
     if head = ct2 then some (CFunctional (result, tail))
     else none

let rec ctype_comps ct1 cts =
  match cts with
  | [] -> some ct1
  | ct :: other ->
     let* ctnext = ctype_comp ct1 ct in
     ctype_comps ctnext other

let rec string_of_ctype ct =
  match ct with
  | CBasic name -> name
  | CFunctional (result, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     iter (fun arg -> add_string (string_of_ctype arg);
                           add_string " -> ") args;
     add_string (string_of_ctype result);
     add_string ")";
     Buffer.contents buf

let rec cexpr_typecheck ce =
  match ce with
  | CConstant (_, ct) -> some ct
  | CApp (fn, args) ->
     let* fnct = cexpr_typecheck fn in
     let* argsct = option_list_of_list_option (map cexpr_typecheck args) in
     ctype_comps fnct argsct

let rec string_of_cexpr ?(with_ctype = true) ce =
  match ce with
  | CConstant (name, _) ->
     if with_ctype then (
       let buf = Buffer.create 0 in
       let add_string = Buffer.add_string buf in
       add_string name;
       add_string ": ";
       (match cexpr_typecheck ce with
        | Some ct -> add_string (string_of_ctype ct)
        | None -> add_string "impossible!");
       Buffer.contents buf
     ) else name
  | CApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_cexpr ~with_ctype:false fn);
     iter (fun arg -> add_string " ";
                           add_string (string_of_cexpr ~with_ctype:false arg)) args;
     add_string ")";
     if with_ctype then (
       add_string ": ";
       match cexpr_typecheck ce with
       | Some ct -> add_string (string_of_ctype ct)
       | None -> add_string "impossible!"
     );
     Buffer.contents buf

(* EVE EXPRESSIONS *)
(* untyped *)

type eexpr = | EConstant of string
             | EApp of eexpr * eexpr list

let rec string_of_eexpr ee =
  match ee with
  | EConstant name -> name
  | EApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_eexpr fn);
     iter (fun arg -> add_string " ";
                           add_string (string_of_eexpr arg)) args;
     Buffer.add_string buf ")";
     Buffer.contents buf

(* CONTEXT *)

(* the name is bad, but it's more than a frame... *)

module KnownConstants = Map.Make(String)

type context = {
    mutable parsers : (string -> cexpr list) list; (* recognize constants *)
    mutable assoc : (cexpr list) KnownConstants.t; (* symbols we know how to interpret *)
    mutable pushers: (cexpr -> cexpr option) list; (* the actual frame: arrows to push things around *)
  }

let context_empty () = { parsers = []; assoc = KnownConstants.empty; pushers = [] }

(* parsing a string means either recognizing a constant or a known symbol *)
let context_parse context name =
  let assoc_parser name =
    match KnownConstants.find_opt name context.assoc with
    | Some result -> result
    | None -> []
  in
  chain_of_contribution (assoc_parser :: context.parsers) name

let context_add_parser context parser =
  context.parsers <- parser :: context.parsers

let context_add_pusher context pusher =
  context.pushers <- append context.pushers [pusher]

(* it's possible to add a new interpretation to a known symbol *)
let context_add_constant context (name, value) =
  context.assoc <- KnownConstants.update name
                     (fun ores -> match ores with
                                  | Some res -> Some (value :: res)
                                  | None -> Some [value])
                     context.assoc

(* when a variable gets added, we also want to include all its interpretations *)
let context_add_variable context (name, init_ce) =
  let helper ce_ pusher =
    match pusher ce_ with
    | Some ce ->
       context_add_constant context (name, ce);
       ce
    | None -> ce_
  in
  context_add_constant context (name, init_ce);
  ignore (fold_left helper init_ce context.pushers)

let string_of_context context =
  let string_of_map map =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    add_string "{";
    KnownConstants.iter (fun key value ->
        add_string "(";
        add_string key;
        add_string ", ";
        add_string (string_of_list string_of_cexpr value);
        add_string ") ") map;
    add_string "}";
    Buffer.contents buf
  and print_parsers parsers =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    add_string (string_of_int (length parsers));
    add_string " parsers";
    Buffer.contents buf
  in
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  add_string "{ ";
  add_string (print_parsers context.parsers);
  add_string "; assoc=";
  add_string (string_of_map context.assoc);
  add_string "}";
  Buffer.contents buf

(* INCARNATION *)

(* for a single Eve expression, try to find all possible associated
   Coq expressions given the current context *)
let rec eexpr_incarnate (context: context) ee =
  match ee with
  | EConstant name ->
     context_parse context name
  | EApp (fn, args) ->
     let fns = eexpr_incarnate context fn in
     let argss = list_product_map (eexpr_incarnate context) args in
     let possible =
       concat_map (fun fn_ -> map (fun args_ -> CApp (fn_, args_)) argss) fns
     in
     filter (fun ce -> is_some (cexpr_typecheck ce)) possible

(* N *)

let n_add_to_context context =
  let typename = "nat" in
  let to_coq_parser value =
    let nat = Str.regexp "[0-9]+" in
    if Str.string_match nat value 0
    then [CConstant (value, CBasic typename)]
    else []
  and constants =
    let ct_binop = CFunctional (CBasic typename, [CBasic typename; CBasic typename])
    and ct_pred = CFunctional (CBasic "bool", [CBasic typename])
    and ct_rel = CFunctional (CBasic "bool", [CBasic typename; CBasic typename])
    in
    let add = CConstant ("Nat.add", ct_binop)
    and mul = CConstant ("Nat.mul", ct_binop)
    and nonzero = CConstant ("Nat.nonzero?", ct_pred)
    and le = CConstant ("Nat.le", ct_rel)
    and lt = CConstant ("Nat.lt", ct_rel)
    in
    [("add", add); ("mul", mul); ("nonzero", nonzero); ("le", le); ("lt", lt)]
  in
  context_add_parser context to_coq_parser;
  iter (fun value -> context_add_constant context value) constants

(* Z *)

let z_add_to_context context =
  let typename = "Z" in
  let to_coq_parser value =
    let rel = Str.regexp "-?[0-9]+" in
    if Str.string_match rel value 0
    then [CConstant (value, CBasic typename)]
    else []
  and constants =
    let ct_binop = CFunctional (CBasic typename, [CBasic typename; CBasic typename])
    and ct_pred = CFunctional (CBasic "bool", [CBasic typename])
    and ct_rel = CFunctional (CBasic "bool", [CBasic typename; CBasic typename])
    in
    let add = CConstant ("Z.add", ct_binop)
    and mul = CConstant ("Z.mul", ct_binop)
    and nonzero = CConstant ("Z.nonzero?", ct_pred)
    and le = CConstant ("Z.le", ct_rel)
    and lt = CConstant ("Z.lt", ct_rel)
    in
    [("add", add); ("mul", mul); ("nonzero", nonzero); ("le", le); ("lt", lt)]
  and pusher ce_ =
    let ce = CApp ((CConstant ("Z.of_nat",
                               CFunctional (CBasic "Z", [CBasic "nat"]))), [ce_])
    in
    let cto = cexpr_typecheck ce in
    match cto with | Some _ -> some ce | None -> none
  in
  context_add_parser context to_coq_parser;
  context_add_pusher context pusher;
  iter (fun value -> context_add_constant context value) constants

(* C *)

let c_add_to_context context =
  let typename = "C" in
  let to_coq_parser value =
    let rel = Str.regexp "-?[0-9]+" in
    if Str.string_match rel value 0
    then [CConstant (value, CBasic typename)]
    else []
  and constants =
    let ct_binop = CFunctional (CBasic typename, [CBasic typename; CBasic typename])
    and ct_pred = CFunctional (CBasic "bool", [CBasic typename])
    in
    let add = CConstant ("C.add", ct_binop)
    and mul = CConstant ("C.mul", ct_binop)
    and nonzero = CConstant ("C.nonzero?", ct_pred)
    in
    [("add", add); ("mul", mul); ("nonzero", nonzero)]
  in
  context_add_parser context to_coq_parser;
  iter (fun value -> context_add_constant context value) constants

(* TEST CODE *)

let test_ctype_comp () =
  let cbn = CBasic "nat" in
  let helper (ct1, ct2) =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    add_string "\t(";
    add_string (string_of_ctype ct1);
    add_string ") o (";
    add_string (string_of_ctype ct2);
    add_string "): ";
    (match ctype_comp ct1 ct2 with
     | Some ct ->
        add_string (string_of_ctype ct);
        add_string "\n"
     | None ->
        add_string "impossible\n");
    print_string (Buffer.contents buf)
  in
  print_string "Testing simple Coq type composition:\n";
  iter helper
    [
      (cbn, cbn);
      (CFunctional (cbn, [cbn]), cbn);
      (CFunctional (cbn, [CBasic "real"]), cbn);
      (CFunctional (CBasic "real", [cbn]), cbn);
      (CFunctional (cbn, [cbn; cbn]), cbn);
      (CFunctional (cbn, [cbn; cbn; CFunctional (cbn, [cbn])]), cbn);
     ]

let test_ctype_comps () =
  let cbn = CBasic "nat" in
  let helper (ct1, cts) =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    add_string "\t(";
    add_string (string_of_ctype ct1);
    add_string ") o (";
    add_string (string_of_list string_of_ctype cts);
    add_string "): ";
    (match ctype_comps ct1 cts with
     | Some ct ->
        add_string (string_of_ctype ct);
        add_string "\n"
     | None ->
        add_string "impossible\n");
    print_string (Buffer.contents buf)
  in
  print_string "Testing multiple Coq type composition:\n";
  iter helper
    [
      (cbn, [cbn]);
      (CFunctional (cbn, [cbn]), [cbn]);
      (CFunctional (cbn, [CBasic "real"]), [cbn]);
      (CFunctional (CBasic "real", [cbn]), [cbn]);
      (CFunctional (cbn, [cbn; cbn]), [cbn; cbn]);
      (CFunctional (cbn, [cbn; cbn; CFunctional (cbn, [cbn])]), [cbn]);
      (CFunctional (cbn, [cbn; cbn; CFunctional (cbn, [cbn])]), [cbn; cbn; CFunctional (cbn, [cbn])]);
     ]

let test_cexpr_typecheck () =
  let cbn = CBasic "nat" in
  let ct_add = CFunctional (cbn, [cbn; cbn]) in
  let helper ce =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    add_string "\t";
    add_string (string_of_cexpr ce);
    add_string "\n";
    print_string (Buffer.contents buf)
  in
  print_string "Testing Coq expression typechecking:\n";
  iter helper
    [
      CApp (CConstant ("Nat.add", ct_add), [CConstant ("2", cbn)]);
      CApp (CConstant ("Nat.add", ct_add), [CConstant ("2", cbn);
                                            CConstant ("3", cbn)]);
      CApp (CApp (CConstant ("Nat.add", ct_add), [CConstant ("2", cbn)]), [CConstant ("3", cbn)]);
    ]

let test_eexpr_incarnation () =
  let context = context_empty () in
  n_add_to_context context;
  z_add_to_context context;
  c_add_to_context context;
  context_add_variable context ("a", CConstant ("a", CBasic "nat"));
  let helper ee =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    let cexpr_printer ce =
      add_string "\t\t";
      add_string (string_of_cexpr ce);
      add_string "\n"
    in
    add_string "\t";
    add_string (string_of_eexpr ee);
    add_string " can mean:\n";
    (match eexpr_incarnate context ee with
     | [] -> add_string "\t\tnothing!\n"
     | l -> iter cexpr_printer l);
    add_string "\n";
    print_string (Buffer.contents buf)
  in
  print_string "Testing Eve expression incarnation:\n";
  iter helper
    [
      EConstant "123";
      EConstant "-12";
      EApp (EConstant "add", [EConstant "123"; EConstant "45"]);
      EApp (EConstant "add", [EConstant "3"; EConstant "-2"]);
      EApp (EConstant "add", [EConstant "3"]);
      EApp (EConstant "add", [EConstant "3"; EConstant "-2"; EConstant "2"]);
      EApp (EConstant "add", [EConstant "1"; EApp (EConstant "add", [EConstant "2"; EConstant "3"])]);
      EApp (EConstant "add", [EConstant "-1"; EApp (EConstant "add", [EConstant "2"; EConstant "3"])]);
      EApp (EConstant "le", [EConstant "a"; EConstant "2"]);
    ]

let main () =
  let tests =
    [
      test_ctype_comp;
      test_ctype_comps;
      test_cexpr_typecheck;
      test_eexpr_incarnation;
    ]
  in
  iter (fun test -> test (); print_string "\n") tests
;;

main ()
