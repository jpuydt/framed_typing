open Elaboration.Annot
module T = Elaboration.Pseudocoq
module F = Elaboration.Frame.Make(T)
open T
open F

let n = ABasic "N"
let z = ABasic "Z"
let op tt = AFunct (tt, AFunct (tt, tt))
let nn = op n
let zz = op z

let tests =
  [
    (([n], []),([], []),([n], []));
    (([n], [n]),([], []),([n], [n]));
    (([n], [n; z]),([], []),([n], [n; z]));
    (([n; z], [n; z]), ([z], [n; z]), ([n; z], [n; z]));
    (([n; z], [z]), ([], []), ([n; z], [z]));
    (([n; z], [n]), ([], []), ([n], [n]));
    (([nn; zz], []), ([], [AFunct (n, ABruijn 0)]), ([nn], [AFunct (n, ABruijn 0)]));
  ]

let converter frame arg =
  let (fa1g, fa1d), (fa2g, fa2d), (faeg, faed) = arg in
  let ttype_list_of_atype_list arg = List.map (ttype_of_atype frame.context) arg in
  let ft1 =
    ttype_list_of_atype_list fa1g, ttype_list_of_atype_list fa1d
  and ft2 =
    ttype_list_of_atype_list fa2g, ttype_list_of_atype_list fa2d
  and expected =
    ttype_list_of_atype_list faeg, ttype_list_of_atype_list faed
  in
  ft1, ft2, expected
let _ =
  let error = ref false in
  let frame = empty () in
  add_coercion frame "Z_of_N";
  List.iter (fun (ft1, ft2, expected) ->
      let res = ftype_tighten frame ft1 ft2 in
      if not (res = expected) then begin
          print_endline ("BAD " ^ (string_of_ftype frame ft1)
                         ^ " and " ^ (string_of_ftype frame ft2)
                         ^ " give " ^ (string_of_ftype frame res)
                         ^ " instead of " ^ (string_of_ftype frame expected));
          error := true
        end
    ) (List.map (converter frame) tests);
  if !error then (exit 1);
  exit 0
