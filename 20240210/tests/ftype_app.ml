open Elaboration.Annot
module T = Elaboration.Pseudocoq
module F = Elaboration.Frame.Make(T)

open T
open F

let a = ABasic "a"
let b = ABasic "b"
let c = ABasic "c"
let n = ABasic "N"
let z = ABasic "Z"
let op tt = AFunct (tt, AFunct (tt, tt))

let tests =
  [
    (([AFunct (a, c); AFunct(b,c)], []), ([a],[]), ([], []),
     (([AFunct (a, c)],[AFunct (a, c)]), ([a], [a]), ([c], [])));
    (([AFunct (a, b); AFunct (a, c)], []), ([a], []), ([], [c]),
     (([AFunct (a, c)],[AFunct (a, c)]), ([a], [a]), ([c], [c])));
    (([op n; op z], []), ([n; z], []), ([], []),
     (([op n; op z],[op n; AFunct (n, AFunct (z, z)); AFunct (z, AFunct (n, n)); op z]), ([n; z], [n; z]), ([AFunct (n, n); AFunct (z, z)], [])));
    (([op n; op z], []), ([n; z], []), ([], [AFunct (z, z)]),
     (([op z],[AFunct (n, AFunct (z, z)); AFunct (z, AFunct (z, z))]), ([n; z], [z]), ([AFunct (z, z)], [AFunct (z, z)])));
  ]

let converter frame arg =
  let (fa1g, fa1d), (fa2g, fa2d), (fa3g, fa3d), ((fae1g, fae1d), (fae2g, fae2d), (fae3g, fae3d))
    = arg
  in
  let ttype_list_of_atype_list arg = List.map (ttype_of_atype frame.context) arg in
  (ttype_list_of_atype_list fa1g, ttype_list_of_atype_list fa1d),
  (ttype_list_of_atype_list fa2g, ttype_list_of_atype_list fa2d),
  (ttype_list_of_atype_list fa3g, ttype_list_of_atype_list fa3d),
  ((ttype_list_of_atype_list fae1g, ttype_list_of_atype_list fae1d),
   (ttype_list_of_atype_list fae2g, ttype_list_of_atype_list fae2d),
   (ttype_list_of_atype_list fae3g, ttype_list_of_atype_list fae3d))
let _ =
  let error = ref false in
  let frame = empty () in
  add_coercion frame "Z_of_N";
  List.iter (fun (fnct, arg, res, (fnct_expected, arg_expected, res_expected)) ->
      let fnct_result, arg_result, res_result = ftype_app frame fnct arg res in
      if not (fnct_result = fnct_expected && arg_result = arg_expected && res_result = res_expected)
      then begin
          print_endline ("BAD:\n"
                         ^ "\t" ^ (string_of_ftype frame fnct) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame arg) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame res) ^ "\n"
                         ^ " gives:\n"
                         ^ "\t" ^ (string_of_ftype frame fnct_result) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame arg_result) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame res_result) ^ "\n"
                         ^ " instead of:\n"
                         ^ "\t" ^ (string_of_ftype frame fnct_expected) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame arg_expected) ^ "\n"
                         ^ "\t" ^ (string_of_ftype frame res_expected) ^ "\n");
          error := true
        end
    ) (List.map (converter frame) tests);
  if !error then (exit 1);
  exit 0
