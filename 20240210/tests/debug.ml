open Elaboration.Untyped

module T = Elaboration.Pseudocoq
open T
module E = Elaboration.Elaborate.Make(T)
open E
open E.Frame

let uexpr_of_string str =
  let lexbuf = Sedlexing.Utf8.from_string str in
  let lexer = Sedlexing.with_tokenizer Elaboration.Untyped_lexer.token lexbuf in
  let parser = MenhirLib.Convert.Simplified.traditional2revised Elaboration.Untyped_parser.main in
  parser lexer

let add_symb (frame: E.Frame.t) name level var =
  match T.read_variable frame.context var with
  | Some expr ->
     add_symbol frame name level expr
  | None ->
     raise (Invalid_argument ("add_s: '" ^ var ^ "' seems unknown"))

let add_full (frame: E.Frame.t) symb level name ttype =
  add_variable frame name ttype;
  add_symb frame symb level name

let add_NZ_to (frame: E.Frame.t) =
  let bool = CBasic "bool"
  and prop = CBasic "Prop"
  and nn = CBasic "N"
  and zz = CBasic "Z"
  and a_list = CParam ("list", [CBruijn 0])
  in
  add_coercion frame "Z_of_N";
  add_full frame "add" nn "N.add" (ttype_operator nn);
  add_full frame "mul" nn "N.mul" (ttype_operator nn);
  add_full frame "add" zz "Z.add" (ttype_operator zz);
  add_full frame "mul" zz "Z.mul" (ttype_operator zz)

let _ =
  let frame = Frame.empty () in
  add_NZ_to frame;
  add_variable frame "a" (CBasic "N");
  add_variable frame "b" (CBasic "N");
  add_variable frame "c" (CBasic "Z");
  let uexpr = uexpr_of_string "let t = a in add @ t @ c" in
  print_endline ("uexpr: " ^ (string_of_uexpr uexpr));
  let fexpr = fexpr_of_uexpr frame uexpr in
  print_endline ("fexpr: " ^ (string_of_fexpr frame fexpr));
  let fexpr = bottom_top_pass frame fexpr in
  print_endline ("bottom_top: " ^ (string_of_fexpr frame fexpr));
  let fexpr = top_bottom_pass frame ftype_any fexpr in
  print_endline ("top_bottom: " ^ (string_of_fexpr frame fexpr));
  let fexpr = bottom_top_pass frame fexpr in
  print_endline ("bottom_top: " ^ (string_of_fexpr frame fexpr));
  let fexpr = choice frame fexpr in
  print_endline ("choice: " ^ (string_of_fexpr frame fexpr));
  let fexpr = bottom_top_pass frame fexpr in
  print_endline ("bottom_top: " ^ (string_of_fexpr frame fexpr));
  ()
