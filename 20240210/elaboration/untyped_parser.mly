%token EOF
%token LBRACK RBRACK COLON
%token ARROW MAPSTO LET IN LAMBDA EQUAL
%token<string> STRING

%{
    open Stdlib.Option

    open Annot
    open Untyped

    let uexpr_impose ue_ atype =
      match ue_ with
      | UString (str, _) -> UString (str, some atype)
      | UApp (fnct, arg, _) -> UApp (fnct, arg, some atype)
      | ULet (name, def, body) -> ULet (name, def, body)
      | ULambda (name, oannot, body, _) -> ULambda (name, oannot, body, some atype)

    let rec uexpr_of_list uexprs =
      match uexprs with
      | [] -> raise (Invalid_argument "Shouldn't happen!")
      | uexpr :: [] -> uexpr
      | fnct :: arg :: [] -> UApp (fnct, arg, None)
      | fnct :: arg :: rest -> uexpr_of_list ((UApp (fnct, arg, None)) :: rest)
%}

%type <uexpr>main
%start main

%%

main: uexpr EOF { $1 }

uexpr:
  | uexpr_basic { $1 }
  | uexpr_list { uexpr_of_list $1 }

uexpr_list:
  | uexpr_basic uexpr_basic { [$1; $2] }
  | uexpr_basic uexpr_list { $1 :: $2 }

uexpr_basic:
  | LBRACK uexpr RBRACK { $2 }
  | LET STRING EQUAL uexpr IN uexpr { ULet ($2, $4, $6) }
  | LAMBDA STRING MAPSTO uexpr { ULambda ($2, None, $4, None) }
  | LAMBDA STRING COLON atype MAPSTO uexpr { ULambda ($2, Some $4, $6, None) }
  | uexpr COLON atype { uexpr_impose $1 $3 }
  | STRING { UString ($1, None) }

atype:
  | atype_simple { $1 }
  | atype_param { $1 }

atype_simple:
  | STRING { ABasic $1 }
  | LBRACK atype RBRACK { $2 }
  | atype ARROW atype {  AFunct ($1, $3) }

atype_param:
  | STRING atype+ { AParam ($1, $2) }
