open Untyped
open Tools

open Stdlib.Option

module Make (T: Target.target) = struct

  module Frame = Frame.Make(T)
  open Frame

  let coercion (frame: Frame.t) expr src dst =
    let helper_src expr name =
      let fnct = get (T.read_variable frame.context name) in
      T.app frame.context fnct expr
    and helper_dst expr name =
      let fnct = get (T.read_variable frame.context name) in
      T.comp frame.context fnct expr
    and src_path, dst_path = coercion_paths frame src dst in
    List.fold_left helper_dst (List.fold_left helper_src expr src_path) dst_path

  let rec impose (frame: Frame.t) fexpr used =
    (* beware of that 'rec': we must not really go down here! *)
    let msg branch =
      "[Elaborate.impose] "
      ^ branch
      ^ ": "
      ^ (string_of_fexpr frame fexpr)
      ^ " is used as "
      ^ (string_of_ftype frame used)
      ^ " which is incoherent"
    in
    match fexpr with
    | FTarget (texpr, known) ->
       let tight = ftype_tighten frame known used in
       if ftype_empty tight then raise (Invalid_argument (msg "FTarget"));
       if ftype_ambiguities tight = 0
       then
         let texpr_real = T.ttype_of_expr frame.context texpr
         and texpr_expected = ftype_choice frame tight
         in
           FTarget (coercion frame texpr texpr_real texpr_expected, tight)
       else FTarget (texpr, tight)
    | FSymb (symbol, known) ->
       let tight = ftype_tighten frame known used in
       if ftype_empty tight then raise (Invalid_argument (msg "FSymb"));
       let symb = symbol_tighten frame symbol tight in
       begin
         match symbol_uniq symb with
         | Some texpr -> FTarget (texpr, tight)
         | None -> FSymb (symb, tight)
       end
    | FApp (fnct, arg, known) ->
       let tight = ftype_tighten frame known used in
       if ftype_empty tight then raise (Invalid_argument (msg "FApp"));
       FApp (fnct, arg, tight)
    | FVar (name, known) ->
       let tight = ftype_tighten frame known used in
       update_local_variable frame name tight;
       FVar (name, tight)
    | FLet (name, def, body) ->
       do_with_local_variable frame name (ftype_of_fexpr def) (fun () ->
           FLet (name, def, impose frame body used) (* The only place we get down is here *)
         )
    | FLambda (name, name_ftype, body, ftype) ->
       let tight = ftype_tighten frame ftype used in
       if ftype_empty tight then raise (Invalid_argument (msg "FLambda"));
       FLambda (name, name_ftype, body, tight)

  let fexpr_of_uexpr (frame: Frame.t) uexpr =
    let ftype_of_annot =
      fold
        ~none:ftype_any
        ~some:(fun at -> ftype_of_used [T.ttype_of_atype frame.context at])
    in
    let rec helper = function
      | UString (name, oannot) ->
         (match interpret_name frame name with
          | Some res ->
             impose frame res (ftype_of_annot oannot)
          | None ->
             raise (Invalid_argument ("[Elaborate.fexpr_of_uexpr] constant '"
                                      ^ name
                                      ^ "' isn't known")))
      | UApp (fnct, arg, oannot) ->
         FApp (helper fnct, helper arg, ftype_of_annot oannot)
      | ULet (name, def, body) ->
         let def_fexpr = impose frame (helper def) ftype_any in
         let def_ftype = ftype_of_fexpr def_fexpr in
         do_with_local_variable frame name def_ftype (fun () ->
             let body_fexpr = helper body in
             FLet (name, def_fexpr, body_fexpr)
           )
      | ULambda (name, name_annot, body, annot) ->
         let name_ft =
           match name_annot with
           | Some atype -> ftype_of_known [T.ttype_of_atype frame.context atype]
           | None -> ftype_of_known []
         in
         do_with_local_variable frame name name_ft (fun () ->
             FLambda (name, name_ft, helper body, ftype_of_annot annot)
           )
    in
    helper uexpr

  let prune_app (frame: Frame.t) fnct arg res_ftype =
    match fnct, arg with
    | FTarget (fnct_expr, fnct_ft), FTarget (arg_expr, arg_ft) ->
       begin
         if ftype_ambiguities fnct_ft = 0 && ftype_ambiguities arg_ft = 0 then
           let fnct_tt = ftype_choice frame fnct_ft
           and arg_tt = ftype_choice frame arg_ft
           in
           match T.ttype_funct_src_dst frame.context fnct_tt with
           | Some (arg_expected, _) ->
              let arg_coerced = coercion frame arg_expr arg_tt arg_expected in
              let expr = T.app frame.context fnct_expr arg_coerced in
              FTarget (expr, ftype_of_known [T.ttype_of_expr frame.context expr])
           | None ->
              raise (Invalid_argument "[Elaborate.prune_app] shouldn't happen!")
         else
           FApp (fnct, arg, res_ftype)
       end
    | _ ->
       FApp (fnct, arg, res_ftype)

  let rec bottom_top_pass (frame: Frame.t) fexpr =
    match fexpr with
    | FTarget _ | FSymb _ ->
       fexpr
    | FApp (fnct, arg, used) ->
       let fnct_pre = bottom_top_pass frame fnct
       and arg_pre = bottom_top_pass frame arg
       in
       let fnct_pre_ftype = ftype_of_fexpr fnct_pre
       and arg_pre_ftype = ftype_of_fexpr arg_pre
       in
       let fnct_ftype, arg_ftype, res_ftype =
         ftype_app frame fnct_pre_ftype arg_pre_ftype used in
       if (ftype_empty fnct_ftype) then
         raise (Invalid_argument ("[Elaborate.bottom_top_pass] impossible application of function "
                                  ^ (string_of_fexpr frame fnct_pre)
                                  ^ " to the argument "
                                  ^ (string_of_fexpr frame arg_pre)));
       let fnct_post = impose frame fnct_pre fnct_ftype
       and arg_post = impose frame arg_pre arg_ftype
       in
       prune_app frame fnct_post arg_post res_ftype
    | FVar (name, known) ->
       let stored = get (read_local_variable frame name) in
       if ftype_ambiguities stored = 0
       then FTarget (T.var frame.context name (ftype_choice frame stored), stored)
       else FVar (name, known)
    | FLet (name, def, body) ->
       let def_post = bottom_top_pass frame def in
       let def_ftype = ftype_of_fexpr def_post in
       do_with_local_variable frame name def_ftype (fun () ->
           let body_post = bottom_top_pass frame body in
           let tight =
             ftype_tighten frame def_ftype (Option.get (read_local_variable frame name))
           in
           let def_tight = impose frame def_post tight in
           begin
             match def_tight, body_post with
             | FTarget (def_expr, _), FTarget (body_expr, body_ft) ->
                FTarget (T.letin frame.context name def_expr body_expr, body_ft)
             | _ ->
                FLet (name, def_tight, body_post)
           end)
    | FLambda (name, name_ft, body, ftype) ->
       do_with_local_variable frame name name_ft (fun () ->
           let body_post = bottom_top_pass frame body in
           let name_ft_post = Option.get (read_local_variable frame name) in
           let ftype_post = ftype_funct frame name_ft_post (ftype_of_fexpr body_post) in
           let tight = ftype_tighten frame ftype ftype_post in
           begin
             match body_post with
             | FTarget (body_expr, body_ftype) ->
                let ttype = ftype_choice frame name_ft in
                FTarget (T.lambda frame.context name ttype body_expr, body_ftype)
             | _ ->
               FLambda (name, name_ft_post, body_post, tight)
           end)

  let rec top_bottom_pass frame (used: ftype) fexpr =
    match fexpr with
    | FTarget _ | FSymb _ ->
       impose frame fexpr used
    | FApp (fnct, arg, res_ft) ->
       let res_used = ftype_tighten frame res_ft used in
       let fnct_tight, arg_tight, res_tight =
         ftype_app frame (ftype_of_fexpr fnct) (ftype_of_fexpr arg) res_used
       in
       let fnct_refined = top_bottom_pass frame fnct_tight fnct in
       let arg_refined = top_bottom_pass frame arg_tight arg in
       prune_app frame fnct_refined arg_refined res_tight
    | FVar (name, known) ->
       begin
         match read_local_variable frame name with
         | Some stored ->
            let tight = ftype_tighten frame stored known in
            update_local_variable frame name tight;
            FVar (name, tight)
         | None ->
            raise (Invalid_argument ("[Elaborate.top_bottom_pass] unknown variable" ^ name))
       end
    | FLet (name, def, body) ->
       begin
         do_with_local_variable frame name (ftype_of_fexpr def) (fun () ->
             let body_post = top_bottom_pass frame (ftype_of_fexpr body) body in
             let def_post = Option.get (read_local_variable frame name) in
             FLet (name, impose frame def def_post, body_post)
           )
       end
    | FLambda (name, name_ft, body, ftype) ->
       let tight = ftype_tighten frame ftype used in
       let targets =
         ftype_of_used
           (List.filter_map
              (fun tt ->
                match T.ttype_funct_src_dst frame.context tt with
                | Some (_, dst) -> some dst
                | _ -> none)
              (fst tight)) (* FIXME: using knowledge of ftype ! *)
       in
       do_with_local_variable frame name name_ft (fun () ->
           let body_post = top_bottom_pass frame targets body in
           let name_ft_post = Option.get (read_local_variable frame name) in
           FLambda (name, name_ft_post, body_post, tight)
         )

  let choice (frame: Frame.t) fexpr =
    let rec helper fexpr =
      match fexpr with
      | FTarget (expr, expr_ftype) ->
         if ftype_ambiguities expr_ftype = 0
         then fexpr, false
         else FTarget (expr, ftype_of_known [ftype_choice frame expr_ftype]), true
      | FSymb (symbol, _) ->
         let expr = symbol_first_instance frame symbol in
         FTarget (expr, ftype_of_known [T.ttype_of_expr frame.context expr]), true
      | FApp (fnct, arg, res_ftype) ->
         begin
           match helper fnct with
           | fnct_post, true ->
              FApp (fnct_post, arg, res_ftype), true
           | _ ->
              let arg_post, found = helper arg in
              FApp (fnct, arg_post, res_ftype), found
         end
      | FVar _ ->
         fexpr, false
      | FLet (name, def, body) ->
         begin
           match helper def with
           | def_post, true ->
              FLet (name, def_post, body), true
           | _ ->
              do_with_local_variable frame name (ftype_of_fexpr def) (fun () ->
                  match helper body with
                  | body_post, true ->
                     FLet (name, def, body_post), true
                  | _ ->
                     FLet (name, def, body), false
                )
         end
      | FLambda (name, name_ft, body, ftype) ->
         if 0 = ftype_ambiguities name_ft
         then
           match helper body with
           | body_post, true ->
              FLambda (name, name_ft, body_post, ftype), true
           | _ ->
              FLambda (name, name_ft, body, ftype), false
         else
           let name_tt = ftype_choice frame name_ft in
           FLambda (name, ftype_of_known [name_tt], body, ftype), true
    in
    match helper fexpr with
    | res, true ->
       res
    | _ ->
       raise (Invalid_argument ("[Elaborate.choice] no choice was found for "
                                ^ (string_of_fexpr frame fexpr)))

  let elaborate (frame: Frame.t) uexpr =
    let print_fexpr name fexpr =
      let ambiguities = fexpr_ambiguities fexpr
      and fuzzyness = fexpr_fuzzy_height fexpr
      in
      T.log frame.context
        ("\t(* "
         ^ name
         ^ " (ambiguities: "
         ^ (string_of_int ambiguities)
         ^ ", fuzzy height: "
         ^ (string_of_int fuzzyness)
         ^ ") : "
         ^ (string_of_fexpr frame fexpr)
         ^ " *)\n")
    in
    let helper_maker name action (fexpr: fexpr) =
      let res = action fexpr in
      let ambiguities = fexpr_ambiguities res
      and fuzzyness = fexpr_fuzzy_height res
      in
      print_fexpr name res;
      res, (ambiguities + fuzzyness) = 0
    in
    let rec helpers_chainer helpers fexpr =
      match helpers with
        [] -> fexpr, false
      | helper :: others ->
         let res, stop = helper fexpr in
         if stop
         then res, true
         else helpers_chainer others res
  in
  let helper_bt = helper_maker "bottom-top" (bottom_top_pass frame)
  and helper_tb = helper_maker "top-bottom" (top_bottom_pass frame ftype_any)
  and helper_choice = helper_maker "choice" (choice frame)
  in
  let helper_3_bttb_1c =
    helpers_chainer (List.append (repeat 3 [helper_bt; helper_tb]) [helper_choice])
  in
  let rec insist fexpr =
    let res, stop = helper_3_bttb_1c fexpr in
    if stop
    then bottom_top_pass frame res
    else insist res
  in
  let fexpr =
    T.log frame.context ("\n(* considering untyped expression: "
                         ^ (string_of_uexpr uexpr)
                         ^ " *)\n");
    fexpr_of_uexpr frame uexpr
  in
    print_fexpr "first step" fexpr;
    let fexpr_refined = insist fexpr in
    match fexpr_refined with
    | FTarget (expr, _) ->
       expr
    | _ ->
       raise (Invalid_argument "elaboration didn't give a Coq expression!")

end
