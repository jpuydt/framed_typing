open Stdlib.Option
open Stdlib.List

open Tools

module Make (T: Target.target) = struct

  open T

  (* TYPE DEFINITIONS *)

  type ftype = ttype list * ttype list (* fuzzy type: known, used *)

  type symbol = string * (ttype * expr) list (* symbol: name and (level, instance) list *)

  type local_variables =
    {
      mutable store: ftype NotTrie.t;
    }

  (* fuzzy expressions *)
  type fexpr = | FTarget of expr * ftype (* embedded target and known use *)
               | FSymb of symbol * ftype (* symbol and known use *)
               | FApp of fexpr * fexpr * ftype (* function, argument and result use *)
               | FVar of string * ftype (* name and type *)
               | FLet of string * fexpr * fexpr (* name, definition and body *)
               | FLambda of string * ftype * fexpr * ftype (* name known use and body and known use *)

  type t =
    {
      context: T.context;
      (* source, destination, coercion path *)
      mutable le: (ttype * ttype * (string list)) list;
      mutable symbols: symbol NotTrie.t;
      mutable constant_parsers: (string -> T.expr list) list;
      mutable variables: local_variables list;
    }

  let empty () = {
      context = empty_context;
      le = [];
      symbols = NotTrie.empty;
      constant_parsers = [];
      variables = [];
    }



  (* TYPE COMPARISON AND COERCION FEATURES *)

  let add_coercion (frame: t) (caster: string) =
    let real_add_coercion source destination =
      let helper (src, dst, path) =
        if src = destination then
          [(src, dst, path); (source, dst, caster :: path)]
        else if dst = source then
          [(src, dst, path); (src, destination, path @ [caster])]
        else
          [(src, dst, path)]
      in
      frame.le <- (source, destination, [caster]) :: (concat_map helper frame.le)
    in
    match T.read_variable frame.context caster with
    | None -> raise (Invalid_argument ("add_coercion: '" ^ caster ^ "' isn't a known function"))
    | Some fnct ->
       let fnct_type = T.ttype_of_expr frame.context fnct in
       match T.ttype_funct_src_dst frame.context fnct_type with
       | Some (src, dst) ->
          real_add_coercion src dst
       | _ ->
          raise (Invalid_argument ("add_coercion: '" ^ caster ^ "' doesn't seem to be a function"))

  let coercion_paths (frame: t) (src: ttype) (dst: ttype) =
    let path_finder src dst =
      if is_some (unify frame.context src dst)
      then []
      else
        let (_, _, res) = find (fun (src_, dst_, _) -> src = src_ && dst = dst_) frame.le in
        res
    in
    try
      path_finder src dst, []
    with Not_found ->
          match ttype_funct_src_dst frame.context src, ttype_funct_src_dst frame.context dst with
          | Some (src_arg, src_res), Some (dst_arg, dst_res) ->
             begin
               let arg_path =
                 try
                   path_finder dst_arg src_arg
                 with Not_found ->
                   raise (Invalid_argument ("Failed to find coercion from " ^ (string_of_ttype frame.context dst_arg)
                                            ^ " to " ^ (string_of_ttype frame.context src_arg)
                                            ^ " while trying to coerce from " ^ (string_of_ttype frame.context src)
                                            ^ " to " ^ (string_of_ttype frame.context dst)))
             and res_path =
               try
                 path_finder src_res dst_res
               with Not_found ->
                 raise (Invalid_argument ("Failed to find coercion from " ^ (string_of_ttype frame.context src_res)
                                          ^ " to " ^ (string_of_ttype frame.context dst_res)
                                          ^ " while trying to coerce from " ^ (string_of_ttype frame.context src)
                                          ^ " to " ^ (string_of_ttype frame.context dst)))
               in
               arg_path, res_path
             end
          | _ ->
             raise (Invalid_argument ("Failed to find coercion from " ^ (string_of_ttype frame.context src)
                                      ^ " to " ^ (string_of_ttype frame.context dst)))

  let rec le (frame: t) (t1: ttype) (t2: ttype) =
    if t1 = t2 then true
    else if exists (fun (src, dst, _) -> src = t1 && dst = t2) frame.le then true
    else (* here is where the functoriality is handled *)
      match ttype_funct_src_dst frame.context t1, ttype_funct_src_dst frame.context t2 with
      | Some (src1, dst1), Some (src2, dst2) ->
         (le frame src2 src1) && (le frame dst1 dst2)
      | _ ->
       false



  (* FUZZY TYPES *)

  let ftype_ambiguities (ft: ftype) = (length (fst ft)) - 1

  let ftype_any: ftype = [], []

  let ftype_empty (ft: ftype) = [] = fst ft

  let ftype_of_known (ttypes: ttype list): ftype = ttypes, []

  let ftype_of_used (ttypes: ttype list): ftype = [], ttypes

  let string_of_ftype (frame: t) (ft: ftype) =
    let known, used = ft in
    let knowns =
      "known as [ " ^ (string_of_list ~separator:", " (string_of_ttype frame.context) known) ^ " ]"
    and useds =
      "used as [ " ^ (string_of_list ~separator:", " (string_of_ttype frame.context) used) ^ " ]"
    in
    match length known, length used with
    | 0, 0 -> "{ any }"
    | _, 0 -> "{ " ^ knowns ^ " }"
    | 0, _ -> "{ " ^ useds ^ " }"
    | _, _ -> "{ " ^ knowns ^ " " ^ useds ^ " }"

  let ftype_funct (frame: t) (arg_ft: ftype) (body_ft: ftype) =
    let _, arg_used = arg_ft
    and body_known, body_used = body_ft
    and helper arg body = some (ttype_funct frame.context arg body) in
    filter_crossmap2 helper arg_used body_known, filter_crossmap2 helper arg_used body_used

  let ftype_choice (frame: t) (ft: ftype): ttype =
    let known, _ = ft
    and helper tt1 tt2 =
      if le frame tt1 tt2 then -1
      else if le frame tt2 tt1 then 1
      else 0
    in
    if known = [] then raise (Invalid_argument "[ftype_choice] from an empty type!");
    let sorted = stable_sort helper known in
    nth sorted 0

  let frame_compat (frame: t) (t1: ttype) (t2: ttype) =
    if le frame t1 t2 then some t1
    else unify frame.context t1 t2

  let ftype_above_ttype (frame: t) (tt: ttype): ftype =
    tt :: (filter_map (fun (src, dst, _) -> if tt = src then some dst else none) frame.le), []

  let ftype_used_and (frame: t) (c1: ttype list) (c2: ttype list): ttype list =
    if [] = c1 then c2
    else if [] = c2 then c1
    else list_uniq (filter_crossmap2 (frame_compat frame) c1 c2)

  let ftype_tighten (frame: t) (ft1: ftype) (ft2: ftype): ftype =
    let known1, used1 = ft1
    and known2, used2 = ft2
    in
    let known = list_uniq (known1 @ known2)
    and used = ftype_used_and frame used1 used2
    in
    if [] = used
    then known, []
    else list_uniq (filter_crossmap2 (frame_compat frame) known used), used

  let ftype_app (frame: t) (fnct: ftype) (arg: ftype) (res: ftype): ftype * ftype * ftype =
    let (fnct_known, _), (arg_known, _), (_, res_used) = fnct, arg, res in
    let frame_compat_used tt used =
      if [] = used then true
      else exists (fun cond -> is_some (frame_compat frame tt cond)) used
    in
    let helper fnct_ttype arg_ttype =
      (* Why is it so complex? Because here we use the target system's unification,
         and it is clueless about the frame... *)
      let helper_helper fnct_ttype arg_ttype =
        match ttype_funct_src_dst frame.context fnct_ttype with
        | Some (src_ttype, dst_ttype) ->
           if is_some (frame_compat frame arg_ttype src_ttype) && frame_compat_used dst_ttype res_used
           then some (fnct_ttype, arg_ttype, dst_ttype)
           else none
        | None -> none
      in
      match unify frame.context fnct_ttype (ttype_from_funct frame.context arg_ttype) with
      | Some fnct_ttype_post ->
         helper_helper fnct_ttype_post arg_ttype
      | None ->
         helper_helper fnct_ttype arg_ttype
    in
    let (fnct_known_post, arg_known_post, res_known_post) =
      let a, b, c =
        if arg_known = []
        then list_split3 (filter_crossmap2 helper fnct_known [ttype_of_atype frame.context (ABruijn 0)]) (* this is for lambda variables *)
        else list_split3 (filter_crossmap2 helper fnct_known arg_known)
      in
      list_uniq a, list_uniq b, list_uniq c
    in
    (fnct_known_post, ftype_used_and frame (map (ttype_from_funct frame.context) arg_known_post) (map (ttype_to_funct frame.context) res_known_post)),
    (arg_known_post, filter_map (fun fnct -> match ttype_funct_src_dst frame.context fnct with Some (src, _) -> some src | _ -> none) fnct_known_post),
    (res_known_post, res_used)



  (* SYMBOLS *)

  let empty_symbol name: symbol = name, []

  let ftype_of_symbol (frame: t) (symbol: symbol): ftype =
    let _, instances = symbol in
    list_uniq (map (fun (_, instance) -> ttype_of_expr frame.context instance) instances), []

  let symbol_add_instance (frame: t) (symbol: symbol) level expr: symbol =
    let name, instances = symbol
    and helper (tt1, _) (tt2, _) =
      if le frame tt1 tt2 then -1
      else if le frame tt2 tt1 then 1
      else 0
    in
    name, stable_sort helper (instances @ [level, expr])

  let symbol_add_instances (frame: t) (symbol: symbol) exprs: symbol =
    let name, instances = symbol
    and helper (tt1, _) (tt2, _) =
      if le frame tt1 tt2 then -1
      else if le frame tt2 tt1 then 1
      else 0
    in
    name, stable_sort helper (instances @ (map (fun e -> (ttype_of_expr frame.context e, e)) exprs))

  let string_of_symbol (frame: t) (symbol: symbol) =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf
    and name, instances = symbol in
    add_string name;
    add_string " [";
    add_string (string_of_list ~separator:", "
                  (fun (_, expr) -> (string_of_expr frame.context expr)
                                    ^ ": "
                                    ^ (string_of_ttype frame.context (ttype_of_expr frame.context expr))) instances);
    add_string "]";
    Buffer.contents buf

  let symbol_normalize (frame: t) (symbol: symbol) =
    let name, instances = symbol
    and helper (tt1, _) (tt2, _) =
      if le frame tt1 tt2 then -1
      else if le frame tt2 tt1 then 1
      else 0
    in
    name, stable_sort helper instances

  let symbol_first_instance (frame: t) (symbol: symbol): expr =
    let name, instances = symbol_normalize frame symbol in
    match instances with
    | (_, expr)::_ ->
       expr
    | _ ->
       raise (Invalid_argument ("[symbol_first_instance] symbol '" ^ name ^"' has no instance!"))

  let symbol_ambiguities (symbol: symbol) = length (snd symbol)

  let symbol_tighten (frame: t) (symbol: symbol) (ft: ftype): symbol =
    let name, instances = symbol in
    name, filter (fun (_, expr) ->
              let ttype = ttype_of_expr frame.context expr in
              (0 = length (snd ft))
                || exists (fun tt -> is_some (frame_compat frame ttype tt)) (snd ft)) instances

  let symbol_uniq (symbol: symbol) =
    match snd symbol with
    | [_, texpr] -> some texpr
    | _ -> none



  (* FUZZY EXPRESSIONS *)

  let rec ftype_of_fexpr = function
    | FTarget (_, res) | FSymb (_, res) | FApp (_, _, res) | FVar (_, res) | FLambda (_, _, _, res) ->
       res
    | FLet (_, _, body) ->
       ftype_of_fexpr body

  let rec fexpr_fuzzy_height = function
    | FTarget _ -> 0
    | FSymb _ -> 1
    | FApp (fnct, arg, _) -> 1 + max (fexpr_fuzzy_height fnct) (fexpr_fuzzy_height arg)
    | FVar _ -> 1
    | FLet (_, def, body) -> (fexpr_fuzzy_height def) + (fexpr_fuzzy_height body)
    | FLambda (_, _, body, _) -> 1 + fexpr_fuzzy_height body

  let rec fexpr_ambiguities = function
    | FTarget _ -> 0
    | FSymb (symbol, _) -> symbol_ambiguities symbol
    | FApp (fnct, arg, _) -> (fexpr_ambiguities fnct) + (fexpr_ambiguities arg)
    | FVar (_, known) -> ftype_ambiguities known
    | FLet (_, def, body) -> (fexpr_ambiguities def) + (fexpr_ambiguities body)
    | FLambda (_, ftype, body, _) -> (ftype_ambiguities ftype) + (fexpr_ambiguities body)

  let string_of_fexpr ?(with_annot=true) frame fexpr =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    let add_annot ft =
      if with_annot then begin
          add_string ": ";
          add_string (string_of_ftype frame ft)
        end
    in
    let rec s_of_fe = function
      | FTarget (expr, ftype) ->
         add_string (string_of_expr frame.context expr);
         add_annot ftype
      | FSymb (symbol, ftype) ->
         add_string (string_of_symbol frame symbol);
         add_annot ftype
      | FApp (fnct, arg, ftype) ->
         add_string "(";
         s_of_fe fnct;
         add_string " ";
         s_of_fe arg;
         add_string ")";
         add_annot ftype
      | FVar (name, ftype) ->
         add_string "?";
         add_string name;
         add_annot ftype
      | FLet (name, def, body) ->
         add_string "(let ";
         add_string name;
         add_string " = ";
         s_of_fe def;
         add_string " in ";
         s_of_fe body;
         add_string ")";
      | FLambda (name, name_ftype, body, ftype) ->
         add_string "(fun ";
         add_string name;
         add_annot name_ftype;
         add_string " => ";
         s_of_fe body;
         add_string ")";
         add_annot ftype;
    in
    s_of_fe fexpr;
    Buffer.contents buf


  (* FIXME: find a good title *)

  let do_with_local_variable frame name ftype action =
    let store_new =  { store = NotTrie.update name (fun _ -> some ftype) NotTrie.empty } in
    frame.variables <- store_new :: frame.variables;
    let res = action () in
    frame.variables <- List.tl frame.variables;
    res

  let add_variable frame name ttype =
    T.add_variable frame.context name ttype

  let add_symbol frame name level texpr =
    frame.symbols <- NotTrie.update name
                       (fun osymb ->
                         let symb = value ~default:(empty_symbol name) osymb in
                         some (symbol_add_instance frame symb level texpr))
                       frame.symbols

  let add_constant_parser frame parser =
    frame.constant_parsers <- frame.constant_parsers @ [parser]

  let read_local_variable frame name =
    List.find_map
      (fun lvars ->
        Option.map (fun ftype -> ftype) (NotTrie.find_opt name lvars.store))
      frame.variables

  let update_local_variable frame name ftype =
    let rec helper = function
      | lvars :: tail ->
         if NotTrie.mem name lvars.store
         then
           lvars.store <- NotTrie.update name (fun _ -> some ftype) lvars.store
         else
           helper tail
      | [] ->
         raise (Invalid_argument ("[Elaboration.update_local_variable] trying to "
                                  ^ "update non-existent variable " ^ name))
    in
    helper frame.variables

  let interpret_name frame name =
    let interpret_local_variable name =
      Option.map (fun ftype -> FVar (name, ftype)) (read_local_variable frame name)
    and interpret_target_variable name =
      Option.map
        (fun expr -> FTarget (expr, ftype_above_ttype frame (T.ttype_of_expr frame.context expr)))
        (T.read_variable frame.context name)
    and interpret_symbol name =
      let symbol_raw = value ~default:(empty_symbol name) (NotTrie.find_opt name frame.symbols)
      and exprs = T.expr_of_string frame.context name
      in
      let symbol = symbol_add_instances frame symbol_raw exprs in
      some (FSymb (symbol, ftype_of_symbol frame symbol))
    in
    chain_of_responsibility [interpret_local_variable;
                             interpret_target_variable;
                             interpret_symbol] name

  let string_of_frame frame =
    let buf = Buffer.create 0 in
    let add_string = Buffer.add_string buf in
    let string_of_symbols () =
      add_string "(* available symbols for elaboration:\n";
      NotTrie.iter (fun _ symbol ->
          add_string "\t";
          add_string (string_of_symbol frame symbol);
          add_string "\n") frame.symbols;
      add_string "*)\n\n"
    in
    string_of_symbols ();
    add_string (T.string_of_context frame.context);
    Buffer.contents buf

end
