open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools

let uexprs_to_test =
  ["add 0 (mul 0 (v 0))",
   left "Eadd E0 (Eext_mul (K_of_Z (Z_of_N 0%N)) (v 0%N)): E"]

let _ =
  let frame = empty () in
  log frame.context "Section Zero.\n\n";
  add_NZ_to frame;
  add_field_to frame "K";
  add_vector_space_to frame "E" "K";
  add_variable frame "v" (CFunct (CBasic "N", CBasic "E"));
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Zero.\n";
  print_string (string_of_frame frame);
  ()
