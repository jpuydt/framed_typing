open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools

let uexprs_to_test =
  let add_f = "add f",
              left "Fadd f: F -> F"
  and mul_k = "mul k",
              left "Kmul k: K -> K"
  and add_ff = "add f f",
               left "Fadd f f: F"
  and add_fg = "add f g",
               left "Eadd (E_of_F f) (E_of_G g): E"
  and add_f0 = "add f 0",
               left "Fadd f F0: F"
  and mul_k0 = "mul k 0",
               left "Kmul k (K_of_Z (Z_of_N 0%N)): K"
  and mul_kf = "mul k f",
               left "Fext_mul k f: F"
  in
  [add_f; add_ff; add_fg; add_f0; mul_k; mul_k0; mul_kf]

let _ =
  let frame = empty () in
  log frame.context "Section Vectors.\n\n";
  add_NZ_to frame;
  add_field_to frame "K";
  add_vector_space_to frame "E" "K";
  add_subvector_space_to frame "F" "E" "K";
  add_subvector_space_to frame "G" "E" "K";
  add_variable frame "f" (ttype_basic frame "F");
  add_variable frame "g" (ttype_basic frame "G");
  add_variable frame "k" (ttype_basic frame "K");
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Vectors.\n";
  print_string (string_of_frame frame);
  ()
