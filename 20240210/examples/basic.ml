open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools

let uexprs_to_test =
  let stupid_a_bool = "a: bool",
                      right "[Elaborate.impose] FTarget: a: { known as [ N, Z ] } is used as { used as [ bool ] } which is incoherent"
  and plus_a_b = "add a b",
                 left "N.add a b: N"
  and plus_a_c = "add a c",
                 left "Z.add (Z_of_N a) c: Z"
  and plus_c_1 = "add c 1",
                 left "Z.add c (Z_of_N 1%N): Z"
  and not_foo = "fun x |-> not x",
                left "fun x: Prop => not x: Prop -> Prop"
  and lambda_x_plus_minus_1 = "fun x |-> add x -1",
                              left "fun x: N => Z.add (Z_of_N x) (-1)%Z: N -> Z"
  and plus_a_annot = "(add a): Z -> Z",
                     left "Z.add (Z_of_N a): Z -> Z"
  and plus_c = "add c",
               left "Z.add c: Z -> Z"
  and plus_a = "add a",
               left "N.add a: N -> N"
  and plus_a_b_c = "add (add a b) c",
                   left "Z.add (Z_of_N (N.add a b)) c: Z"
  and plus_1_annot = "fun x: Z |-> add x 1",
                     left "fun x: Z => Z.add x (Z_of_N 1%N): Z -> Z"
  and plus_two_vars = "fun x |-> (fun y |-> add x y)",
                      left "fun x: N => fun y: N => N.add x y: N -> N -> N"
  in
  [stupid_a_bool; plus_a_b; plus_a_c; plus_c_1; not_foo;
   lambda_x_plus_minus_1; plus_a_annot; plus_c; plus_a;
   plus_a_b_c; plus_1_annot; plus_two_vars]

let _ =
  let frame = empty () in
  log frame.context "Section Basic.\n\n";
  add_NZ_to frame;
  add_variable frame "a" (ttype_basic frame "N");
  add_variable frame "b" (ttype_basic frame "N");
  add_variable frame "c" (ttype_basic frame  "Z");
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Basic.\n";
  print_string (string_of_frame frame);
  ()
