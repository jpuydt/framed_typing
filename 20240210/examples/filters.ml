open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools

let uexprs_to_test =
  let v_to = "tends_to v",
             left "tends_to v: filter N -> filter R -> Prop"
  and v_to_x = "tends_to v +oo x",
               left "tends_to v inftyN (filter_of_Rbar (Rbar_of_R x)): Prop"
  and v_to_infty = "tends_to v +oo +oo",
                   left "tends_to v inftyN (filter_of_Rbar pinftyR): Prop"
  and f_x_to_x = "tends_to f x x",
                 left "tends_to f (filter_of_Rbar (Rbar_of_R x)) (filter_of_Rbar (Rbar_of_R x)): Prop"
  and f_x_to_infty = "tends_to f x +oo",
                     left "tends_to f (filter_of_Rbar (Rbar_of_R x)) (filter_of_Rbar pinftyR): Prop"
  and f_infty_to_x = "tends_to f +oo x",
                     left "tends_to f (filter_of_Rbar pinftyR) (filter_of_Rbar (Rbar_of_R x)): Prop"
  and f_infty_to_1 = "tends_to f +oo 1",
                     left "tends_to f (filter_of_Rbar pinftyR) (filter_of_Rbar (Rbar_of_R (R_of_Z (Z_of_N 1%N)))): Prop"
  and f_infty_to_infty = "tends_to f +oo +oo",
                         left "tends_to f (filter_of_Rbar pinftyR) (filter_of_Rbar pinftyR): Prop"
  in
  [
    v_to; v_to_x; v_to_infty; f_x_to_x; f_x_to_infty;
    f_infty_to_x; f_infty_to_1; f_infty_to_infty
  ]

let _ =
  let frame = empty () in
  log frame.context "Section Filters.\n\n";
  add_NZ_to frame;
  add_filters_to frame;
  add_variable frame "x" (ttype_basic frame "R");
  add_variable frame "v" (CFunct (CBasic "N", CBasic "R"));
  add_variable frame "f" (CFunct (CBasic "R", CBasic "R"));
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Filters.\n";
  print_string (string_of_frame frame);
  ()
