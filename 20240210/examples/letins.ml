open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools


let uexprs_to_test =
  let plus x y = "add " ^ x ^ " " ^ y in
  let let_plus x y = ("let t = " ^ x ^ " in " ^ (plus "t" y))
  and plus_let x y = ("add " ^ x ^ " (let t = " ^ y ^ " in t)")
  in
  let let_plus_a_b = let_plus "a" "b", left "let t := a in N.add t b: N"
  and let_plus_a_c = let_plus "a" "c", left "let t := a in Z.add (Z_of_N t) c: Z"
  and let_plus_c_a = let_plus "c" "a", left "let t := c in Z.add t (Z_of_N a): Z"
  and let_plus_a_0 = let_plus "a" "0", left "let t := a in N.add t 0%N: N"
  and let_plus_c_0 = let_plus "c" "0", left "let t := c in Z.add t (Z_of_N 0%N): Z"
  and let_plus_0_a = let_plus "0" "a", left "let t := 0%N in N.add t a: N"
  and let_plus_0_c = let_plus "0" "c", left "let t := 0%N in Z.add (Z_of_N t) c: Z"
  and plus_let_a_b = plus_let "a" "b", left "N.add a (let t := b in t): N"
  and plus_let_a_c = plus_let "a" "c", left "Z.add (Z_of_N a) (let t := c in t): Z"
  in
  [let_plus_a_b; let_plus_a_c; let_plus_c_a; let_plus_a_0; let_plus_c_0; let_plus_0_a; let_plus_0_c;
   plus_let_a_b; plus_let_a_c]

let _ =
  let frame = empty () in
  log frame.context "Section Letins.";
  add_NZ_to frame;
  add_variable frame "a" (ttype_basic frame "N");
  add_variable frame "b" (ttype_basic frame "N");
  add_variable frame "c" (ttype_basic frame "Z");
  test_uexprs frame uexprs_to_test;
  log frame.context "End Letins.";
  print_string (string_of_frame frame);
  ()
