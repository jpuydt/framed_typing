open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools


let uexprs_to_test =
  let add_f = "add f",
              left "FIKadd f: (I -> K) -> I -> K"
  and add_fg = "add f g",
               left "FIKadd f g: I -> K"
  and mul_fg = "mul f g",
               left "FIKmul f g: I -> K"
  and add_f0 = "add f 0",
               left "FIKadd f (FIK_of_K (K_of_Z (Z_of_N 0%N))): I -> K"
  and add_fh = "add f h",
               left "FIKadd f (compose K_of_Z (compose Z_of_N h)): I -> K"
  and mul_kf = "mul k f",
               left "FIKext_mul k f: I -> K"
  in
  [add_f; add_fg; mul_fg; add_f0; add_fh; mul_kf]

let _ =
  let frame = empty () in
  log frame.context "Section Functions.";
  add_NZ_to frame;
  add_field_to frame "K";
  add_variable frame "I" (CBasic "Type");
  add_numeric_functions_on_to frame "I" "K";
  add_variable frame "f" (CFunct (CBasic "I", CBasic "K"));
  add_variable frame "g" (CFunct (CBasic "I", CBasic "K"));
  add_variable frame "h" (CFunct (CBasic "I", CBasic "N"));
  add_variable frame "k" (CBasic "K");
  test_uexprs frame uexprs_to_test;
  log frame.context "End Functions.";
  print_string (string_of_frame frame);
  ()
