open Either
open Stdlib.Option

module T = Elaboration.Pseudocoq
module E = Elaboration.Elaborate.Make(T)

open Elaboration.Annot
open T
open E.Frame

let ttype_basic frame name = T.ttype_of_atype frame (ABasic name)

let uexpr_of_string str =
  let lexbuf = Sedlexing.Utf8.from_string str in
  let lexer = Sedlexing.with_tokenizer Elaboration.Untyped_lexer.token lexbuf in
  let parser = MenhirLib.Convert.Simplified.traditional2revised Elaboration.Untyped_parser.main in
  parser lexer

let add_symb (frame: E.Frame.t) name level var =
  match T.read_variable frame.context var with
  | Some expr ->
     add_symbol frame name (T.ttype_of_atype frame level) expr
  | None ->
     raise (Invalid_argument ("add_s: '" ^ var ^ "' seems unknown"))

let add_var (frame: E.Frame.t) name atype =
  add_variable frame name (T.ttype_of_atype frame atype)

let add_full (frame: E.Frame.t) symb level name atype =
  add_var frame name atype;
  add_symb frame symb level name

let add_NZ_to (frame: E.Frame.t) =
  let nn = ABasic "N"
  and zz = ABasic "Z"
  in
  add_coercion frame "Z_of_N";
  add_full frame "add" nn "N.add" (atype_operator nn);
  add_full frame "mul" nn "N.mul" (atype_operator nn);
  add_full frame "add" zz "Z.add" (atype_operator zz);
  add_full frame "mul" zz "Z.mul" (atype_operator zz)

let add_comparisons_to (frame: E.Frame.t) =
  let n = ABasic "N"
  and z = ABasic "Z"
  and r = ABasic "R"
  and c = ABasic "C"
  in
  add_NZ_to frame;
  add_var frame "leN" (atype_relation n);
  add_full frame "le" n "leN" (atype_relation n);
  add_var frame "leZ" (atype_relation z);
  add_full frame "le" z "leZ" (atype_relation z);
  add_var frame "R" (ABasic "Type");
  add_var frame "leR" (atype_relation z);
  add_full frame "le" r "leR" (atype_relation r);
  add_var frame "R_of_Z" (AFunct (z, r));
  add_coercion frame "R_of_Z";
  add_var frame "C" (ABasic "Type");
  add_var frame "C_of_R" (AFunct (r, c));
  add_coercion frame "C_of_R"

let add_field_to (frame: E.Frame.t) name =
  let zz = ABasic "Z"
  and kk = ABasic name
  in
  add_var frame name (ABasic "Type");
  add_var frame (name ^ "_of_Z") (AFunct (zz, kk));
  add_coercion frame (name ^ "_of_Z");
  add_full frame "add" kk (name ^ "add") (atype_operator kk);
  add_full frame "mul" kk (name ^ "mul") (atype_operator kk)

let add_ring_with_quotient_to (frame: E.Frame.t) =
  let r = ABasic "R"
  and rquoti = ABasic "RquotI"
  in
  add_field_to frame "R";
  add_var frame "RquotI" (ABasic "Type");
  add_var frame "R_modI" (AFunct (r, rquoti));
  add_coercion frame "R_modI";
  add_full frame "add" rquoti "RquotIadd" (atype_operator rquoti);
  add_full frame "mul" rquoti "RquotImul" (atype_operator rquoti)

let add_vector_space_to (frame: E.Frame.t) name kk_name =
  let kk = ABasic kk_name
  and ee = ABasic name
  in
  add_var frame name (ABasic "Type");
  add_full frame "add" ee (name ^ "add") (atype_operator ee);
  add_full frame "mul" ee (name ^ "ext_mul") (AFunct (kk, AFunct (ee, ee)));
  add_full frame "0" ee (name ^ "0") ee;
  add_constant_parser frame (fun value -> if value = "0"
                                              then [CConst (name ^ "0", T.ttype_of_atype frame ee)]
                                              else [])

let add_subvector_space_to (frame: E.Frame.t) name ee_name kk_name =
  let kk = ABasic kk_name
  and ee = ABasic ee_name
  and ff = ABasic name
  in
  add_var frame name (ABasic "Type");
  add_var frame (ee_name ^ "_of_" ^ name) (AFunct (ff, ee));
  add_coercion frame (ee_name ^ "_of_" ^ name);
  add_full frame "add" ff (name ^ "add") (atype_operator ff);
  add_full frame "mul" ff (name ^ "ext_mul") (AFunct (kk, AFunct (ff, ff)));
  add_full frame "0" ff (name ^ "0") ff;
  add_constant_parser frame (fun value -> if value = "0"
                                              then [CConst (name ^ "0", T.ttype_of_atype frame ff)]
                                              else [])

let add_filters_to (frame: E.Frame.t) =
  let n = ABasic "N"
  and z = ABasic "Z"
  and r = ABasic "R"
  and rbar = ABasic "Rbar"
  in
  let filter_n = AParam ("filter", [n])
  and filter_r = AParam ("filter", [r])
  in
  add_var frame "filter" (atype_involution (ABasic "Type"));
  add_var frame "tends_to" (AFunct (AFunct (ABruijn 0, ABruijn 1),
                                    AFunct (AParam ("filter", [ABruijn 0]),
                                            AFunct (AParam ("filter", [ABruijn 1]),
                                                    ABasic "Prop"))));
  add_var frame "R" (ABasic "Type");
  add_var frame "Rbar" (ABasic "Type");
  add_var frame "R_of_Z" (AFunct (z, r));
  add_coercion frame "R_of_Z";
  add_var frame "Rbar_of_R" (AFunct (r, rbar));
  add_coercion frame "Rbar_of_R";
  add_var frame "filter_of_Rbar" (AFunct (rbar, filter_r));
  add_coercion frame "filter_of_Rbar";
  add_full frame "+oo" n "inftyN" filter_n;
  add_full frame "+oo" r "pinftyR" rbar;
  add_full frame "-oo" r "minftyR" rbar

let add_numeric_functions_on_to (frame: E.Frame.t) name kk_name =
  let kk = ABasic kk_name
  and fik = AFunct (ABasic name, ABasic kk_name)
  in
  add_var frame ("F" ^ name ^ kk_name ^ "_of_" ^ kk_name) (AFunct (kk, fik));
  add_coercion frame ("F" ^ name ^ kk_name ^ "_of_" ^ kk_name);
  add_full frame "add" fik ("F" ^ name ^ kk_name ^ "add") (atype_operator fik);
  (* beware here: here we implicitly declare that the exterior multiplication is simpler! *)
  add_full frame "mul" fik ("F" ^ name ^ kk_name ^ "ext_mul") (AFunct (kk, AFunct (fik, fik)));
  add_full frame "mul" fik ("F" ^ name ^ kk_name ^ "mul") (atype_operator fik)

let test_uexprs (frame: E.Frame.t) =
  let handle (string, expected) =
    let actual =
      try
        let uexpr = uexpr_of_string string in
        let res = E.elaborate frame uexpr in
        let s_res = string_of_expr frame.context res
        and c_res = string_of_ttype frame.context (ttype_of_expr frame.context res)
        in
        left (s_res ^ ": " ^ c_res)
      with Invalid_argument msg ->
        right msg
    in
    match actual, expected with
    | Left s_actual, Left s_expected ->
       log frame.context
           ("Check "
            ^ s_actual
            ^ ". "
            ^ (if s_actual = s_expected
               then ""
               else ("(* BAD, expected: " ^ s_expected ^ " *)"))
           ^ "\n")
    | Right s_actual, Left s_expected ->
       log frame.context
       ("(* BAD expected a result "
          ^ s_expected
          ^ " but got error "
          ^ s_actual
          ^ " *)\n")
    | Left s_actual, Right s_expected ->
       log frame.context
       ("Check "
        ^ s_actual
        ^ ". (* maybe BAD, expected "
        ^ s_expected
        ^ " *)\n")
    | Right s_actual, Right s_expected ->
       if s_actual = s_expected
       then begin
           log frame.context
             ("(* expected error: "
              ^  s_actual
              ^  " *)\n")
         end else begin
           log frame.context
             ("(* BAD expected error "
              ^ s_expected
              ^ " but instead got error "
              ^ s_actual
              ^ " *)\n")
         end
  in
  Stdlib.List.iter handle
