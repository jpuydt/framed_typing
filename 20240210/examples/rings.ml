open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools


let uexprs_to_test =
  let plus_a_b = "add a b",
                 left "Radd (R_of_Z (Z_of_N a)) b: R"
  and plus_a_c = "add a c",
                 left "RquotIadd (R_modI (R_of_Z (Z_of_N a))) c: RquotI"
  and plus_c_1 = "add c 1",
                 left "RquotIadd c (R_modI (R_of_Z (Z_of_N 1%N))): RquotI"
  and lambda_x_plus_1 = "fun x: N |-> add x -1",
                        left "fun x: N => Z.add (Z_of_N x) (-1)%Z: N -> Z"
  and plus_a_annot = "(add a): R -> R",
                     left "Radd (R_of_Z (Z_of_N a)): R -> R"
  and plus_c = "add c",
               left "RquotIadd c: RquotI -> RquotI"
  and plus_a = "add a",
               left "N.add a: N -> N"
  and plus_a_b_c = "add (add a b) c",
                   left "RquotIadd (R_modI (Radd (R_of_Z (Z_of_N a)) b)) c: RquotI"
  and plus_two_vars = "fun x |-> (fun y |-> add x y)",
                      left "fun x: N => fun y: N => N.add x y: N -> N -> N"
  and plus_two_vars_annot = "fun x: R |-> (fun y |-> add x y)",
                            left "fun x: R => fun y: R => Radd x y: R -> R -> R"
  in
  [plus_a_b; plus_a_c; plus_c_1; lambda_x_plus_1; plus_a_annot;
   plus_c; plus_a; plus_a_b_c; plus_two_vars; plus_two_vars_annot]

let _ =
  let frame = empty () in
  log frame.context "Section Rings.\n\n";
  add_NZ_to frame;
  add_ring_with_quotient_to frame;
  add_variable frame "a" (ttype_basic frame "N");
  add_variable frame "b" (ttype_basic frame "R");
  add_variable frame "c" (ttype_basic frame "RquotI");
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Rings.\n";
  print_string (string_of_frame frame);
  ()
