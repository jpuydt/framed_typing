open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools


let uexprs_to_test =
  let le_a_a = "le a a",
               left "leN a a: Prop"
  and le_a_b = "le a b",
               left "leZ (Z_of_N a) b: Prop"
  and le_a_x = "le a x",
               left "leR (R_of_Z (Z_of_N a)) x: Prop"
  and le_a_z = "le a z",
               right "[Elaborate.bottom_top_pass] impossible application of function (le [leN: N -> N -> Prop, leZ: Z -> Z -> Prop, leR: R -> R -> Prop]: { known as [ N -> N -> Prop, Z -> Z -> Prop, R -> R -> Prop ] used as [ N -> N -> Prop, N -> Z -> Prop, N -> R -> Prop, Z -> N -> Prop, Z -> Z -> Prop, Z -> R -> Prop, R -> N -> Prop, R -> Z -> Prop, R -> R -> Prop ] } a: { known as [ N, Z, R ] used as [ N, Z, R ] }): { known as [ N -> Prop, Z -> Prop, R -> Prop ] } to the argument z: { known as [ C ] }"
  and le_b_x = "le b x",
               left "leR (R_of_Z b) x: Prop"
  and le_z_x = "le z x",
               right "[Elaborate.bottom_top_pass] impossible application of function le [leN: N -> N -> Prop, leZ: Z -> Z -> Prop, leR: R -> R -> Prop]: { known as [ N -> N -> Prop, Z -> Z -> Prop, R -> R -> Prop ] } to the argument z: { known as [ C ] }"
  in
  [le_a_a; le_a_b; le_a_x; le_a_z; le_b_x; le_z_x]

let _ =
  let frame = empty () in
  log frame.context "Section Inequalities.\n\n";
  add_comparisons_to frame;
  add_variable frame "a" (ttype_basic frame "N");
  add_variable frame "b" (ttype_basic frame "Z");
  add_variable frame "x" (ttype_basic frame "R");
  add_variable frame "z" (ttype_basic frame "C");
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Inequalities.\n";
  print_string (string_of_frame frame);
  ()
