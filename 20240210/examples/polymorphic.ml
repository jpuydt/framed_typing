open Either

module T = Elaboration.Pseudocoq
open T
module F = Elaboration.Frame.Make(T)
open F

open Tools

let uexprs_to_test =
  let cons_NZ = "cons 1 nil",
                left "cons 1%N nil: list N"
  and cons_N = "cons a nil",
               left "cons a nil: list N"
  and cons_Z = "cons -1 nil",
               left "cons (-1)%Z nil: list Z"
  and list_N = "cons 1 (cons 2 nil)",
               left "cons 1%N (cons 2%N nil): list N"
  and list_Z1 = "cons -1 (cons 2 nil)",
                left "cons (-1)%Z (cons (Z_of_N 2%N) nil): list Z"
  and list_Z2 = "cons 1 (cons -2 nil)",
                left "cons (Z_of_N 1%N) (cons (-2)%Z nil): list Z"
  and stupid_cons_a_a = "cons a a",
                        right "[Elaborate.bottom_top_pass] impossible application of function (cons: { known as [ N -> list N -> list N, Z -> list Z -> list Z ] used as [ N -> list N -> list N, N -> list Z -> list Z, Z -> list N -> list N, Z -> list Z -> list Z ] } a: { known as [ N, Z ] used as [ N, Z ] }): { known as [ list N -> list N, list Z -> list Z ] } to the argument a: { known as [ N, Z ] }"
  and tail = "fun tl |-> cons a tl",
             left "fun tl: list N => cons a tl: list N -> list N"
  and list_annot = "cons 1 nil: list Z",
                   left "cons (Z_of_N 1%N) nil: list Z"
  and eq3_abc = "eq3 a b c",
                left "eq3 (Z_of_N a) (Z_of_N b) c: bool"
  in
  [cons_NZ; cons_N; cons_Z; list_N; list_Z1; list_Z2;
   stupid_cons_a_a; tail; list_annot; eq3_abc]

let _ =
  let frame = empty () in
  log frame.context "Section Polymorphic.\n\n";
  add_NZ_to frame;
  add_variable frame "a" (ttype_basic frame "N");
  add_variable frame "b" (ttype_basic frame "N");
  add_variable frame "c" (ttype_basic frame "Z");
  add_variable frame "eq3" (CFunct (CBruijn 0,
                                      CFunct(CBruijn 0, CFunct (CBruijn 0, CBasic "bool"))));
  test_uexprs frame uexprs_to_test;
  log frame.context "\n\nEnd Polymorphic.\n";
  print_string (string_of_frame frame);
  ()
